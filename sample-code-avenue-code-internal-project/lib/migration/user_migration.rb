require 'migration/base_migration'
require 'migration/parser/address_parser'
require 'migration/parser/contract_parser'
require 'migration/parser/role_parser'
require 'migration/parser/location_parser'
require 'migration/parser/visa_parser'

module Migration
  # Migrate the given CSV file to
  class UserMigration < BaseMigration
    attr_reader :office

    private

    def process_header(row)
      return unless row.first
      Rails.logger.info row.first.blue
      @office = row.first.split.last
    end

    def process_row(row)
      user_data = to_hash row
      user = parse user_data
      user.save! unless dry_run
      raise user.errors.messages.to_s unless user.valid?
      migration_success row.first
    rescue => e
      migration_fail row.first, e
    end

    def parse(user_csv)
      user = User.new
      user.assign_attributes user_csv.select { |k, _v| User.fields.keys.include? k.to_s }

      parse_user_fields user, user_csv
      parser_other_fields user, user_csv

      use_existing_one user
    end

    def use_existing_one(user)
      db_user = User.where(cpf: user.cpf).first
      db_user ||= User.where(ac_email: user.ac_email).first
      if db_user
        db_user.assign_attributes user.attributes.except('_id')
        db_user
      else
        user
      end
    end

    def parser_other_fields(user, user_csv)
      [
        Migration::Parser::AddressParser,
        Migration::Parser::ContractParser,
        Migration::Parser::RoleParser,
        Migration::Parser::LocationParser,
        Migration::Parser::VisaParser
      ].each { |parser| parser.new(user, user_csv).parse }
    end

    def parse_user_fields(user, user_csv)
      user.full_name = user_csv[:full_name]
      user.category    = user_csv[:contract]
      user.birth_date  = to_date user_csv[:birth_date]
    end

    def to_hash(array)
      result = header_position.map { |key, index| [key, array[index]] }.to_h
      result[:address] = result[:address].tr "\n", ' '
      result[:ctps] ||= "__#{result[:cpf_cnpj]}"
      manual_fields.merge result.compact
    end

    # Default values
    def manual_fields
      {
        role:                           'Fellow',
        register_position:              'Mid', # TODO: Check if the spreadsheet contains this field
        nationality:                    'Brazilian',
        emergency_contact_phone_number: '(31) 2516-1448',
        emergency_contact_relationship: 'AC',
        emergency_contact_name:         'AC',
        skype:                          '_',
        personal_email:                 'mail@dont.know',
        location_name:                  ENV['LOCATION_NAME'] || config[:default]['location_name'],
        serial:                         '_',
        pis:                            '_',
        ctps_state:                     '_'
      }
    end

    def header_position
      @header_position ||= header.map.with_index { |h, index| [config[:user].key(h).to_sym, index] }.to_h
    end
  end
end
