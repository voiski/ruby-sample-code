require "#{Rails.root}/config/environment"
require 'net/http'

module Migration
  # Migration support with helper methods
  module Support
    protected

    def to_date(value)
      Date.parse value if value
    rescue => e
      raise "#{e} - #{value}"
    end

    def config
      @config ||= HashWithIndifferentAccess.new YAML.load_file(File.join(Rails.root, 'config', 'migration.yml'))
    end
  end
end
