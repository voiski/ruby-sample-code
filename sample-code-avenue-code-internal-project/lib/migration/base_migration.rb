require 'migration/support'
require 'csv'
require 'i18n'

module Migration
  # Method template for migration
  class BaseMigration
    include Migration::Support
    attr_reader :file, :dry_run, :total_of_rows, :header

    def initialize(file, dry_run=false)
      @file = file
      @dry_run = dry_run
    end

    def import(line=nil)
      @total_of_rows = 0
      @errors = []
      CSV.foreach(@file) { |row| handler_row(row, line) }
      @header_line = nil

      return if errors.empty?
      print_errors
      print_failed_examples
    end

    def errors
      @errors ||= []
    end

    protected

    def print_errors
      Rails.logger.error "Failed #{errors.count} of #{total_of_rows}".on_red
      errors.each_with_index do |e, index|
        Rails.logger.error "#{index + 1}) #{e[:message]}\n#{e[:exception].to_s.red}"
      end
    end

    def print_failed_examples
      Rails.logger.error "\nFailed examples:"
      errors.each { |e| Rails.logger.error "#{rake_command(e).red}\t# #{e[:message].blue}" }
    end

    def rake_command(e)
      @rake_arg ||= if ARGV.first
                      ARGV.first[0..ARGV.first.size - 2]
                    else
                      'migrate:type[\'/path/to/file\''
                    end
      "rake #{@rake_arg},#{e[:line]}]"
    end

    def handler_row(row, line)
      @total_of_rows ||= 0
      if @header_line
        @total_of_rows += 1
        process_row(row) if line.nil? || @total_of_rows == line.to_i
      elsif row.second.present?
        @header = row
        @header_line = true
      else
        process_header row
      end
    end

    def process_header(_row)
      raise 'Not Implemented!'
    end

    def process_row(_row)
      raise 'Not Implemented!'
    end

    def migration_success(message)
      Rails.logger.info "#{message} - #{@dry_run ? :OK : :SAVED} (_8(D)".green
    end

    def migration_fail(message, exception)
      errors << { line: total_of_rows, message: message, exception: exception }
      Rails.logger.error "#{message} - ERROR (_B(/) #{exception}".red
    end
  end
end
