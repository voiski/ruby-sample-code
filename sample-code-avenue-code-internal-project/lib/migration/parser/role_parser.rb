require 'migration/parser/base_parser'

module Migration
  module Parser
    # Role Parser
    class RoleParser < BaseParser
      def parse
        model.company_role = User.company_roles[csv_row[:role].downcase.to_sym]
      end
    end # end class
  end
end
