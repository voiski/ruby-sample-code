require 'migration/support'

module Migration
  module Parser
    # Base parser with helpers
    class BaseParser
      include Migration::Support

      attr_reader :model, :csv_row

      def initialize(model, csv_row)
        @model   = model
        @csv_row = csv_row
      end

      def parse
        raise 'Not Implemented!'
      end

      protected

      def json_client(url)
        uri = URI.parse(url)
        http = Net::HTTP.new(uri.host, uri.port)
        if url.include? 'https'
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE # read into this
        end
        response = http.get(uri.request_uri)
        JSON.parse response.body
      end

      def config
        @config ||= HashWithIndifferentAccess.new YAML.load_file(File.join(Rails.root, 'config', 'migration.yml'))
      end
    end # end class
  end
end
