require 'migration/parser/base_parser'

module Migration
  module Parser
    # Address Parser
    class AddressParser < BaseParser
      VIACEP_API = 'http://viacep.com.br/ws/%{cep}/json/'.freeze
      MAPS_API = 'https://maps.googleapis.com/maps/api/geocode/json?key=%{key}&address=%{address}'.freeze

      def parse
        raise 'Missing address' unless csv_row[:address]
        address = request_address

        splited_address = csv_row[:address].split(/[\n,-]/).map(&:strip)
        handle_missing_address_atributes address, splited_address
        parse_address_number address, splited_address
        model.assign_attributes address
      end

      protected

      def handle_missing_address_atributes(address, splited_address)
        address[:street] = splited_address.first if address[:street].blank?
        if address[:neighborhood].blank?
          address[:neighborhood] = splited_address.join.scan(/Bairro \w+/).first
          address[:neighborhood] ||= address[:city]
        end
      end

      def parse_address_number(address, splited_address)
        address[:number] = csv_row[:address].scan(/\d+/i).first
        complement = if !((address[:street] =~ /^\d{2,}$/)) && splited_address.first =~ /\d{2,}$/
                       splited_address.second
                     else
                       splited_address.third
                     end
        address[:complement] = complement if complement =~ /\d/
      end

      def request_address
        cep = csv_row[:address].scan(/\d{2}\.?\d{3}-?\.?\d{3}/).last
        address = address_by_cep cep if cep
        if address.blank?
          begin
            address = address_from_google csv_row[:address]
          rescue
            nil
          end
        end
        raise 'Wrong address format' if address.blank?
        address
      end

      def address_by_cep(cep)
        address = json_client VIACEP_API % { cep: cep.gsub(/\D/, '') }
        return nil if address['erro']
        {
          street:       address['logradouro'],
          neighborhood: address['bairro'],
          city:         address['localidade'],
          state:        address['uf'],
          zipcode:      address['cep'],
          country:      address['pais'] || config[:default]['country']
        }
      end

      def address_from_google(address)
        address = I18n.transliterate address
        response = json_client MAPS_API % { key: Rails.application.secrets.maps_api, address: address }
        handler_maps_response response if response['status'] == 'OK'
      end

      private

      def handler_maps_response(response)
        address_components = reduce_maps_response response
        {
          street:       address_components[:route],
          neighborhood: address_components[:sublocality],
          city:         address_components[:locality] || address_components[:administrative_area_level_2],
          state:        address_components[:administrative_area_level_1],
          zipcode:      address_components[:postal_code],
          country:      address_components[:country]
        }
      end

      def reduce_maps_response(response)
        address_components = {}
        response['results'].first['address_components'].each do |component|
          component['types'].each { |t| address_components[t.to_sym] = component['long_name'] }
        end
        address_components
      end
    end # end class
  end
end
