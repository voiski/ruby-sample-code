require 'migration/parser/base_parser'

module Migration
  module Parser
    # Contract Parser including doc parser
    class ContractParser < BaseParser
      def parse
        parse_cpf_cnpj
        parse_ac_fields
      end

      private

      def parse_cpf_cnpj
        if csv_row[:contract].casecmp('PJ').zero?
          model.cnpj = parse_cnpj
          model.is_pf = false
        else
          model.cpf = parse_cpf
        end
      end

      def parse_cnpj
        document 14 do |doc|
          "#{doc[0..1]}.#{doc[2..4]}.#{doc[5..7]}/#{doc[8..11]}-#{doc[12..13]}"
        end
      end

      def parse_cpf
        document 11 do |doc|
          "#{doc[0..2]}.#{doc[3..5]}.#{doc[6..8]}-#{doc[9..10]}"
        end
      end

      def parse_ac_fields
        model.billable = csv_row[:billable] == 'Billable'
        model.ac_email = complete_ac_mail
        model.registry_book_page = convert_registry_book_page
        model.annual_review_date = convert_annual_review_date
      end

      def complete_ac_mail
        raise 'No ac email' unless csv_row[:ac_mail]
        "#{csv_row[:ac_mail].split('@').first}@avenuecode.com"
      end

      def convert_registry_book_page
        return 0 unless csv_row[:registry_book_page]
        registry_book_numbers = csv_row[:registry_book_page].split('/').map(&:to_i)
        registry_book_numbers.first * 100 + registry_book_numbers.last
      end

      def convert_annual_review_date
        to_date(csv_row[:annual_review_date] || csv_row[:start_date]).at_beginning_of_month
      end

      def document(size)
        yield csv_row[:cpf_cnpj].gsub(/\D/, '').rjust(size, '0')
      end
    end # end class
  end
end
