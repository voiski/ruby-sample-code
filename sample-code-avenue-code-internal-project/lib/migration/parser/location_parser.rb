require 'migration/parser/base_parser'

module Migration
  module Parser
    # Location Parser
    class LocationParser < BaseParser
      def parse
        location       = Location.where(city: csv_row[:location_name].scan(/\w[\w\ ]+\w/).first).first
        model.location = location
        model.office   = location.office if location
      end
    end # end class
  end
end
