require 'rails_helper'
require 'migration/base_migration'

RSpec.describe Migration::BaseMigration, type: :lib do
  subject { Migration::BaseMigration.new 'sad' }
  let(:sample) { [['Header', nil], %i(name value), ['First', 1], ['Second', 2], ['Third', 3]] }

  describe '#import' do
    it :success do
      allow(CSV).to receive(:foreach) { true }
      subject.import
    end
    it :errors do
      allow(CSV).to receive(:foreach) do
        subject.errors << { line: 1, message: 'Hello World', exception: { msg: 'Exception' } }
      end
      expect(Rails.logger).to receive(:error).with(/Failed 1 of 0/)
      expect(Rails.logger).to receive(:error).with(/Hello World/)
      expect(Rails.logger).to receive(:error).with(/Failed examples/)
      expect(Rails.logger).to receive(:error).with(/Hello World/)
      subject.import
    end
  end

  describe '#handler_row' do
    it :process_header do
      allow(subject).to receive(:process_header) { |row| row }
      handler_row_response = subject.send :handler_row, sample.first, nil
      expect(handler_row_response).to be sample.first
    end
    it :process_table_header do
      handler_row_response = subject.send :handler_row, sample.second, nil
      expect(handler_row_response).to be_truthy
      expect(subject.header).to be sample.second
    end
    it :process_row do
      allow(subject).to receive(:process_header) { true }
      allow(subject).to receive(:process_row) { |row| @current_row = row }
      sample.each { |row| subject.send :handler_row, row, nil }
      expect(@current_row).to be sample.last
    end
    it :process_row_by_line do
      allow(subject).to receive(:process_header) { true }
      allow(subject).to receive(:process_row) { |row| @current_row = row }
      sample.each { |row| subject.send :handler_row, row, 2 }
      expect(@current_row).to be sample[3]
    end
  end

  describe :not_implemented do
    it '#process_header' do
      expect { subject.send(:process_header, nil) }.to raise_error 'Not Implemented!'
    end
    it '#process_row' do
      expect { subject.send(:process_row, nil) }.to raise_error 'Not Implemented!'
    end
  end

  describe :support_methods do
    it '#migration_success dry_run=false' do
      expect(Rails.logger).to receive(:info).with(/Hello World - SAVED/)
      subject.send :migration_success, 'Hello World'
    end
    it '#migration_success dry_run=true' do
      expect(Rails.logger).to receive(:info).with(/Hello World - OK/)
      subject.instance_variable_set :@dry_run, true
      subject.send :migration_success, 'Hello World'
    end

    it '#migration_fail' do
      expect(Rails.logger).to receive(:error).with(/Hello World - ERROR/)
      subject.send :migration_fail, 'Hello World', msg: 'Exception'
      expect(subject.errors.count).to eq 1
      expect(subject.errors.first[:message]).to eq 'Hello World'
      expect(subject.errors.first[:exception]).to eq msg: 'Exception'
    end

    describe '#to_date' do
      it :success do
        expect(subject.send(:to_date, Date.current.to_s)).to eq Date.current
      end

      it :nil do
        expect(subject.send(:to_date, nil)).to be nil
      end

      it :error do
        expect { subject.send(:to_date, 'Agosto') }.to raise_error 'invalid date - Agosto'
      end
    end
  end
end
