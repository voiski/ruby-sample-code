require 'rails_helper'
require 'migration/parser/location_parser'

RSpec.describe Migration::Parser::LocationParser, type: :lib do
  let(:sample) { { location_name: 'Belo Horizonte (MG)' } }

  it :valid do
    FactoryGirl.create :office, :bh, location: FactoryGirl.create(:location)
    parser = Migration::Parser::LocationParser.new User.new, sample
    parser.parse
    expect(parser.model.location).not_to be_nil
    expect(parser.model.office).not_to be_nil
  end

  it :invalid do
    parser = Migration::Parser::LocationParser.new User.new, sample
    parser.parse
    expect(parser.model.location).to be_nil
    expect(parser.model.office).to be_nil
  end
end
