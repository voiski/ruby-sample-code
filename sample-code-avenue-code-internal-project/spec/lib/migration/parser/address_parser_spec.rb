require 'rails_helper'
require 'migration/parser/address_parser'

RSpec.describe Migration::Parser::AddressParser, type: :lib do
  subject(:parser_with_cep) { Migration::Parser::AddressParser.new User.new, sample }
  subject(:parser_without_cep) { Migration::Parser::AddressParser.new User.new, sample_without_cep }
  let(:sample) { { address: 'Rua Gavea, 50, apto 204 - Jd. America CEP 30421-340' } }
  let(:sample_without_cep) { { address: 'Rua Gavea, 50, apto 204 - Jd. America' } }

  describe '#parse' do
    it :success do
      allow(subject).to receive(:request_address) { { street: 'Rua' } }
      allow(subject).to receive(:handle_missing_address_atributes) {}
      allow(subject).to receive(:parse_address_number) {}
      subject.parse
      expect(subject.model.street).to eq 'Rua'
    end
    it :missing_address do
      parser = Migration::Parser::AddressParser.new User.new, {}
      expect { parser.parse }.to raise_error 'Missing address'
    end
  end

  describe :support_methods do
    let(:splited_address) { sample[:address].split(/[\n,-]/).map(&:strip) }
    describe :handle_missing_address_atributes do
      it :street do
        subject.send :handle_missing_address_atributes, sample, splited_address
        expect(sample[:street]).to eq 'Rua Gavea'
      end
      it :neighborhood do
        splited_address << 'Bairro Neighborhood'
        subject.send :handle_missing_address_atributes, sample, splited_address
        expect(sample[:neighborhood]).to eq splited_address.last
      end
      it :neighborhood_city do
        sample[:city] = 'BH'
        subject.send :handle_missing_address_atributes, sample, splited_address
        expect(sample[:neighborhood]).to eq 'BH'
      end
    end

    describe :parse_address_number do
      it :normal do
        sample[:street] = splited_address.first
        subject.send :parse_address_number, sample, splited_address
        expect(sample[:number]).to eq splited_address.second
        expect(sample[:complement]).to eq splited_address.third
      end
      it :messed_with_the_street do
        sample[:street] = splited_address.first
        splited_address.first << " #{splited_address.second}"
        splited_address.second << '1'
        subject.send :parse_address_number, sample, splited_address
        expect(sample[:number]).to eq '50'
        expect(sample[:complement]).to eq splited_address.second
      end
    end

    describe '#request_address' do
      it :with_cep do
        allow(parser_with_cep).to receive(:address_by_cep) { 'CEP Address' }
        allow(parser_with_cep).to receive(:address_from_google) { 'Google Address' }
        expect(parser_with_cep.send(:request_address)).to eq 'CEP Address'
      end
      it :without_cep do
        allow(parser_without_cep).to receive(:address_by_cep) { 'CEP Address' }
        allow(parser_without_cep).to receive(:address_from_google) { 'Google Address' }
        expect(parser_without_cep.send(:request_address)).to eq 'Google Address'
      end
      it :wrong_format do
        allow(parser_without_cep).to receive(:address_by_cep) { nil }
        allow(parser_without_cep).to receive(:address_from_google) { nil }
        expect { parser_without_cep.parse }.to raise_error 'Wrong address format'
      end
    end

    context 'third party integration' do
      shared_examples 'get address' do |origin|
        it origin.to_s do
          expect(I18n.transliterate(address[:street])).to eq 'Rua Gavea'
          expect(I18n.transliterate(address[:neighborhood])).to eq 'Jardim America'
          expect(address[:city]).to eq 'Belo Horizonte'
          expect(address[:state]).to match(/MG|Minas Gerais/)
          expect(address[:zipcode]).to eq '30421-340'
          expect(address[:country]).to match(/Brasil|Brazil/)
        end
      end

      it_behaves_like 'get address', '#address_by_cep' do
        let(:address) do
          # Copy from the google response, this is a mock to save time and also to avoid network problems
          allow(subject).to receive(:json_client) { AddressMock.cep_payload }
          subject.send :address_by_cep, '30.421-340'
        end
      end

      it_behaves_like 'get address', '#address_by_cep_without_country' do
        let(:address) do
          # Copy from the google response, this is a mock to save time and also to avoid network problems
          allow(subject).to receive(:json_client) { AddressMock.cep_payload 'pais' => nil }
          subject.send :address_by_cep, '30.421-340'
        end
      end

      it_behaves_like 'get address', '#address_from_google' do
        let(:address) do
          # Copy from the google response, this is a mock to save time and also to avoid network problems
          allow(subject).to receive(:json_client) { AddressMock.google_maps_payload }
          subject.send :address_from_google, 'Rua Gavea, Jardim America, Belo Horizonte, Brazil'
        end
      end

      it_behaves_like 'get address', '#address_from_google without location' do
        let(:address) do
          # Copy from the google response, this is a mock to save time and also to avoid network problems
          allow(subject).to receive(:json_client) { AddressMock.google_maps_payload_without_locality }
          subject.send :address_from_google, 'Rua Gavea, Jardim America, Belo Horizonte, Brazil'
        end
      end
    end
  end
end
