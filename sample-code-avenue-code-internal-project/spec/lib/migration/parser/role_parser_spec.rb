require 'rails_helper'
require 'migration/parser/role_parser'

RSpec.describe Migration::Parser::RoleParser, type: :lib do
  shared_examples 'parse role' do |role, is_manager, is_buddy|
    it "#parse role #{role}" do
      parser = Migration::Parser::RoleParser.new User.new, role: role
      parser.parse
      expect(parser.model.company_role).to eq role.to_sym
      expect(parser.model.company_role_manager?).to be is_manager
      expect(parser.model.company_role_buddy?).to be is_buddy
    end
  end

  it_behaves_like 'parse role', 'manager', true, false
  it_behaves_like 'parse role', 'buddy', false, true
  it_behaves_like 'parse role', 'fellow', false, false
end
