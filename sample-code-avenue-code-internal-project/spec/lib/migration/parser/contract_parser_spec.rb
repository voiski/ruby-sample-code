require 'rails_helper'
require 'migration/parser/contract_parser'

RSpec.describe Migration::Parser::ContractParser, type: :lib do
  let(:sample) do
    {
      billable:           'Billable',
      ac_mail:            'avoiski@',
      registry_book_page: '1 / 20',
      start_date:         '2016-Nov-13',
      annual_review_date: 'January',
      contract:           'PF',
      cpf_cnpj:           '*0000000'
    }
  end
  let(:pj_sample) { sample.merge contract: 'PJ' }
  let(:pf_sample) { sample.merge contract: 'PF' }

  context :parse do
    it :success do
      parser = Migration::Parser::ContractParser.new User.new, sample
      parser.parse
      expect(parser.model.billable).to be_truthy
      expect(parser.model.ac_email).to eq 'avoiski@avenuecode.com'
      expect(parser.model.registry_book_page).to eq 120
      expect(Date::MONTHNAMES[parser.model.annual_review_date.month]).to eq 'January'
    end

    it :pj do
      parser = Migration::Parser::ContractParser.new User.new, pj_sample
      parser.parse
      expect(parser.model.is_pf).to be_falsey
      expect(parser.model.cpf).to be_nil
      expect(parser.model.cnpj).to eq '00.000.000/0000-00'
    end

    it :pf do
      parser = Migration::Parser::ContractParser.new User.new, pf_sample
      parser.parse
      expect(parser.model.is_pf).to be_truthy
      expect(parser.model.cpf).to eq '000.000.000-00'
      expect(parser.model.cnpj).to be_nil
    end
  end

  context :missing_fields do
    it :billable do
      sample[:billable] = nil
      parser = Migration::Parser::ContractParser.new User.new, sample
      parser.parse
      expect(parser.model.billable).to be_falsey
    end

    it :registry_book_page do
      sample[:registry_book_page] = nil
      parser = Migration::Parser::ContractParser.new User.new, sample
      parser.parse
      expect(parser.model.registry_book_page).to eq 0
    end

    it :annual_review_date do
      sample[:annual_review_date] = nil
      parser = Migration::Parser::ContractParser.new User.new, sample
      parser.parse
      expect(parser.model.annual_review_date).to eq Date.new(2016, 11, 1)
    end

    it :ac_email do
      sample[:ac_mail] = nil
      parser = Migration::Parser::ContractParser.new User.new, sample
      expect do
        parser.parse
      end.to raise_error 'No ac email'
    end
  end
end
