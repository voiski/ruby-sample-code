require 'rails_helper'
require 'migration/user_migration'
require 'migration/parser/address_parser'

RSpec.describe Migration::UserMigration, type: :lib do
  subject { Migration::UserMigration.new 'sad', true }
  let(:header_row) do
    [
      'Employees',
      'Annual Review',
      'Contract',
      'Start Date',
      'Registry Book / Page',
      'CPF / CNPJ',
      'RG',
      'Birth Date',
      'Phone Number',
      'Address',
      'Passport Number',
      'Has visa?',
      'Access Card (Pin)',
      'Billability',
      'Role',
      'AC E-mail'
    ]
  end
  let(:sample) do
    [
      'Alan Nunes Voiski',
      'November',
      'CLT',
      '13-Nov-2015',
      '5 / 20',
      '*062.059.726-70',
      'MG 12.704.482',
      '21-Feb-1985',
      '55 31 999158584',
      'Rua Gavea, 50, apto 204 - Jd. America CEP 30421-340',
      'FO876652',
      'B1 / B2',
      '15',
      'Billable',
      'fellow',
      'avoiski@'
    ]
  end

  it '#process_header' do
    expect(Rails.logger).to receive(:info).with(/Staff - Admin Info - BH/)
    subject.send :process_header, ['Staff - Admin Info - BH', nil, nil, nil]
    expect(subject.office).to eq 'BH'
  end

  describe '#process_row' do
    before { allow(subject).to receive(:to_hash) { [] } }
    it :success do
      expect(Rails.logger).to receive(:info).with(/Alan Voiski - OK/)
      allow(subject).to receive(:parse) { FactoryGirl.build :user, :reverse }
      subject.send :process_row, ['Alan Voiski', nil, nil, nil]
    end
    it :fail do
      expect(Rails.logger).to receive(:error).with(/Alan Voiski - ERROR/)
      allow(subject).to receive(:parse) { FactoryGirl.build :user, :reverse, birth_date: nil }
      subject.send :process_row, ['Alan Voiski', nil, nil, nil]
    end
    it :fail_dry_run do
      expect(Rails.logger).to receive(:error).with(/Alan Voiski - ERROR/)
      allow(subject).to receive(:parse) { FactoryGirl.build :user, :reverse, birth_date: nil }
      subject.instance_variable_set :@dry_run, true
      subject.send :process_row, ['Alan Voiski', nil, nil, nil]
    end
  end

  it '#to_hash' do
    subject.send :handler_row, header_row, 0
    user_csv = subject.send :to_hash, sample
    expect(user_csv).to eq(full_name:                      'Alan Nunes Voiski',
                           annual_review_date:             'November',
                           contract:                       'CLT',
                           start_date:                     '13-Nov-2015',
                           registry_book_page:             '5 / 20',
                           cpf_cnpj:                       '*062.059.726-70',
                           rg:                             'MG 12.704.482',
                           birth_date:                     '21-Feb-1985',
                           phone_number_one:               '55 31 999158584',
                           address:                        'Rua Gavea, 50, apto 204 - Jd. America CEP 30421-340',
                           passport_number:                'FO876652',
                           has_visa:                       'B1 / B2',
                           badge_number:                   '15',
                           billable:                       'Billable',
                           role:                           'fellow',
                           ac_mail:                        'avoiski@',
                           register_position:              'Mid',
                           nationality:                    'Brazilian',
                           emergency_contact_phone_number: '(31) 2516-1448',
                           emergency_contact_relationship: 'AC',
                           emergency_contact_name:         'AC',
                           skype:                          '_',
                           personal_email:                 'mail@dont.know',
                           location_name:                  'Belo Horizonte (MG)',
                           ctps:                           '__*062.059.726-70',
                           serial:                         '_',
                           pis:                            '_',
                           ctps_state:                     '_')
  end

  context '#parse' do
    let(:user_csv) do
      allow_any_instance_of(Migration::Parser::AddressParser).to receive(:json_client) { AddressMock.cep_payload }
      subject.send :handler_row, header_row, 0
      subject.send :to_hash, sample
    end

    it :new do
      user = subject.send :parse, user_csv
      expect(user).to be_valid
      expect(User.where(id: user.id)).to be_empty
    end

    it :updating_by_cpf do
      db_user = FactoryGirl.create :user, :reverse, cpf: '062.059.726-70'
      user = subject.send :parse, user_csv
      expect(user).to be_valid
      expect(user.id).to eq db_user.id
    end

    it :updating_by_email do
      db_user = FactoryGirl.create :user, :reverse, ac_email: 'avoiski@avenuecode.com'
      user = subject.send :parse, user_csv
      expect(user).to be_valid
      expect(user.id).to eq db_user.id
    end
  end
end
