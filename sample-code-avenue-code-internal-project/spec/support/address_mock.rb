# Thirdparty services valid payloads
module AddressMock
  def self.cep_payload(override={})
    {
      'cep'         => '30421-340',
      'logradouro'  => "Rua Gávea",
      'complemento' => '',
      'bairro'      => "Jardim América",
      'localidade'  => 'Belo Horizonte',
      'uf'          => 'MG',
      'unidade'     => '',
      'ibge'        => '3106200',
      'gia'         => '',
      'pais'        => 'Brasil'
    }.merge override
  end

  # rubocop:disable Metrics/MethodLength
  def self.google_maps_payload
    {
      'results' => [{
        'address_components' => [
          { 'long_name'  => 'Rua Gavea',
            'short_name' => 'R. Gavea',
            'types'      => ['route'] },
          { 'long_name'  => 'Jardim America',
            'short_name' => 'Jardim America',
            'types'      => %w(political sublocality sublocality_level_1) },
          { 'long_name'  => 'Belo Horizonte',
            'short_name' => 'Belo Horizonte',
            'types'      => %w(locality political) },
          { 'long_name'  => 'Belo Horizonte',
            'short_name' => 'Belo Horizonte',
            'types'      => %w(administrative_area_level_2 political) },
          { 'long_name'  => 'Minas Gerais',
            'short_name' => 'MG',
            'types'      => %w(administrative_area_level_1 political) },
          { 'long_name'  => 'Brazil',
            'short_name' => 'BR',
            'types'      => %w(country political) },
          { 'long_name'  => '30421-340',
            'short_name' => '30421-340',
            'types'      => ['postal_code'] }
        ],
        'formatted_address'  => 'R. Gavea - Jardim America, Belo Horizonte - MG, 30421-340, Brazil',
        'place_id'           => 'ChIJOVMiCqmXpgAR5E1nnOI1z0I',
        'types'              => ['route']
      }],
      'status'  => 'OK'
    }
  end

  def self.google_maps_payload_without_locality
    {
      'results' => [{
        'address_components' => [
          { 'long_name'  => 'Rua Gavea',
            'short_name' => 'R. Gavea',
            'types'      => ['route'] },
          { 'long_name'  => 'Jardim America',
            'short_name' => 'Jardim America',
            'types'      => %w(political sublocality sublocality_level_1) },
          { 'long_name'  => 'Belo Horizonte',
            'short_name' => 'Belo Horizonte',
            'types'      => %w(administrative_area_level_2 political) },
          { 'long_name'  => 'Minas Gerais',
            'short_name' => 'MG',
            'types'      => %w(administrative_area_level_1 political) },
          { 'long_name'  => 'Brazil',
            'short_name' => 'BR',
            'types'      => %w(country political) },
          { 'long_name'  => '30421-340',
            'short_name' => '30421-340',
            'types'      => ['postal_code'] }
        ],
        'formatted_address'  => 'R. Gavea - Jardim America, Belo Horizonte - MG, 30421-340, Brazil',
        'place_id'           => 'ChIJOVMiCqmXpgAR5E1nnOI1z0I',
        'types'              => ['route']
      }],
      'status'  => 'OK'
    }
  end
end
