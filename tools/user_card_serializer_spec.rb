require 'rails_helper'
require 'benchmark'
include Benchmark # we need the CAPTION and FORMAT constants

RSpec.describe UserCardSerializer, type: :serializer do
  describe 'performance' do
    it '#to_json 20 users' do
      20.times.each { |_i| FactoryGirl.create :user }

      Benchmark.benchmark(CAPTION, 11, FORMAT, '%UserCard', '%With Scope') do |x|
        user = x.report('User') { User.all.each { |u| UserSerializer.new(u).to_json } }
        card = x.report('UserCard') { User.all.each { |u| UserCardSerializer.new(u).to_json } }
        scope = x.report('With Scope') { User.card_list.each { |u| UserCardSerializer.new(u).to_json } }
        [card / user * 100, scope / user * 100]
      end
    end
  end
end
