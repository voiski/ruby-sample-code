# Embedded Document definitions to solve search in the embedded objects
module EmbeddedDocument
  extend ActiveSupport::Concern

  included do
    include Mongoid::Document

    # Find the object by the given id
    # Ex: EmbeddedModel.find(id)
    def self.find(id)
      raise Mongoid::Errors::InvalidFind, self unless id
      result = nil
      parent_documents.find { |parent| result = find_by_parent id, parent } if BSON::ObjectId.legal? id
      raise Mongoid::Errors::DocumentNotFound.new self, id unless result
      result
    end

    # Find the object by the given id
    # Ex: EmbeddedModel.all
    def self.all
      parent_documents.map { |parent| parent[:klass].all.map(&parent[:embedded_key]) }.flatten
    end

    # Find the object by the given id
    # Ex: EmbeddedModel.where name: 'Value'
    def self.where(options)
      parent_documents.map { |parent| where_by_parent options, parent }.flatten
    end

    private

    def self.parent_documents
      unless @parent_documents
        @parent_documents = []
        reflect_on_all_associations(:embedded_in).map do |parent|
          parent_klass = parent.name.to_s.classify.constantize
          embedded_key = inverse_method parent_klass
          @parent_documents << { klass:        parent_klass,
                                 parent_key:   parent.name,
                                 embedded_key: embedded_key.to_sym }
        end
      end
      @parent_documents
    end

    def self.inverse_method(parent_klass)
      [model_name.plural, model_name.singular].find do |method|
        parent_klass.instance_methods.include? method.to_sym
      end
    end

    def self.find_by_parent(id, parent)
      parent_object = parent[:klass].elem_match(parent[:embedded_key] => { _id: BSON::ObjectId(id) }).first
      return nil unless parent_object

      found_elements = parent_object.send(parent[:embedded_key])
      result = found_elements.find id if found_elements.is_a? Array
      result || found_elements
    end

    def self.where_by_parent(options, parent)
      criteria, options = where_criteria options, parent
      criteria.elem_match(parent[:embedded_key] => options).map do |parent_object|
        found_elements = parent_object.send(parent[:embedded_key])
        result = found_elements.where options if found_elements.is_a? Array
        result || found_elements
      end.flatten
    end

    def self.where_criteria(options, parent)
      criteria = parent[:klass]
      if options.include? parent[:parent_key]
        criteria = criteria.where(options[parent[:parent_key]])
        options.delete parent[:parent_key]
      end
      [criteria, options]
    end
  end
end
