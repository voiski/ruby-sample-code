class QuoteCreator
   PREVIOUS = "previous"

    def self.create_quote(table_hash = {}, options = {})
      hash = EnvironmentSettings.replace_quote_settings(table_hash)
      #replace_master_item_for_update(options[:created_master_item_id],hash) if is_update?(hash)
      contract = override_default_values(hash)
      contract[:master_item_id] = options[:created_master_item_id] || contract[:master_item_id]
      tradestone_contract = build_tradestone_soap_contract(contract)
      tradestone_response = post_contract_to_tradestone(tradestone_contract)
      created_master_item_id, created_offers, created_rfq = get_created_rfq_info(tradestone_response)

      puts "Quote created: #{created_rfq}"
      puts "Master Item Id: " + created_master_item_id

     created_quote_info = {
        :created_rfq_no => created_rfq,
        :created_master_item_id => created_master_item_id,
        :created_offers => created_offers,
        :contract => contract
      }

      created_quote_info
    end

   def self.post_contract_to_tradestone(tradestone_contract)
      TradeStoneWebService.post tradestone_contract
   end

   def self.override_default_values(hash)
     hash.empty? ? FlattenedRfq.create_flat_contract() : FlatContractBuilder.create_modified_flat_contract(hash)
   end

   private
   def self.get_created_rfq_info(tradestone_response)
     created_rfq = TradeStoneResponseHandler.get_rfq_number_from_response(tradestone_response)
     created_master_item_id = TradeStoneResponseHandler.get_master_item_id_from_response(tradestone_response)
     created_offers = TradeStoneResponseHandler.save_offers_from_response(tradestone_response)
     return created_master_item_id, created_offers, created_rfq
   end

   def self.build_tradestone_soap_contract(hash)
     TradeStoneContractBuilder.build(hash)
   end

   def self.replace_master_item_for_update(created_master_item_id, hash = {})
      hash["master_item_id"] = created_master_item_id
    end

    def self.is_update?(hash = {})
      hash["master_item_id"] == PREVIOUS
    end
end