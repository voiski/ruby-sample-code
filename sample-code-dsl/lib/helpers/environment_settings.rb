class EnvironmentSettings
  SETTINGS_TO_REPLACE = ["AUTOMATION CT", "AUTOMATION CT2", "AUTOMATION GP", "AUTOMATION TP"]

  def self.replace_quote_settings(hash = {})
    hash.each do |key, value|
      if value.is_a?(String) && SETTINGS_TO_REPLACE.include?(value.upcase)
        hash[key] = eval(value.upcase.gsub(/ /,"_").strip) || value
      end
    end
    hash
  end
end