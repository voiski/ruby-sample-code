module Helpers
  module HashUtils
    #convert ordinals symbols to cardinal integers
    ORDINALS = {
        :first => 1,
        :second => 2,
        :third => 3,
        :fourth => 4,
        :fifth => 5,
        :sixth => 6,
        :seventh => 7,
        :eighth => 8,
        :ninth => 9,
        :tenth => 10
    }
  end

  module FileUtils
    def self.read_file(path_to_file, file_name=nil)
      file_name == nil ? File.read(path_to_file) : File.read(File.join(path_to_file, file_name))
    end

    def self.get_correct_platform_path(path)
      path.gsub(/\//, File::ALT_SEPARATOR || File::SEPARATOR)
    end

  end

  module WebServiceModelsUtils
    def self.fill_object(properties_hash,object)
      properties_hash.each do |key, value|
        proc = object.method(key.downcase.gsub(/ /, "_") + "=")
        value = Helpers::RandomNumber.generate_int(1000, 1000000000).to_s if value.downcase == "random value"
        proc.call(value)
      end
      object
    end
  end

  module DateUtils
    def self.get_formatted_date(date)
      Chronic.parse(date).strftime('%m/%d/%Y')
      end

    def self.get_date_with_format date, format
      Chronic.parse(date).strftime(format)
    end

    def self.get_utc_date(date)
      Chronic.parse(date).utc.iso8601
    end

    def self.get_ws_formatted_date(date)
      Chronic.parse(date).strftime('%Y-%m-%d')
    end

    def tomorrow
      (Date.today+1).strftime("%m/%d/%Y")
    end
  end

  module RandomNumber
    def self.generate_int(min, max)
       ((rand() * (min + 1)) % max).to_i
    end
  end
end

World(Helpers)
