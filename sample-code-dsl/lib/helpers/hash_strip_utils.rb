module HashStripUtils
  RFQ_KEYS_TO_DELETE = ['tot_wgt', 'wgt_pct', 'tot_meas', 'meas_pct', 'tot_calc_cost',
                        'request_no', 'assoc_id', 'modify_ts', 'expirable_ind', 'status', 'modify_user', 'assortment_ind', 'attachment', 'requested_by', 'notes', 'create_ts', 'create_user', 'offer_no', 'memo3', 'memo4', 'est_calc_cost', 'pattern_alt_desc1', 'sell_channel','offer_whs_ext']
  OPTION_KEYS_TO_DELETE = ['tot_wgt', 'wgt_pct', 'tot_meas', 'meas_pct', 'tot_calc_cost',
                           'cost','est_offer_price','offer_price', 'offer_calc_cost','offer_whs_ext','status_01', 'date2', 'assoc_id', 'modify_ts', 'modify_user', 'offer_no', 'create_ts', 'create_user', 'memo2', 'memo3', 'memo4', 'notes']
  CARRY_OVER_ITEM_CHANNEL_KEYS_TO_DELETE = ['assoc_id', 'modify_ts', 'modify_user', 'lading_point', 'delivery_terms', 'qty_per_pack']
  CARRY_OVER_OPTION_KEYS_TO_DELETE = ['assoc_id', 'modify_ts', 'modify_user', 'offer_no', 'create_ts', 'create_user', 'select_ind', 'date4', 'status_04', 'offer_date', 'effective_date', 'effective_to', 'date1']

  def strip_non_copied_keys(hash, keys_to_ignore)
    if hash.is_a?(Hash)
      hash.delete_if do |key, value|
        if value.is_a?(Array)
          value.each do |element|
            strip_non_copied_keys(element, keys_to_ignore)
          end
        end
        keys_to_ignore.include?(key)
      end
    end
  end
end