# Deprecated: Please use FlattenedRFQ
class FieldNamesHelper

  TS_OPTION_FIELD_NAMES = {
      # Option
      'Option Description' => 'memo1',
      'CT Comments' => 'material_1',
      'Duty %' => 'numbr2',
      'Option Status' => 'status',
      'Vendor code' => 'supplier',
      'Vendor Response Due Date' => 'offer_date',
      'Terms of Sale' =>'delivery_terms',
      'Agent Name' => 'other_party',
      'Ship Mode' => 'trans_mode',
      'Transfer Point'=>'lading_point',
      'Country of Origin' => 'origin_cntry',
      'Costs Valid Till' => 'effective_to',
      'Pack Factor' => 'qty_per_pack',
      'Carton Type' => 'carton_code',
      'Freight Paid By' => 'payment_type',
      'Duty Class. Code' => 'bom_id',
      'Import Category' => 'fold_code',
      'Chief Weight Fiber' => 'packmat_pack',
      'Product Type' => 'packmat_prepack'
  }

  TS_CC_FIELD_NAMES = {
      # CC
      'CC Qty' => 'numbr1',
      'LFL BOM CC #' => 'alt_desc1',
      'On RFQ?' => 'memo1'
  }

  TS_BOM_FIELD_NAMES = {
      # BOM
      'Component Type' => 'component',
      'RD' => 'memo4',
      'Description' => 'description',
      'Quality Details' => 'freetext2',
      'Unit Cost' => 'unit_value',
      'Qty' => 'qty',
      'RD' => 'memo4',
      'Component Sub-Type' => 'inspect_type',
      'UM' => 'meas_um',
      'Wastage %' => 'numbr5',
      'Comments' => 'freetext1'
  }


  def self.ts_option_field_name(field_label)
    TS_OPTION_FIELD_NAMES[field_label] if field_label
  end

  def self.ts_cc_field_name(field_label)
    TS_CC_FIELD_NAMES[field_label] if field_label
  end

  def self.ts_bom_field_name(field_label)
    TS_BOM_FIELD_NAMES[field_label] if field_label
  end

  def self.ts_option_field_label(field_name)
    TS_OPTION_FIELD_NAMES.key(field_name) if field_name
  end

  def self.ts_cc_field_label(field_name)
    TS_CC_FIELD_NAMES.key(field_name) if field_name
  end

  def self.ts_bom_field_label(field_name)
    TS_BOM_FIELD_NAMES.key(field_name) if field_name
  end

end
