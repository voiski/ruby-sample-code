module Helpers
  module ColorCodes
    COLORS = {
        '000000000015' => 'BLUE',
        '000000000014' => 'GREEN',
        '000000000013' => 'YELLOW',
        '000000000016' => 'PINK',
        '000000000017' => 'PURPLE',
        '000000000018' => 'RED',
        '000000000019' => 'RED 1',
        '000000000020' => 'RED 2',
        '000000000021' => 'RED 3',
        '000000000022' => 'RED 4',
        '000000000023' => 'RED 5',
        '000000000024' => 'RED 6',
        '000000000025' => 'RED 7',
        '000000000026' => 'RED 8',
        '000000000027' => 'RED 9',
        '000000000028' => 'RED 10'
    }
  end
end