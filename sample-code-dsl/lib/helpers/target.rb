class Target
  def self.empty_contract
    {:ts => "2013-05-07T15:41:03.432Z",
     :resource => {
         :targets => []
     }
    }
  end

  def self.create_contract(table)
    target = {}
    table.rows_hash.each do |key, value|
      case
        when key == 'inDCDate'
          value = Helpers::DateUtils.get_ws_formatted_date(value)
      end
      target[key.to_sym] = value
    end
    contract = empty_contract
    contract[:resource][:targets][0] = target
    contract
  end
end