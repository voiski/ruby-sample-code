require 'csv'
require 'date'

class Codes
  def self.option_status_codes
    {'NEW' => 'NEW',
     'WORK' => 'WORK',
     'CONFIRM' => 'CFM',
     'EMAILED' => 'EMAILED',
     'PROMOTE TO GP' => 'CTS',
     'RE-REQUEST TO CT' => 'CTREBID',
     'OFFERED' => 'OFFERED',
     'VENDOR REBID' => 'REBID',
     'NON-CONFIRM' => 'NONCFM',
     'CLIENT REJECT' => 'REJECTED',
     'VENDOR REJECT' => 'VREJECT'}
  end

  def self.freight_paid_by
    {'PURCHASER' => 'G',
     'VENDOR' => 'V',
     'PURCHASER/ VENDOR' => 'B',
     'VENDOR PAY 50%' => 'S'
    }
  end

  def self.rfq_status_codes
    {'READY FOR CT' => 'RCT',
     'NEW' => 'NEW',
     'CONFIRMED' => 'CFM',
     'DROPPED' => 'DROPPED',
     'COMPLETED' => 'COMP'}
  end

  def self.market_codes
    {'US' => 1,
     'CANADA' => 4,
     'EU' => 5,
     'JAPAN' => 6,
     'CHINA' => 8}
  end

  def self.brand_codes
    {'BR' => 2,
     'CLIENT' => 1,
     'CLIENT 2' => 3}
  end

  def self.division_codes
    {'OUTLET' => 210,
     'CLIENT' => 111,
     'MENS' => 211}
  end

  def self.department_codes
    {'P WOVEN SHIRTS' => 2174,
     'WOMENS' => 1111,
     'MENS SWEATERS' => 2117}
  end

  def self.class_codes
    {'JACKETS' => 632302,
     'DENIM' => 111100,
     'CTN SWTR' => 211701}
  end

  def self.subclass_codes
    {'BASIC' => 63800300,
     'DENIM' => 11110000,
     'CTN CREW' => 21170101}
  end

  def self.channel_codes
    {'RETAIL' => 1,
     'ONLINE' => 2}
  end

  def self.season_codes
    {
        'HO' => 'HOLIDAY',
        'SU' => 'SUMMER',
        'SP' => 'SPRING',
        'FA' => 'FALL'
    }
  end

  def self.cost_alternative
    {
        '' => '',
        'ALTERNATIVE ACCEPTED' => '3ACCEPTD',
        'ALTERNATIVE REJECTED' => '1REJECTD',
        'ALTERNATIVE PROPOSED' => '2PROPOSD'
    }
  end

  # Place holders to help with the parameters in the scenario creation allowing
  # to use dynamic values instead of fixed ones, like the current year.
  # It will work in three ways:
  # - Without a parameter that return all placeholders, then the key can be used
  #   to get the corresponding value.
  # - A parameter in any case, the method will transform to uppercase to get the
  #   corresponding value.
  # - Using curly braces, the method will remove them by itself.
  # Ex:
  # Codes.place_holders['SAMPLE']
  # Codes.place_holders('Sample')
  # Codes.place_holders('{Sample}')
  def self.place_holders(place_holder=nil)
    @place_holders ||= {
      'CURRENT YEAR' => Date.today.year,
      'SAMPLE'       => 'this is a sample'
    }
    if place_holder
      place_holder = place_holder[1..place_holder.size-2] if place_holder[0]==='{'
      @place_holders[place_holder.upcase]||place_holder
    else
      @place_holders
    end
  end

  # Read a csv file making a cache of this. If the transform_to_hash is true
  # will transform the csv in a array of hashes using the values of first row
  # as keys.
  # Ex.
  # Codes.fixture_csv :name_file_without_extension
  # Codes.fixture_csv :name_file_without_extension, true
  def self.fixture_csv(name,transform_to_hash=false)
    @csv ||= {}
    unless @csv[name]
      @csv[name] = CSV.read("#{File.dirname(__FILE__)}/../../features/fixtures/#{name.to_s}.csv")
      if transform_to_hash
        header = @csv[name].delete_at 0
        header.map!{|key| to_sym key }
        @csv[name].map! do |row|
          hash_row = {}
          header.map.with_index{|key,i| hash_row[key] = row[i] }
          hash_row
        end
      end
    end
    @csv[name]
  end

  # Read a csv file making a cache of this and return the result matched by the
  # filter param.
  # Ex.
  # Codes.fixture_csv_filtered :name_file_without_extension, {:name => 'one', :year => 2015}
  def self.fixture_csv_filtered(name,filter)
    self.fixture_csv name, true
    @csv[name].select do |row|
      finded = true
      filter.each do |key,value|
        finded=row[key]===value
        break unless finded
      end
      finded
    end
  end

  def self.to_sym(key)
    key.downcase.gsub('#','no').gsub(/\W/,'_').to_sym
  end
end
