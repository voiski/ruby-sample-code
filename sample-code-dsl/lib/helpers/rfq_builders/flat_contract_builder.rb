class FlatContractBuilder
  
  def self.create_modified_flat_contract(things_to_modify, flat_contract = FlattenedRfq.create_flat_contract)
    keys_that_require_conversion = %w(brand market channel department division class subclass rfq_status)

    things_to_modify.each do |key, value|
      case
        when key.start_with?('cc')
          add_cc_details(flat_contract, key, value)
        when key.start_with?('bom component')
          add_bom_details(flat_contract, key, value)
        when key.start_with?('option')
          add_option_details(flat_contract, key, value)
        when key == 'lastUpdated'
          value = Helpers::DateUtils.get_utc_date(value)
          flat_contract[key.to_sym] = value
        when key == 'dc_date'
          value = Helpers::DateUtils.get_ws_formatted_date(value)
          flat_contract[key.to_sym] = value
        when keys_that_require_conversion.include?(key)
          flat_contract[key.to_sym] = convert_value(key,value)
        else
          if flat_contract.has_key?(key.to_sym)
            flat_contract[key.to_sym] = value
          else
            flat_contract[:additional_keys].merge!(key => value)
          end
      end
    end
    flat_contract
  end

  def self.delete_from_contract(items_to_delete)
    flat_contract = FlattenedRfq.create_flat_contract
    items_to_delete.each do |item|
      if item == 'bom component description'
        flat_contract[:bom_components].delete :description
      else
        flat_contract.delete item.to_sym
      end
    end
    flat_contract
  end

  private
  def self.get_cc_position(cc_string)
    cc_number = get_regex_position cc_string
    cc_number.to_i - 1
  end

  def self.get_regex_position(string)
    string.match(/(([A-Z]|[a-z])*)(\d+)/)[3..-1][0]
  end

  def self.get_cc_key(key)
    key.gsub(/cc\d+ /, '')
  end

  def self.add_cc_details(flat_contract, key, value)
    cc_position = get_cc_position(key)
    cc_key = get_cc_key(key)
    flat_contract[:ccs] << flat_contract[:ccs][0].dup if flat_contract[:ccs][cc_position].nil?
    flat_contract[:ccs][cc_position][cc_key.to_sym] = value
  end

  def self.add_bom_details(flat_contract, key, value)
    bom_position = get_bom_position(key)
    bom_key = get_bom_key(key)
    bom_details = flat_contract[:bom_components][bom_position]

    if (flat_contract[:bom_components][bom_position].nil?)
      bom_details = flat_contract[:bom_components][0].dup
      bom_details[:id] = rand(9999999).to_s
      flat_contract[:bom_components] << bom_details
    end

    converted_value = value
    converted_value = Codes.cost_alternative[value] if bom_key == "cost_alternative"

    bom_details[bom_key.to_sym] = converted_value
  end

  def self.add_option_details(flat_contract, key, value)
    option_position = get_option_position(key)
    option_key = get_option_key(key)

    default_option = flat_contract[:options][0].dup

    flat_contract[:options] << default_option.dup  if flat_contract[:options][option_position].nil?

    add_price_exceptions(flat_contract[:options][option_position],option_key,value) if option_key.start_with?('price_exception')

    value = Helpers::DateUtils.get_ws_formatted_date(value) if option_key == 'costs_valid_till'
    value = Codes.freight_paid_by[value] if option_key == 'freight_paid_by'
    value = Codes.option_status_codes[value] if option_key == 'status'
    flat_contract[:options][option_position][option_key.to_sym] = value
  end

  def self.add_price_exceptions(option,key,value)
    price_exception_key = get_price_exception_key(key)
    price_exception_position = get_price_exception_position(key)

    price_exception_record = {}
    price_exception_record[price_exception_key.to_sym] = value

    option[:price_exceptions][price_exception_position] ||= {}
    option[:price_exceptions][price_exception_position].merge!(price_exception_record)
  end

  def self.get_option_key(key)
    key.gsub(/option\d+ /, '')
  end

  def self.get_option_position(key)
    option_position = key.slice(/\d+/).to_i
    option_position - 1
  end

  def self.get_price_exception_key(key)
    key.gsub(/price_exception\d+ /, '')
  end

  def self.get_price_exception_position(key)
    option_position = key.slice(/\d+/).to_i
    option_position - 1
  end

  def self.get_bom_position(key)
    bom_position = key.slice(/\d+/).to_i
    bom_position - 1
  end

  def self.get_bom_key(key)
    key.gsub(/bom component\d+ /, '')
  end

  def self.convert_value(key, value)
    value = value.upcase
    code_name = "#{key.downcase}_codes"
    codes_hash = Codes.send(code_name.to_sym)
    codes_hash[value].nil? ? value : codes_hash[value]
  end
end