class CostingIntegrationContractBuilder

  def self.default_costing_integration_contract
    build(FlattenedRfq.create_flat_contract)
  end

  def self.build(flat_contract, additional_keys = {})
    ccs = build_cc_array(flat_contract)
    bom_components = build_bom_component_array(flat_contract)

    resource = {:id => flat_contract[:master_item_id],
                :href => flat_contract[:rfq_href],
                :flow => flat_contract[:flow],
                :season => flat_contract[:season],
                :year => flat_contract[:year],
                :masterStyle => flat_contract[:masterStyle],
                :masterStyleDesc => flat_contract[:masterStyleDesc],
                :item => {
                    :bomNumber => flat_contract[:master_item_id],
                    :href => flat_contract[:item_href],
                    :ccs => ccs
                },
                :description => flat_contract[:item_description],
                :status => flat_contract[:plm_status],
                :category => flat_contract[:category],
                :subcategory => flat_contract[:subcategory],
                :itemType => flat_contract[:itemType],
                :techPack => {
                   :href => flat_contract[:teck_pack_url],
                   :filename => "teckPack.pdf"
                },
                :sketch => {
                    :href => flat_contract[:sketch_url],
                    :filename => "image.jpg"
                },
                :age => flat_contract[:age],
                :gender => flat_contract[:gender],
                :fitType => flat_contract[:fitType],
                :bom => {
                    :id => 'PLM',
                    :lastUpdated => flat_contract[:lastUpdated],
                    :components => bom_components
                },
                :vendor => flat_contract[:vendor]
    }
    resource.delete(:techPack) if flat_contract[:teck_pack_url].nil?
    resource.delete(:sketch) if flat_contract[:sketch_url].nil?
    unless flat_contract[:additional_keys].empty?
      resource = resource.flatten + additional_keys.flatten
      resource = Hash[*resource.flatten]
    end
    contract = {
        :ts => '2013-05-07T15:41:03Z',
        :entityName => 'design',
        :changeNotes => flat_contract[:changeNotes],
        :resource => resource
    }
    process_hash_property contract
  end

  private
  def self.process_hash_property hash
    hash.each do |key, value|
      process_array_property value if value.is_a?(Array)
      process_hash_property value if value.is_a?(Hash)
      hash[key] = value == 'Y' if !value.nil? && %w{Y N}.include?(value)
    end
    hash
  end

  def self.process_array_property property
     property.each do |value|
       process_hash_property value if value.is_a?(Hash)
       process_array_property value if value.is_a?(Array)
     end
  end

  def self.build_cc_array(flat_contract)
    required_cc_attributes = [:bomCCNumber, :description, :status]
    flat_contract[:ccs].each do |cc|
      cc[:bomCCNumber] = cc[:color_code]
      cc[:description] = Helpers::ColorCodes::COLORS[cc[:color_code]] || cc[:colorway_name]
    end
    build_array(flat_contract[:ccs], required_cc_attributes)
  end

  def self.build_bom_component_array(flat_contract)
    required_bom_component_attributes = [:id,
                                         :rdnumber,
                                         :isPrimary,
                                         :type,
                                         :subtype,
                                         :description,
                                         :qualityDetails,
                                         :quantity,
                                         :unitOfMeasure,
                                         :usage]
    build_array(flat_contract[:bom_components], required_bom_component_attributes)
  end

  def self.build_array(array_of_components, required_attributes)
    array = []
    array_of_components.each do |item|
      array << item.select { |key| required_attributes.include?(key) }
    end
    array
  end
end
