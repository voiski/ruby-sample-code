class TradeStoneContractBuilder
  def self.default_tradestone_contract
    build(FlattenedRfq.create_flat_contract)
  end

  def self.build(flat_contract)
    xml_contract = Nokogiri::XML::Document.new
    document = Nokogiri::XML::Node.new('document', xml_contract)
    request = Nokogiri::XML::Node.new('request', xml_contract)
    quote_node = create_quote_node(xml_contract, flat_contract)
    request.add_child(quote_node)
    document.add_child(request)
    xml_contract.add_child(document)
    xml_contract.to_xml
  end

private
  def self.create_quote_node(xml_contract, flat_contract)
    quote = Nokogiri::XML::Node.new('quote', xml_contract)
    add_quote_level_attributes(quote, flat_contract)
    add_tech_pack(quote, xml_contract, flat_contract) if flat_contract[:tech_pack]
    add_sketch(quote, xml_contract, flat_contract) if flat_contract[:image]
    add_rfq_comments(quote,xml_contract,flat_contract)
    add_sell_channels(quote,xml_contract,flat_contract) 
    add_header_item_channels(quote,xml_contract,flat_contract, flat_contract[:options][0])
    bom_components = create_bomd_nodes(xml_contract, flat_contract[:bom_components])
    add_rfq_level_bom(quote, xml_contract, bom_components)
    add_offers(quote, xml_contract, flat_contract[:options], bom_components)
    add_ccs(quote, xml_contract, flat_contract[:ccs])
    quote
  end

  def self.add_header_item_channels(quote, xml_contract, flat_contract, option)
    brand_description = Codes.brand_codes.key(flat_contract[:brand].to_i)
    SellChannelHelper.get_expected_sell_channel_for_brand(brand_description).each do |selling_channel|
      item_channel_node = Nokogiri::XML::Node.new('item_channel',xml_contract)
      item_channel_node.set_attribute('channel',selling_channel.upcase)
      item_channel_node.set_attribute('lading_point',option[:lading_point])
      item_channel_node.set_attribute('duty_pct',option[:duty])
      item_channel_node.set_attribute('trans_mode',option[:trans_mode])
      item_channel_node.set_attribute('delivery_terms',option[:delivery_terms])
      item_channel_node.set_attribute('carton_code', option[:carton_code])
      item_channel_node.set_attribute('qty_per_pack', option[:pack_factor])
      item_channel_node.set_attribute('hts_no',flat_contract[specific_sell_channel_property(selling_channel,"status")] || "ACTIVE")
      item_channel_node.set_attribute('cur_resell_price',flat_contract[specific_sell_channel_property(selling_channel,"cur")] || nil)
      item_channel_node.set_attribute('resell_price',flat_contract[specific_sell_channel_property(selling_channel,"initial_retail_price")] || nil)
      item_channel_node.set_attribute('target_mu_percent',flat_contract[specific_sell_channel_property(selling_channel,"target_imu")] || nil)
      item_channel_node.set_attribute('upcharge1',flat_contract[specific_sell_channel_property(selling_channel,"packaging_upcharge")] || nil)
      quote.add_child(item_channel_node)
    end
  end

  def self.add_option_item_channels(option, xml_contract, flat_contract, option_node)
    brand_description = Codes.brand_codes.key(flat_contract[:brand].to_i)
    SellChannelHelper.get_expected_sell_channel_for_brand(@brand_description).each do |selling_channel|
      item_channel_node = Nokogiri::XML::Node.new('item_channel',xml_contract)
      item_channel_node.set_attribute('channel',selling_channel.upcase)
      item_channel_node.set_attribute('lading_point',option[:lading_point])
      item_channel_node.set_attribute('duty_pct',option[:duty])
      item_channel_node.set_attribute('target_mu_percent',option[:duty] || nil)
      item_channel_node.set_attribute('trans_mode',option[:trans_mode])
      item_channel_node.set_attribute('delivery_terms',option[:delivery_terms])
      item_channel_node.set_attribute('carton_code', option[:carton_code])
      item_channel_node.set_attribute('qty_per_pack', option[:pack_factor])
      item_channel_node.set_attribute('pack_wgt','11.170085')
      item_channel_node.set_attribute('length','22.9375')
      item_channel_node.set_attribute('um_length','IN')
      item_channel_node.set_attribute('width','15.25')
      item_channel_node.set_attribute('um_width','IN')
      item_channel_node.set_attribute('height','11.6875')
      item_channel_node.set_attribute('um_height','IN')
      item_channel_node.set_attribute('pack_meas','0.067006')
      item_channel_node.set_attribute('um_pack_meas','CBM')
      item_channel_node.set_attribute('hts_no',flat_contract[specific_sell_channel_property(selling_channel,"status")] || "ACTIVE")
      add_item_channel_cost_exception(option,xml_contract,item_channel_node)
      option_node.add_child(item_channel_node)
    end
  end

  def self.add_item_channel_cost_exception(option, xml_contract, item_channel_node)
    option[:price_exceptions].each do |price_exception|
      price_exception_node = Nokogiri::XML::Node.new('price_exception',xml_contract)
      price_exception_node.set_attribute('color_code',price_exception[:color_code])
      price_exception_node.set_attribute('price',price_exception[:price] || '0')
      price_exception_node.set_attribute('calc_cost',price_exception[:calc_cost] || "0")
      item_channel_node.add_child(price_exception_node)
    end
  end

  def self.add_quote_level_attributes(quote, flat_contract)
    quote.set_attribute('request_no', flat_contract[:rfq_number])
    quote.set_attribute('season', flat_contract[:season])
    quote.set_attribute('status_06', flat_contract[:year])
    quote.set_attribute('business', flat_contract[:brand])
    quote.set_attribute('class', flat_contract[:class])
    quote.set_attribute('dc_date', flat_contract[:dc_date]) unless flat_contract[:dc_date].nil?
    quote.set_attribute('content_3', flat_contract[:market])
    quote.set_attribute('content_4', flat_contract[:channel])
    quote.set_attribute('dept', flat_contract[:department])
    quote.set_attribute('division', flat_contract[:division])
    quote.set_attribute('item_no', flat_contract[:master_item_id])
    quote.set_attribute('variation_desc', flat_contract[:variation_desc] || flat_contract[:master_item_id])
    quote.set_attribute('commodity_desc',flat_contract[:masterStyleDesc])
    quote.set_attribute('group_no',flat_contract[:masterStyle])
    quote.set_attribute('status', flat_contract[:rfq_status])
    quote.set_attribute('subclass', flat_contract[:subclass])
    quote.set_attribute('content_2', flat_contract[:category_team])
    quote.set_attribute('requested_by', flat_contract[:global_production])
    quote.set_attribute('status_03', flat_contract[:age])
    quote.set_attribute('request_price', flat_contract[:target_auc])
    quote.set_attribute('owned_price', flat_contract[:initial_retail_price])
    quote.set_attribute('target_qty', flat_contract[:planned_quantity])
    quote.set_attribute('buy_program_no', flat_contract[:bundle_number])
    quote.set_attribute('import_cntry', flat_contract[:destination_country])
    quote.set_attribute('final_dest', flat_contract[:destination_port])
    quote.set_attribute('pack_material', flat_contract[:buy_size])

    #changeNotes is firing alert for Style Change when creating a RFQ
    #commenting the line to check if it will be no impact in other tests
    #quote.set_attribute('alt_desc2', flat_contract[:changeNotes])

    quote.set_attribute('request_type', flat_contract[:merch_type])
    quote.set_attribute('body_type', flat_contract[:fitType])
    quote.set_attribute('description', 'ST ENGINEERED STRIPE CARDIGAN')
    quote.set_attribute('memo3', 'JANUARY A')
    quote.set_attribute('response_due_date', flat_contract[:ct_response_due_date]) unless flat_contract[:ct_response_due_date].nil?
    quote.set_attribute('content_1', flat_contract[:subcategory])
    quote.set_attribute('gender_age', flat_contract[:gender])
    quote.set_attribute('memo5', flat_contract[:itemType])
    quote.set_attribute('category', flat_contract[:category])
    quote.set_attribute('bom_id',flat_contract[:duty_class_code])
  end

  def self.add_tech_pack(quote, xml_contract, flat_contract)
    attachment = Nokogiri::XML::Node.new('attachment', xml_contract)
    attachment.set_attribute('attachment_no', 'TECH_PACK')
    attachment.set_attribute('location', flat_contract[:techPackFilename])
    quote.add_child(attachment)
  end

  def self.add_rfq_comments(quote,xml_contract,flat_contract)
    rfq_comments = Nokogiri::XML::Node.new('notes', xml_contract)
    rfq_comments.set_attribute('text',flat_contract[:rfq_comments])
    rfq_comments.set_attribute('note_id','GP/CT_RFQ_CMNTS')
    rfq_comments.set_attribute('model_name','GP/CT_RFQ_CMNTS')
    rfq_comments.set_attribute('match_id','218')
    rfq_comments.set_attribute('match_01','Client')
    quote.add_child(rfq_comments)
  end

  def self.add_sell_channels(quote,xml_contract,flat_contract)
    @brand_description = Codes.brand_codes.key(flat_contract[:brand].to_i)
    SellChannelHelper.get_expected_sell_channel_for_brand(@brand_description).each do |selling_channel|
      sell_channel_node = Nokogiri::XML::Node.new('sell_channel',xml_contract)
      sell_channel_node.set_attribute('selling_channel',selling_channel.upcase)
      sell_channel_property = specific_sell_channel_property(selling_channel, "status")
      sell_channel_node.set_attribute('status',flat_contract[sell_channel_property] || "ACTIVE")
      sell_channel_flow_node = Nokogiri::XML::Node.new('sell_channel_flow',xml_contract)
      sell_channel_flow_node.set_attribute('plan_id','COSTING CCS')
      sell_channel_flow_node.set_attribute('begin_date','2013-11-07')
      sell_channel_node.add_child(sell_channel_flow_node)
      add_sell_channel_ccs(flat_contract, sell_channel_node, xml_contract,selling_channel)
      sell_channel_defn_node = Nokogiri::XML::Node.new('sell_channel_defn',xml_contract)
      sell_channel_defn_node.set_attribute('attrib_name','STORE')
      sell_channel_defn_node.set_attribute('attrib_value','COSTING UNITS')
      sell_channel_node.add_child(sell_channel_defn_node)
      quote.add_child(sell_channel_node)
    end
  end

  def self.add_sell_channel_ccs(flat_contract, sell_channel_node, xml_contract, selling_channel)
    flat_contract[:ccs].each_with_index do |cc, index|
      sell_channel_d_node = Nokogiri::XML::Node.new('sell_channel_d', xml_contract)
      sell_channel_defn_node = Nokogiri::XML::Node.new('sell_channel_defn', xml_contract)
      sell_channel_d_node.set_attribute('row_no', (index + 1).to_s)
      sell_channel_d_node.set_attribute('alloc_by_1', 'COSTING UNITS')
      sell_channel_d_node.set_attribute('alloc_by_2', 'COSTING CCS')
      sell_channel_d_node.set_attribute('alloc_by_3', cc[:color_code])
      sell_channel_d_node.set_attribute('alloc_by_5', '12146,FP,RR,T&amp;R,VMI')
      sell_channel_d_node.set_attribute('memo2',cc[:status])
      sell_channel_d_node.set_attribute('memo3',cc[:on_rfq])
      sell_channel_d_node.set_attribute('alloc_amt', cc[(specific_sell_channel_property(selling_channel, "qty"))])
      sell_channel_defn_node.set_attribute('attrib_name', 'COLOR')
      sell_channel_defn_node.set_attribute('attrib_value', cc[:color_code])
      sell_channel_node.add_child(sell_channel_d_node)
      sell_channel_node.add_child(sell_channel_defn_node)
    end
  end

  def self.specific_sell_channel_property(selling_channel, qty)
    selling_channel.sub(" ", "").sub("-", "_").downcase.concat("_#{qty.to_s}").to_sym
  end

  def self.add_sketch(quote, xml_contract, flat_contract)
    attachment = Nokogiri::XML::Node.new('attachment', xml_contract)
    attachment.set_attribute('attachment_no', 'IMAGE')
    attachment.set_attribute('location', flat_contract[:sketchFilename])
    quote.add_child(attachment)
  end

  def self.create_bomd_nodes(xml_contract, bom_component_array)
    bom_nodes = []
    bom_component_array ||= []
    bom_component_array.each do |bom|
      bomd = Nokogiri::XML::Node.new('bom_d', xml_contract)
      bomd.set_attribute('component', bom[:type])
      bomd.set_attribute('status_01', bom[:isPrimary])
      bomd.set_attribute('description', bom[:description])
      bomd.set_attribute('freetext2', bom[:qualityDetails])
      bomd.set_attribute('meas_um', bom[:unitOfMeasure])
      bomd.set_attribute('memo4', bom[:rdnumber])
      bomd.set_attribute('memo5', bom[:id])
      bomd.set_attribute('status_03', bom[:gauge])
      bomd.set_attribute('memo3', bom[:stitch])
      bomd.set_attribute('material_type', bom[:ends])
      bomd.set_attribute('memo1', bom[:fabric_fob])
      bomd.set_attribute('numbr1', bom[:fabric_cif])
      bomd.set_attribute('numbr5', bom[:wastage])
      bomd.set_attribute('numbr3', bom[:pounds_per_dozen])
      bomd.set_attribute('numbr2', bom[:YY])
      bomd.set_attribute('numbr4', bom[:finance])
      bomd.set_attribute('unit_value', bom[:unit_value])
      bomd.set_attribute('qty', bom[:quantity])
      bomd.set_attribute('extended_value', bom[:extended_value])
      bomd.set_attribute('status', bom[:status])
      bomd.set_attribute('memo6',bom[:fabric_mill])
      bomd.set_attribute('substitution', bom[:cost_alternative])
      bom_nodes << bomd
    end
    bom_nodes
  end

  def self.add_rfq_level_bom(quote, xml_contract, bom_components)
    bom = Nokogiri::XML::Node.new('bom', xml_contract)
    bom.set_attribute('bom_id', 'PLM')
    bom_components.each do |bom_component|
      unique_nokogiri_node_object = bom_component.dup
      bom.add_child(unique_nokogiri_node_object)
    end
    quote.add_child(bom)
  end

  def self.add_offers(quote, xml_contract, options_array, bom_components)
    options_array.each do |option|
      option_node = Nokogiri::XML::Node.new('offer', xml_contract)
      option_node.set_attribute('offer_no', option[:offer_no])
      option_node.set_attribute('memo1',option[:description])
      option_node.set_attribute('origin_cntry', option[:origin_cntry])
      option_node.set_attribute('supplier', option[:vendor])
      option_node.set_attribute('supplier_d', option[:vendor_name])
      option_node.set_attribute('status', option[:status])
      option_node.set_attribute('status_03', option[:gp_initiated])
      option_node.set_attribute('status_01', option[:status_01])
      option_node.set_attribute('status_04', option[:liked_flag])
      option_node.set_attribute('status_05', option[:vendor_option])
      option_node.set_attribute('mode_qlf', option[:auto_relike_flag])
      option_node.set_attribute('date4', option[:liked_date])
      option_node.set_attribute('other_party', option[:agent])
      option_node.set_attribute('other_party_d', option[:agent_name])
      option_node.set_attribute('numbr2', option[:duty])
      option_node.set_attribute('lading_point', option[:lading_point])
      option_node.set_attribute('trans_mode', option[:trans_mode])
      option_node.set_attribute('payment_type', option[:freight_paid_by])
      option_node.set_attribute('carton_code', option[:carton_code])
      option_node.set_attribute('delivery_terms', option[:delivery_terms])
      option_node.set_attribute('effective_to', option[:costs_valid_till])
      option_node.set_attribute('discount_terms', option[:rate_crd_ok])
      option_node.set_attribute('buy_sell_ind', option[:associated_rate_card])
      option_node.set_attribute('memo5', option[:memo5])
      option_node.set_attribute('memo7', option[:garment_volume_tier])
      option_node.set_attribute('memo8', option[:wash_volume_tier])
      option_node.set_attribute('status_02', option[:rate_card_updated])
      option_node.set_attribute('qty_per_pack', option[:pack_factor])
      option_node.set_attribute('memo6',option[:ct])
      option_node.set_attribute('status_01',option[:plm_option])
      option_node.set_attribute('select_ind',option[:rollup])
      option_node.set_attribute('bom_id',option[:duty_class_code])
      option_node.set_attribute('fold_code', option[:import_category])
      option_node.set_attribute('packmat_pack', option[:chief_weight_fiber])
      option_node.set_attribute('packmat_prepack', option[:product_type])
      add_cost_exceptions_to_offer(option_node,option,xml_contract)
      
     add_option_item_channels(option,xml_contract,quote, option_node)
      bom = Nokogiri::XML::Node.new('bom', xml_contract)
      bom.set_attribute('bom_id', 'PLM')
      bom_components.each do |bom_component|
        unique_nokogiri_node_object = bom_component.dup
        bom.add_child(unique_nokogiri_node_object)
      end
      option_node.add_child(bom)
      quote.add_child(option_node)
    end
  end

  def self.add_cost_exceptions_to_offer(option_node,option_record,xml_contract)
    option_record[:price_exceptions].each do |price_exception|
       price_exception_node = Nokogiri::XML::Node.new('price_exception',xml_contract)
       price_exception_node.set_attribute('color_code',price_exception[:color_code])
       price_exception_node.set_attribute('price',price_exception[:price] || '0')
       price_exception_node.set_attribute('calc_cost',price_exception[:calc_cost] || "0")
       price_exception_node.set_attribute('variance1',price_exception[:variance1])
       price_exception_node.set_attribute('variance2',price_exception[:variance2])
       price_exception_node.set_attribute('variance3',price_exception[:variance3])
       price_exception_node.set_attribute('variance4',price_exception[:variance4])
       price_exception_node.set_attribute('variance5',price_exception[:variance5])
       price_exception_node.set_attribute('variance6',price_exception[:variance6])
       option_node.add_child(price_exception_node)
    end
  end

  def self.add_ccs(quote, xml_contract, ccs_array)
    ccs_array.each do |cc|
      cc_node = Nokogiri::XML::Node.new('colorways', xml_contract)
      cc_node.set_attribute('color_code', cc[:color_code])
      cc_node.set_attribute('colorway_name', cc[:colorway_name] || Helpers::ColorCodes::COLORS[cc[:color_code]])
      cc_node.set_attribute('numbr1', cc[:cc_qty])
      cc_node.set_attribute('memo2', cc[:status])
      cc_node.set_attribute('memo1', cc[:on_rfq])
      quote.add_child(cc_node)
    end
  end
end