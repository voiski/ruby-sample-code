module SellChannelHelper
  def self.get_expected_sell_channel_for_brand(brand)
    SellChannelHelper.send("get_expected_for_#{brand.gsub(/ /,"_").downcase}")
  end

  def self.get_expected_for_gap()
    %w(US-RETAIL US-ONLINE CANADA-RETAIL CANADA-ONLINE EU-RETAIL EU-ONLINE CHINA-RETAIL(HKG) CHINA-RETAIL(CHN) JAPAN-RETAIL EU-FRANCHISE)
  end

  def self.get_expected_for_br
    %w(US-RETAIL US-ONLINE CANADA-RETAIL CANADA-ONLINE EU-RETAIL EU-ONLINE JAPAN-RETAIL EU-FRANCHISE)
  end

  def self.get_expected_for_old_navy
    %w(CANADA-ONLINE CANADA-RETAIL CHINA-RETAIL(CHN) JAPAN-RETAIL US-ONLINE US-RETAIL HK-FRANCHISE)
  end

  def self.get_expected_for_gap_outlet
    %w(CANADA-RETAIL CHINA-RETAIL(CHN) EU-RETAIL JAPAN-RETAIL US-RETAIL)
  end

  def self.get_expected_for_brfs
    %w(CANADA-RETAIL JAPAN-RETAIL US-ONLINE US-RETAIL)
  end

  def self.get_expected_for_athleta
    %w(CANADA-RETAIL CANADA-ONLINE US-ONLINE US-RETAIL CHINA-RETAIL(HKG) CHINA-RETAIL(CHN))
  end
end