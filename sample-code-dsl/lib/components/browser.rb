module Components
  module Browser
    COMPONENTS = {
      text:       { html: :input    , klass: Components::TextField, criteria: "input[@type='text' or not(@type)]"},
      textarea:   { html: :textarea , klass: Components::TextField},
      select:     { html: :select   , klass: Components::Select   },
      plain_text: { html: nil       , klass: Components::PlainText},
      link:       { html: :a        , klass: Components::Link     },
      checkbox:   { html: :input    , klass: Components::Checkbox }
    }

    def self.close
      page.driver.quit
    end

    def self.maximize
      page.driver.browser.manage.window.maximize
    end

    def self.get_title
      /<title>([^<]+)<\/title>/.match(page.html)[1].strip
    end

    def self.is_current_page_title_equals?(act_title)
      actual_title = /<title>([^<]+)<\/title>/.match(page.html)[1].strip
      if actual_title != act_title
        raise "Expected pages title to be '#{act_title}' but got '#{actual_title}'"
      end
    end

    # Get the right component inside this xpath first trying to get an input
    # then get a select.
    # Example:
    #   Components::Browser.get_component '//td'
    #   Components::Browser.get_component '//td/%{placeholder}'
    #   Components::Browser.get_component '//td/%{input}[@id='some']', 'input'
    def self.get_component(xpath,placeholder=nil, type=nil, *tags)
      if type != :plain_text && placeholder.nil?
        placeholder = 'placeholder'
        xpath << '//%{placeholder}[1]' unless xpath.include? placeholder
      end

      Capybara.using_wait_time 1 do
        if type
          component = COMPONENTS[type]
          xpath = xpath%{placeholder.to_sym => (component[:criteria]||component[:html]||'.')} if placeholder
          component[:klass].new  xpath, *tags
        elsif page.has_selector?(xpath % {placeholder.to_sym => 'input'})
          puts 'DEPRECATED! Please pass the type!'
          get_component xpath, placeholder, :text
        else
          puts 'DEPRECATED! Please pass the type!'
          get_component xpath, placeholder, :select
        end
      end
    end

  end
end
