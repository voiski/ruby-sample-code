module Components
  class Alert
    def self.accept
      page.driver.browser.switch_to.alert.accept
    end

    def self.dismiss
      page.driver.browser.switch_to.alert.dismiss
    end

    def self.text
      page.driver.browser.switch_to.alert.text
    end

    def self.present?
      begin
        page.driver.browser.switch_to.alert
        true
      rescue
        false
      end
    end
  end
end
