module Components
  class Link < GenericComponent

    def get_text
      get_element.native.text
    end
    alias :value :get_text

    def click
      get_element.click
    end

  end
end