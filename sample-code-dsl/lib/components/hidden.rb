module Components
  class Hidden < GenericComponent
    def get_value
      get_element.value
    end
  end
end