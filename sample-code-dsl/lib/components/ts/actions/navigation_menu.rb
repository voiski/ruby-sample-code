module Components
  module TS
    class NavigationMenu
      include RSpec::Matchers
      include Components::TS::PageDesign::EmbeddedRolesDsl
      attr_reader :name, :links

      def initialize(name, collapsed, &block)
        @name = name
        @collapsed = collapsed
        @links = {}
        instance_eval(&block) if block
      end

      def check_layout
        TS.log self, "Checking #{@name} navigation menu"
        using_wait_time 10 do
          expect(page).to have_selector content_xpath
          expect(page).to have_selector content_xpath, visible: !(@collapsed)
        end
        click # open menu
        @links.values.each(&:check_layout)
        expect(page.all("#{content_xpath}//a", visible: true).count).to eq @links.count
        TS.navigate_to_TS 'dash'
      end

      def click
        component.click
        TS.wait_loading
        self
      end

      def is_expanded?
        page.has_selector? content_xpath, visible: !(@collapsed)
      end

      def content_xpath
        "#{header_xpath}/../../following-sibling::tr/td/div"
      end

      private

      def header_xpath
        "//div[@id='_divNavArea']/table/tbody/tr//table/tbody/tr[1]//div[normalize-space(text()) = '#{@name}']"
      end

      def component
        @component ||= Components::Link.new header_xpath, :visible
      end

      # Just a method for query definition, should not be used in other way.
      def navigation_link(title, &block)
        @links[title] = Components::TS::NavigationLink.new self, title, &block
        define_singleton_method TS.to_sym(title), lambda{ @links[title] }
        define_singleton_method "#{TS.to_sym(title)}!", lambda{ @links[title].click }
      end
    end
  end
end
