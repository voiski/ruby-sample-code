module Components
  module TS
    class ActionButton
      include Components::TS::PageDesign::EmbeddedRolesDsl
      include RSpec::Matchers
      attr_reader :label, :buttons

      def initialize(parent, label, &block)
        @label = label
        @buttons = {}
        instance_eval(&block) if block
      end

      def check_layout
        TS.log self, "Checking #{@label} button"
        expect(page).to have_xpath content_xpath
        expect(page.all(children_xpath).count).to eq @buttons.count unless @buttons.empty?
        @buttons.values.each(&:check_layout)
      end

      def component
        @component ||= Components::Link.new content_xpath, :visible
      end

      def click
        component.click
        TS.wait_loading
        self
      end

      private

      def content_xpath
        unless @content_xpath
          # TODO: We can use the title in this xpath, but we need to change the page definitions
          @content_xpath = "//td[@class='clsCenterButton' and ./label[contains(text(),'#{@label}')]]"
          @content_xpath = "//td[@class='clsCenterButton' and ./label[@title='#{@label}']]" if page.all(@content_xpath, visible: true).count>1
        end
        @content_xpath
      end

      def children_xpath
        "#{content_xpath}/../following-sibling::tr//label"
      end

      # Just an alias for query definition, should not be used in other way.
      def button(label, alias_action=nil)
        @buttons[label] = ActionButton.new self, label
        # TODO: should return the object
        define_singleton_method TS.to_sym(label), lambda { @buttons[label].click }
        define_singleton_method "#{alias_action||TS.to_sym(label)}!" do
          @buttons[label].click
          TS.close_warning
          @buttons[label]
        end
      end

      # Just an alias for query definition, should not be used in other way.
      def button_alias(alias_name, original_label)
        method_alias_name = TS.to_sym alias_name
        method_original_label = TS.to_sym original_label
        define_singleton_method method_alias_name, lambda { self.send(method_original_label)}
        define_singleton_method "#{method_alias_name}!", lambda { self.send("#{method_original_label}!")}
      end

    end
  end
end
