module Components
  module TS
    # Component to handle the result panel actions like export, save the view
    # and save the search. See Components::TS::ResultPanel.
    # Ex:
    # result_header.save_my_view 'My View name'
    # result_header.select_my_view 'My View name'
    # result_header.delete_my_view 'My View name'
    # result_header.save_search 'My Search name'
    # result_header.select_saved_search 'My Search name'
    # result_header.delete_saved_search 'My Search name'
    # result_header.saved_searches
    class ResultHeader
      include Components::WaitTime
      include RSpec::Matchers
      attr_reader :my_views

      def initialize(panel, editable)
        @panel = panel
        @editable = editable
        @my_views = Select.new "#{content_xpath}//select[@id='myviewlist']"
      end

      def export_to_excel
        make_action 'Export to Excel(XLSX Format)'
        # TODO implement the after click
      end

      def export_pdf
        make_action 'Export PDF'
      end

      def save_my_view(name = nil, search_page_only = false)
        TS.close_warning
        make_action 'Save My View'
        page.find_by_id('myviewName').set name if name

        if search_page_only
          page.find("//label[@for='saveToDash']").click
        else
          page.find("//label[@for='saveToDashQue']").click
        end

        page.find_by_id('saveMyViewBtn').click
        using_wait_time 1 do
          page.find_by_id('overrideViewBtn').click if page.has_xpath? "//button[@id='overrideViewBtn']", visible: true
        end
        TS.wait_table_loading
      end

      def select_my_view(name)
        my_views.value = name
        wait_until { page.all("//div[@class='k-loading-image']").count > 0 }
        TS.wait_table_loading
        reload_definitions
      end

      def delete_my_view(name)
        TS.close_warning
        make_action 'Delete My View'
        page.find("//ul/li/label[text()='#{name}']", visible: true).click
        page.find_by_id('delete').click
        TS.wait_table_loading
        page.find_by_id('deleteYes').click
        reload_definitions
      end

      def save_search(name)
        make_action 'Save Search'
        # TODO: capybara already have a timeout, we don't need to use wait_until
        wait_until { page.find_by_id('saveSearchName')}.set name if name
        wait_until { page.find_by_id('saveSearchNameChanged-yes')}.click
      end

      def select_saved_search(name)
        make_action 'Saved Searches'
        # TODO: capybara already have a timeout, we don't need to use wait_until
        wait_until { page.find("//table[@id='searchNameTable']/tbody/tr/td[text()='#{name}']") }.click
      end

      def delete_saved_search(name)
        make_action 'Saved Searches'
        # TODO: capybara already have a timeout, we don't need to use wait_until
        delete(name)
      end

      def delete(name)
        wait_until { page.find("//table[@id='searchNameTable']/tbody/tr/td[text()='#{name}']/following-sibling::td[2]/img[1]", visible: true) }.click
        wait_until { page.find("//span[@id='dialogSavedSearchLayout_wnd_title']/following-sibling::div/a/span[contains(@class,'k-i-close')]", visible: true) }.click
      end

      def delete_all_saved_searches
        make_action 'Saved Searches'
        begin
          using_wait_time 2 do
            saved_searches = page.all("//table[@id='searchNameTable']/tbody/tr/td/following-sibling::td[2]/img/parent::td/parent::tr/td[1]", visible: true)
            if saved_searches.any?
              saved_searches.each do |o|
                delete(o.text)
                sleep 1
              end
              close = wait_until { page.find("//span[@id='dialogSavedSearchLayout_wnd_title']/following-sibling::div/a/span[contains(@class,'k-i-close')]", visible: true) }
              if close
                close.click
              end
            else
              raise Capybara::ElementNotFound
            end
          end
        rescue Capybara::ElementNotFound
          begin
            close = wait_until { page.find("//span[@id='dialogSavedSearchLayout_wnd_title']/following-sibling::div/a/span[contains(@class,'k-i-close')]", visible: true) }
            if close
              close.click
            end
            puts "nothing to delete"
          rescue Capybara::ElementNotFound
            puts "no need to close modal window"
          end
        end
      end

      def saved_searches
        make_action 'Saved Searches'
        values = []
        begin
          using_wait_time 5 do
            no_saved_searches = page.find("//table[@id='searchNameTable']/tbody/tr/th[text()='No saved search records found.']")
          end
        rescue Capybara::ElementNotFound
          no_saved_searches = nil
        end
        unless no_saved_searches
          using_wait_time 5 do
            saved_searches = page.all("//table[@id='searchNameTable']/tbody/tr/td/following-sibling::td[2]/img/parent::td/parent::tr/td[1]")
            saved_searches.each do |o|
              values << o.text
            end
          end
        end
        # TODO: capybara already have a timeout, we don't need to use wait_until
        wait_until { page.find("//span[@id='dialogSavedSearchLayout_wnd_title']/following-sibling::div/a/span[contains(@class,'k-i-close')]", visible: true)}.click
        sleep 1
        values
      end

      def fill_up
        make_action 'Fill Up'
        TS.wait_table_loading
      end

      def fill_down
        make_action 'Fill Down'
        TS.wait_table_loading
      end

      def fill_selected
        make_action 'Fill Selected'
        TS.wait_table_loading
      end

      def check_layout
        check_action 'Export to Excel(XLSX Format)'
        check_action 'Export PDF'
        check_action 'Save My View'
        check_action 'Save Search'
        check_action 'Saved Searches'
        if @editable
          check_action 'Fill Up'
          check_action 'Fill Down'
          check_action 'Fill Selected'
        end
      end

      protected

      def make_action(text)
        using_wait_time 15 do
          TS.wait_grey_overlay_to_be_disabled
          button = page.find "#{content_xpath}//label[text()='#{text}']"
          button.click
        end
      end

      def check_action(text)
        expect(page).to have_xpath "#{content_xpath}//label[text()='#{text}']"
      end

      def content_xpath
        "#{@panel.content_xpath}/preceding-sibling::div[1]"
      end

      def reload_definitions
        TS.wait_table_loading
        @panel.table.reload_columns_definitions
      end

    end
  end
end
