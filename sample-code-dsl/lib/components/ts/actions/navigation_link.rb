module Components
  module TS
    class NavigationLink
      include RSpec::Matchers
      include Components::TS::PageDesign::EmbeddedRolesDsl
      attr_reader :name, :links, :parent

      def initialize(parent, name, &block)
        @parent = parent
        @name = name
        @links = {}
        instance_eval(&block) if block
      end

      def check_layout
        TS.log self, "Checking #{@parent.name}/#{@name} navigation link"
        using_wait_time 2 do
          expect(page).to have_selector content_xpath
        end
      end

      def click
        component.click
        TS.wait_loading
        self
      end

      def content_xpath
        "#{@parent.content_xpath}/..//a[normalize-space(@title) = '#{@name}' or normalize-space() = '#{@name}']"
      end

      private

      def component
        @component ||= Components::Link.new content_xpath, :visible
      end

      # Just a method for query definition, should not be used in other way.
      def navigation_link(title, &block)
        @links[title] = Components::TS::NavigationLink.new self, title, &block
        define_singleton_method TS.to_sym(title), lambda{ @links[title] }
        define_singleton_method "#{TS.to_sym(title)}!", lambda{ @links[title].click }
      end
    end
  end
end
