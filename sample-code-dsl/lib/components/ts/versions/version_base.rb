module Components
  module TS
    module Versions
      # Base class for the version definition. This enables the usage of
      # XPath's by groups and also a group of XPath's by layout version.
      class VersionBase

        def self.included(base)
          base.extend(ClassMethods)
        end

        # Get the group to the given class
        # See Components::TS::Versions#klass_xpaths
        def group_by_klass(klass)
          group_key = klass.ancestors.find{|k| self.class.groups.keys.include? k }
          self.class.groups[group_key] if group_key
        end

        protected

        # Enabled groups, just pass the symbols
        def self.enable(*keys)
          @enable = keys
        end

        # Groups with the XPath's, considering the below example:
        # group :some_key, BaseComponentClass, {
        #   component_as_symbol: {
        #     key: 'xpath_value/%{placeholder}'
        #   }
        # }
        #
        # You can also create a block like
        # group :ex_with_block, BaseComponentClass, {
        #   component_as_symbol: {
        #     key: lambda {|placeholder| someaction(placeholder)}
        #   }
        # }
        def self.group(key, klass, xpaths)
          groups[klass] = xpaths if @enable.include? key
        end

        # It is just to store the groups in a class level
        def self.groups
          @groups||={}
        end
      end
    end
  end
end
