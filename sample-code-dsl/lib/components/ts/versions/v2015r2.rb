module Components
  module TS
    module Versions
      # Definition for the TS version 2015R2
      #
      # To enable put this on the class
      # include Components::TS::Versions
      #
      # Then ask for the XPath like this:
      # klass_xpaths :some_key, placeholder1: value, placeholder2: value
      class V2015R2 < VersionBase

        enable :default, :query, :panel, :section, :modal

        # Default values, normally used on the sections panels
        group :default, Components::TS::CollapsiblePanel, {
          table: {
            locked_columns_xpath:   "%{parent}//table[contains(@id,'left_table')]/tbody/tr[1]/td",
            unlocked_columns_xpath: "%{parent}//table[contains(@id,'right_table')]/tbody/tr[1]/td",
            simple_columns_xpath:   "%{parent}//table[@class='clsrptsectionspacing' or @class='clssubsectionheader']/tbody/tr[1]//td",
            all_rows_xpath:         "%{parent}//td[@class='clslabelheader'][1]/../following-sibling::tr"
          },
          table_column: {
            columns_xpath:  "%{parent}//td[@class='clslabelheader'][1]/../td/label[contains(normalize-space(),'%{label}')]",
            checkbox_xpath: "%{parent}//td[@class='clslabelheader'][1]/../td/input[@type='checkbox']",
            rows_xpath:     "(%{base_row_xpath}[%{row}]/td)[%{col}]"
          },
          embedded_field: {
            header_xpath:       "%{parent}/descendant::label[normalize-space()='%{label}'][%{position}]",
            empty_header_xpath: "%{parent}/descendant::label[%{position}]",
            content_xpath:      "%{header_xpath}/../following-sibling::td[1]/%{html}[1]"
          },
          pagination: {
            content_xpath: "%{parent}//table[@class='clsPaginationHeader']",
            first:         ".//a[contains(text(),1)]",
            next:          ".//a[img[contains(@src,'_next_')]]",
            previous:      ".//a[img[contains(@src,'_prev_')]]"
          }
        }

        # Group for Table panels used on the query pages
        group :panel, Components::TS::TablePanel, {
          table: {
            locked_columns_xpath:   "%{parent}//table[@id='detail_section_left_table']/tbody/tr[1]/td",
            unlocked_columns_xpath: "%{parent}//table[@id='detail_section_right_table']/tbody/tr[1]/td",
            simple_columns_xpath:   "%{parent}//td[@class='clslabelheader'][1]/../td",
            all_rows_xpath:         "%{parent}//td[@class='clslabelheader'][1]/../following-sibling::tr[not(contains(translate(@style, ' ', ''),'display:none'))]"
          }
        }

        # Group for Result panels used on the query pages
        group :query, Components::TS::ResultPanel, {
          table: {
            locked_columns_xpath:   "%{parent}//div[contains(@class,'k-grid-header-locked')]/table/thead/tr/th",
            unlocked_columns_xpath: "%{parent}//div[contains(@class,'k-grid-header-wrap')]/table/thead/tr/th",
            all_rows_xpath:         "%{parent}//div[contains(@class,'k-grid-content')]//tr"
          },
          table_column: {
            columns_xpath: "%{parent}//div[contains(@class,'k-grid-header')]/table/thead/tr//th[@data-title='%{label}']"
          },
          pagination: {
            content_xpath: "//span[@class='k-pager-input k-label']",
            first:         ".//a[contains(text(),'first')]",
            next:          ".//a[contains(text(),'next')]",
            previous:      ".//a[contains(text(),'previous')]",
            last:          ".//a[contains(text(),'last')]"
          }
        }

        # Group for Table panels used on the query pages
        group :section, Components::TS::PanelSection, {
          table: {
            simple_columns_xpath: "%{parent}//table[contains(@id,'left_table')]/tbody/tr[1]/td"
          }
        }


        # Group for SearchField used on the query pages
        group :query, Components::TS::SearchField, {
          embedded_field: {
            header_xpath:  "%{parent}//span[1]",
            content_xpath: "%{parent}//span[2]/%{html}"
          }
        }

        # Group for the default modal component
        group :modal, Components::TS::Modal, {
          table: {
            simple_columns_xpath:   "%{parent}//td[@class='tabDetail']//table//table/tbody/tr[1]//td",
            all_rows_xpath:         "%{parent}//td[@class='clslabelheader'][1]/../following-sibling::tr"
          },
          table_column: {
            columns_xpath:  "%{parent}//td[@class='clslabelheader'][1]/../td/label[contains(normalize-space(),'%{label}')]",
            checkbox_xpath: "%{parent}//td[@class='clslabelheader'][1]/../td/input[@type='checkbox']",
            rows_xpath:     "(%{base_row_xpath}[%{row}]/td)[%{col}]"
          }
        }

      end
    end
  end
end
