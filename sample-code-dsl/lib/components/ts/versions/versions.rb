module Components
  module TS
    # This module enables the version feature on the class that includes it.
    #
    # To eneable put this on the class
    # include Components::TS::Versions
    #
    # Then ask for the XPath like this:
    # klass_xpaths :some_key, placeholder1: value, placeholder2: value
    module Versions

      protected

      def klass_xpaths(key, placeholders={})
        result = group key
        if result.is_a? String
          result % placeholders
        else
          instance_exec placeholders, &result
        end
      end

      private

      def group(key_to_find)
        reference = self
        begin
          @group = inheritance_group reference.class, key_to_find
          break if @group || !reference.respond_to?(:parent)
          reference = reference.parent
        end while reference
        @group[class_as_sym][key_to_find]
      end

      def inheritance_group(reference_class, key_to_find)
        inheritance_group = nil
        while reference_class != Object
          inheritance_group = current_version.group_by_klass reference_class
          break if inheritance_group &&
                   inheritance_group[class_as_sym] &&
                   inheritance_group[class_as_sym][key_to_find]
          reference_class = reference_class.superclass
        end
        inheritance_group
      end

      def current_version
        # Default value
        @current_version ||= Components::TS::Versions::V2015R2.new
      end

      # Helper to transform the class name to a symbol
      def class_as_sym
        @class_as_sym ||= self.class.name.split("::").last.gsub(/([^\^])([A-Z])/,'\1_\2').downcase.to_sym
      end

    end
  end
end
