module Components
  module TS
    # Result column component. With this is possible to retrives the field
    # component by row of this column and also make some actions like sort,
    # hide, freeze and drag to another column.
    # Ex:
    # result_column.sort_ascending
    # result_column.sort_descending
    #
    # result_column.value = 9999
    # expect(result_column.value).to eq 9999
    #
    # result_column.hide_column
    # expect(result_column.visible?).to be false
    # result_column.show_column
    # expect(result_column.visible?).to be true
    #
    # result_column.freeze_column
    # expect(result_column.freezed?).to be true
    # result_column.unfreeze_column
    # expect(result_column.freezed?).to be false
    class TableColumn
      include Components::TS::Versions
      include Components::WaitTime
      include RSpec::Matchers
      attr_reader :parent

      def initialize(parent,label,*tags,&block)
        @parent = parent
        @label = label
        @tags = tags
        @items = []
        @panels = {}

        append_classes = @tags.find{|t| t.is_a?(Hash) && t[:append]}

        instance_eval(&block) if block
        if append_classes
          @tags = @tags - [append_classes]
          [append_classes[:append]].flatten.each{|klass| append klass }
        end
      end

      def drag_column(label_to)
        header_column.drag_to header_column(label_to)
      end

      def sort_ascending
        menu_action 'Sort Ascending'
        TS.wait_table_loading
      end

      def sort_descending
        menu_action 'Sort Descending'
        TS.wait_table_loading
      end

      def hide_column(label=nil)
        if label
          menu_action 'Columns'
          uncheck label
        else
          menu_action "Hide #{column_label}"
        end
      end

      def show_column(label=nil)
        if label
          menu_action 'Columns'
          check label
        else
          first_column = @parent.column_titles[:all].find {|label| !label.empty? }
          @parent.column(first_column).show_column(column_label)
        end
      end

      def visible?
        header_column.visible?
      end

      def readonly?
        if [Components::PlainText,Components::Link].find{|klass| self.field_at_row.is_a? klass }
          true
        else
          self.field_at_row.get_element['readonly'] == 'true'
        end
      end

      def freeze_column
        menu_action 'Freeze'
      end

      def unfreeze_column
        menu_action 'UnFreeze'
      end

      def freezed?
        @parent.column_titles[:locked].include? column_label
      end

      def sorted?
        using_wait_time 10 do
          sort_icon = page.find("#{columns_xpath}/a/span[contains(@class,'k-i-arrow-')]" % {label: column_label}) rescue nil
          sort_icon['class'].include?('k-i-arrow-n') ? :asc : :desc if sort_icon
        end
      end

      def row(row_index=default_row)
        @parent.row row_index
      end

      def field_at_row(row_index=default_row)
        Components::Browser.get_component rows_xpath(row_index), nil, type, :first, *@tags
      end

      def value(row_index=default_row)
        column_value = field_at_row(row_index).value
        if column_value.is_a? String
          column_value.strip
        else
          column_value
        end
      end

      def value=(value,row_index=default_row)
        field_at_row(row_index).value=value
      end

      def click(row_index=default_row)
        field_at_row(row_index).click
      end

      def values
        row_values = []
        rows_details = nil
        begin
          rows_details = @parent.rows_with_content_details
        rescue
          rows_details = @parent.rows_details
        end
        rows_details[:qt_on_page].times.each do |i|
          row_values << value(i + 1)
        end
        row_values
      end

      def check_layout
        TS.log self, "Checking #{@label} - #{type} column"
        field = field_at_row
        field.should_exist!
        expect(field.get_element['readonly']).to eq "true" if @tags.include? :readonly

        TS.log self, "Checking #{@label} - #{type} column items #{@items}" if @items.first
        @items.each{|item| send(item).should_exist! }
      end

      def type
        @type ||= (@tags & Components::Browser::COMPONENTS.keys).first
      end

      # Just an alias for query definition, should not be used in other way.
      def sub_table_panel(title, *tags, &block)
        collapsed = tags.include? :collapsed
        enabled_roles = tags - [:collapsed,:editable]
        method_name = TS.to_sym(title)
        @panels[title] = Components::TS::TablePanel.new self, title, collapsed, false, &block
        define_singleton_method method_name,  lambda{ @panels[title] }
        define_singleton_method 'collapsed?', lambda{ using_wait_time(0){ not send(method_name).visible? } }
        define_singleton_method 'collapse!',  lambda{ click unless collapsed? }
        define_singleton_method 'expand!' do
          if collapsed?
            click
            TS.wait_until{ not collapsed? }
          end
        end
        define_singleton_method :column,      lambda{ |label| send(method_name).table.column(label) }
        define_singleton_method :row,         lambda{ |row_index| send(method_name).table.row(row_index) }
        define_singleton_method :content_xpath,lambda{ rows_xpath(default_row) }
        define_singleton_method :check_layout_panels do
          TS.log self, "Checking #{@label} - #{type} column panels #{@panels.keys}"
          self.click
          @panels.each{|_k,panel| panel.check_layout }
        end
      end

      private

      include Components::TS::PageDesign::ComposeDsl

      def column_label
        @column_label ||= @label.split('::').last
      end

      def default_row
        @default_row ||= 1
      end

      def default_row=(row_index)
        if @default_row != row_index
          @panels.each{|k,v| @panels[k] = v.clone self }
        end
        @default_row = row_index
      end

      def menu_action(text)
        args_find_menu = ["//li[span[text()='#{text}']]", visible: true]

        open_column_menu until using_wait_time(1){ page.has_selector? *args_find_menu }
        page.find(*args_find_menu).click

        TS.wait_table_loading
        @parent.reload_columns_definitions
      end

      def open_column_menu
        page.find("#{columns_xpath}/a[@class='k-header-column-menu']").click
      end

      def header_column(label=column_label)
        page.find(columns_xpath(label))
      end

      def columns_xpath(label=column_label)
        column_xpath = klass_xpaths :checkbox_xpath, parent: @parent.content_xpath, label: label if @type == :checkbox
        column_xpath ||= klass_xpaths :columns_xpath, parent: @parent.content_xpath, label: label
      end

      def rows_xpath(row_index)
        klass_xpaths :rows_xpath, base_row_xpath: @parent.all_rows_xpath, row: row_index, col: @parent.index_of_column_title(@label)
      end

      # Just an alias for query definition, should not be used in other way.
      def item(*tags)
        label = tags.first if tags.first.is_a? String
        type = (tags & Components::Browser::COMPONENTS.keys).first

        position = tags.select{|t| t.is_a?(Hash) && t[:position]}.map{|t|t[:position]}.first
        criteria = if label
                     "contains(text(),'#{label}')"
                   else
                     position||(@items.select{|i| i.start_with? type.to_s}.count+1)
                   end

        method_name = TS.to_sym(label||type).to_s
        method_name << "_#{criteria}" if criteria.is_a?(Integer) && criteria>1
        define_singleton_method method_name do |row_index=default_row|
          Components::Browser.get_component "#{rows_xpath(row_index)}//%{placeholder}[#{criteria}]", nil, type, *tags
        end

        @items << method_name

        define_singleton_method "#{method_name}!", lambda{|row_index| send(method_name,row_index).click} if tags.include? :link
      end
    end
  end
end
