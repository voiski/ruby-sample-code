module Components
  module TS
    # To include the dls table component
    # Ex:
    # include Components::TS::EmbeddedTable
    module EmbeddedTable
      def table
        @table ||= Components::TS::Table.new self
      end

      def row(row_no)
        table.row(row_no)
      end

      def column_by_label(label)
        table.column(label)
      end

      def column_titles
        table.column_titles[:all]
      end

      def rows_details
        table.rows_details
      end

      def rows_with_content_details
        table.rows_with_content_details
      end

      def pagination
        table.pagination
      end

      protected

      # You need to call this method to add a check for each row on the first
      # column. This will enable the method checkbox.
      def has_checkbox(*tags)
        table.add_column :checkbox, :checkbox, *tags
        define_singleton_method :checkbox, lambda { table.column(:checkbox) }
      end

      # You need to call this method to add a sandwich menu for each row. This
      # will enable the method sandwich_menu.
      def sandwich_menu(*tags)
        table.add_column :sandwich_menu, :sandwich_menu, *tags
        define_singleton_method :sandwich_menu, lambda { table.column(:sandwich_menu) }
      end

      # Just an alias for query definition, should not be used in other way.
      def column(label,*tags,&block)
        # If you forget to set freezed column to the last columns, it will save you
        if tags.include?(:freezed) && table.cols_definition && !table.cols_definition.values.last[:freezed?]
          table.cols_definition.each{|_k,definition| definition[:freezed?] = true }
        end
        table.add_column label, *tags, &block
        define_singleton_method TS.to_sym(label), lambda { table.column(label) }
      end

      # Just an alias for query definition, should not be used in other way.
      def readonly?(condition=false)
        condition = yield if block_given?
        :readonly if condition
      end
    end
  end
end
