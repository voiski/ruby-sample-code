module Components
  module TS
    # Result row component. With this is possible to retrieve the row and make
    # some actions like check and uncheck.
    #
    # Ex:
    # expect(result_row.column 'Label 1' ).to eq result_row.label_1
    # expect(result_row                  ).to eq result_row.label_1.row
    #
    # result_row.check
    # expect(result_row.is_checked?).to be true
    # result_row.uncheck
    # expect(result_row.is_checked?).to be false
    class TableRow

      def initialize(table,row)
        @table = table
        @row = row

        table.cols_definition.each do |label,_value|
          unless label.empty?
            define_singleton_method TS.to_sym(label) do
              column(label)
            end
          end
        end

      end

      def columns
        row_columns = {}
        @table.cols_definition.each do |label,_value|
          row_columns[label] = column(label)
        end
        row_columns
      end

      def values
        Hash[columns.map{|k,v| [k, v.value]}]
      end

      def column(label)
        column = @table.column(label).clone
        column.send :default_row=, @row
        column
      end

      def is_checked?
        checkbox.field_at_row.checked?
      end

      def check
        checkbox.field_at_row.check
        self
      end

      def uncheck
        checkbox.field_at_row.uncheck
        self
      end
    end
  end
end
