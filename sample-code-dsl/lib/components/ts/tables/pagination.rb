module Components
  module TS
    # Component class to help table with pagination. This will be created by
    # the table.
    # Ex:
    #    table.pagination.simple_detais[qt_on_page] # will work with any table
    #    table.pagination.detais # complete information, work with the pages that have pagination
    #    table.pagination.go_to(1) # go to the specific page, 1 by default
    #    table.pagination.next # go to the next page
    #    table.pagination.previous # go to the previous page
    class Pagination
      include Components::TS::Versions
      attr_reader :table
      alias :parent :table

      DEFAULT_ROWS_PER_PAGE = 10

      def initialize(table,*tags)
        @table = table
        @tags = tags
      end

      def simple_details
        using_wait_time 0 do
          {
              qt_on_page: page.all(table.all_rows_xpath).count
          }
        end
      end

      def details
        using_wait_time 0 do
          if angular_component
            angular_details
          else
            legacy_details
          end
        end
      end

      def go_to(page=1)
        move_to_page ".//a[text()=#{page}]"
      end

      def first
        move_to_page klass_xpaths(:first)
      end

      def next
        move_to_page klass_xpaths(:next)
      end

      def previous
        move_to_page klass_xpaths(:previous)
      end

      def last
        move_to_page klass_xpaths(:last)
      end

      def content_xpath
        klass_xpaths :content_xpath, parent: @table.parent.send(:header_xpath)
      end

      private

      def move_to_page(sub_xpath)
        page.within(content_xpath) { page.find(sub_xpath).click  }
        TS.close_message :cancel
        TS.wait_loading
      end

      def angular_component
        @angular_component ||= page.find("//span[@class='k-pager-info k-label']") rescue nil
      end

      def legacy_details
        first_column = table.cols_definition.values.first[:column]

        qt_on_page = 0
        rows_per_page.times.each do |r|
          break unless first_column.field_at_row(r+1).exist? && first_column.field_at_row(r+1)['keyinfo'] && qt_on_page += 1
        end

        pages = 1
        page  = 1
        page.within content_xpath do
          pages = page.all('.//td').count - 2
          page  = page.find(".//a[@class='clsAnchorDefault']").text.to_i
        end rescue nil

        result = {
          from: ( page - 1 ) * rows_per_page + 1,
          qt_on_page: qt_on_page,
          page: page,
          pages: pages
        }
        result[:to] = result[:from] - 1 + qt_on_page
        result[:of] = pages == page ? result[:to] : pages * rows_per_page # can't know =(
        result
      end

      def angular_details
        @angular_details = Hash.new(0)

        table_details_label = angular_component.text.match(/(\d+) - (\d+) of (\d+) items/i)
        if table_details_label
          pages = angular_component.text.match(/.*(\d+)/i).captures.map!(&:to_i).first

          rows_details = table_details_label.captures.map!(&:to_i)
          @angular_details[:from]       = rows_details[0]
          @angular_details[:to]         = rows_details[1]
          @angular_details[:of]         = rows_details[2]
          @angular_details[:qt_on_page] = rows_details[1]-rows_details[0]+1
          @angular_details[:page]       = angular_component.find('input').value
          @angular_details[:pages]      = angular_component.text.match(/.*(\d+)/i).captures.map!(&:to_i).first
        end

        @angular_details
      end

      def rows_per_page
        @rows_per_page ||= (@tags.find{|t| t.is_a?(Hash) && t[:rows_per_page]}||{rows_per_page: DEFAULT_ROWS_PER_PAGE})[:rows_per_page]
      end

    end
  end
end
