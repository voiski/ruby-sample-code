module Components
  module TS
    # Component class to help in manipulation of query tables. See Components::TS::ResultPanel.
    # Ex:
    # expect(table.column('Label 1').row 3 ).to eq query_table.row 3
    #
    # expect(table.index_of_column_title 'Label 1'  ).to eq 1
    # expect(table.search_column_title   '# LABEL 1').to eq 'Label 1'
    #
    # expect(table.column_titles[:all]      ).to include 'Label 1'
    # expect(table.column_titles[:locked]   ).to include 'Freezed Column Label'
    # expect(table.column_titles[:unloecked]).to include 'Unfreezed Column Label'
    class Table
      include RSpec::Matchers
      include Components::TS::Versions
      attr_reader :cols_definition, :parent, :pagination

      def initialize(parent,*tags)
        @parent = parent
        @tags = tags
        @pagination = Components::TS::Pagination.new self, *tags
      end

      # Add a column, the tags can be any type of component and :freezed or
      # :unfreezed. The :freezed will indicate that column is by default freezed
      # and in the layout check it will be validated.
      def add_column(label,*tags,&block)
        @cols_definition ||= {}
        type = (tags-[:freezed,:unfreezed]).first
        unless type
          type = :plain_text
          tags << type
        end
        revised_label = handle_label_key_over_duplications label
        @cols_definition[revised_label] = {
          label: label,
          type: type,
          column: Components::TS::TableColumn.new(self,revised_label,*tags,&block),
          freezed?: tags.include?(:freezed)
        }
      end

      def column(label)
        @cols_definition[label][:column]
      end

      # Return the column index(base from 1) searching for the given label.
      def index_of_column_title(label)
        if label.is_a? String
          if label['::']
            labels = label.split('::')
            column_titles[:all].index{|c| c =~ /^#{Regexp.escape(labels.first).gsub '\ ', ' ?'}$/} + labels.count
          else
            column_titles[:all].index{|c| c =~ /^#{Regexp.escape(label).gsub '\ ', ' ?'}$/} + 1
          end
        else
          @cols_definition.keys.index(label) + 1
        end
      end

      def check_layout
        @cols_definition.each do |label,definition|
          columns_to_check = if definition[:freezed?]
                               column_titles[:locked]
                             else
                               column_titles[:unlocked]
                             end
          # When the column uses brake line it merges the words without space
          if label.is_a? String
            expect(columns_to_check).to include /#{Regexp.escape(definition[:label]).gsub '\ ', ' ?'}/
          else
            expect(columns_to_check).to include ''
          end
          column(label).check_layout
        end

        empty_columns = 0
        # Today the TS layout creates a empty column if don't exist a checkbox
        empty_columns += 1 unless @cols_definition[:checkbox]
        # tables with expand column will have a empty column at the end
        empty_columns += 1 if @cols_definition[:expand]

        expect(column_titles[:all].count).to eq @cols_definition.count + empty_columns

        column(:expand).check_layout_panels if @cols_definition[:expand]
      end

      def column_titles
        unless (@column_titles and (not @column_titles[:all].empty?))
          @column_titles = {
              locked: [],
              unlocked: [],
              all: []
          }

          lookups = {}
          if @cols_definition.find{|_k,v| v[:freezed?]}
            lookups[:locked] = locked_columns_xpath
          else
            # No freezed columns, simple table!
            @tags << :simple
          end
          lookups[:unlocked] = unlocked_columns_xpath

          lookups.each do |type,xpath|
            page.all(xpath).each do |column_title|
              @column_titles[type] << column_title.text
            end
          end

          @column_titles[:all] = @column_titles[:locked] + @column_titles[:unlocked]
        end
        @column_titles
      end

      # Some actions will change the default column definition, it method will
      # restart the mapped columns.
      def reload_columns_definitions
        @column_titles=nil
      end

      def row(row_index)
        Components::TS::TableRow.new self, row_index
      end

      def rows_details
        pagination.simple_details
      end

      def rows_with_content_details
        pagination.details
      end

      def content_xpath
        @parent.content_xpath
      end

      def all_rows_xpath
        @all_rows_xpath ||= klass_xpaths :all_rows_xpath, parent: content_xpath
      end

      private

      def handle_label_key_over_duplications(label)
        # if already exist or if already was handled
        if @cols_definition[label] || !@cols_definition.keys.grep(%r(^.+::#{label}$)).empty?
          if @cols_definition[label]
            last_key = nil
            cols_definition_revised = @cols_definition.map do |k,v|
              revised_k = "#{last_key}::#{k}" if k == label
              last_key = k
              [revised_k||k,v]
            end
            @cols_definition = Hash[cols_definition_revised]
          end
          last_label=@cols_definition.keys.last
          "#{last_label}::#{label}"
        else
          label
        end
      end

      def locked_columns_xpath
        @locked_columns_xpath ||= klass_xpaths :locked_columns_xpath, parent: @parent.content_xpath
      end

      def unlocked_columns_xpath
        @unlocked_columns_xpath ||= if @tags.include? :simple
                                      klass_xpaths :simple_columns_xpath, parent: @parent.content_xpath
                                    else
                                      klass_xpaths :unlocked_columns_xpath, parent: @parent.content_xpath
                                    end
      end

    end
  end
end
