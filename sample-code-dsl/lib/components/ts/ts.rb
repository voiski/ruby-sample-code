require 'timeout'
module Components
  # Tradestone components module that fits the custom layouts
  module TS
    # New components from TS
    Components::Browser::COMPONENTS.merge!({
          calendar:      { html: :input    , klass: Components::TS::CalendarField   },
          sandwich_menu: { html: :img      , klass: Components::TS::SandwichMenu    },
          attachment:    { html: :img      , klass: Components::TS::AttachmentField },
          expand:        { html: :img      , klass: Components::Link                },
          label:         { html: :label    , klass: Components::PlainText           , criteria: "label[@class='clsTextLabelNormal']"},
          label_link:    { html: :label    , klass: Components::Link                , criteria: "label[@class='clsTextLabelLink']"},
          lookup:        { html: :input    , klass: Components::TS::LookupField     },
          smart_tag:     { html: :input    , klass: Components::TS::SmartTagField   }
        })

    CHECKBOX_LABEL = 'checkbox'
    SANDWICH_MENU_LABEL = 'sandwich_menu'
    EMPTY_LABEL = '<empty_field>'

    def self.navigate_to_TS(page=nil)
      ts_path = ENVIRONMENT[CURRENT_ENVIRONMENT || "qa"]['tradestone']
      ts_path = ts_path.sub 'login', page.to_s if page
      visit ts_path
    end

    def self.navigate_to_rate_card_upload
      visit(ENVIRONMENT[CURRENT_ENVIRONMENT || "qa"]['rate_card_upload'])
    end

    def self.navigate_to_upload
      visit(ENVIRONMENT[CURRENT_ENVIRONMENT || "qa"]['targets_upload'])
    end

    def self.navigate_to_microstrategy
      visit(ENVIRONMENT[CURRENT_ENVIRONMENT || "qa"]['microstrategy'])
    end

    def self.navigate_to_plm
      visit(ENVIRONMENT[CURRENT_ENVIRONMENT || 'qa']['plm'])
    end

    # Just blur the current field.
    # Ex: Components::TS.on_blur
    def self.on_blur
      using_wait_time 1 do
        page.find(PageModule::Versions.footer_area).click
      end
    end

    # Close message popups that appear during some actions. Call this on your
    # method after some action.
    # Ex:
    #     TS.close_message
    #     TS.close_message :ok
    #     TS.close_message :cancel
    #     TS.close_message 'Custom Action'
    def self.close_message(action=:close)
      using_wait_time 1 do
        msg_dialog = page.find("//*[@id='msgDiv']") rescue nil
        if msg_dialog
          action_xpath =  case action
                          when :close
                            ".//img[@id='_closeMsg']"
                          when :ok
                            ".//span[@class='clsokcancel' and text()='Ok']"
                          when :cancel
                            ".//span[@class='clsokcancel' and text()='Cancel']"
                          else
                            ".//span[text()='#{action}']"
                          end
          msg_dialog.find(action_xpath).click
        end
      end
    end

    # Close warnings popups that appear during some actions. Call this on your
    # method after some action.
    # Ex: TS.close_warning
    def self.close_warning
      close_message :close
    end

    # Wait until the loading disappear avoid to let the proccess run before the
    # page is done.
    # Ex: TS.wait_loading
    def self.wait_loading(timeout=Capybara.default_max_wait_time)
      sleep 0.3
      TS.wait_until(timeout) { page.all("/html/body/div[@id='proc']").count == 0 }
    end

    # Wait until the table loading disappear avoid to let the proccess run
    # before the page is done.
    # Ex: TS.wait_table_loading
    def self.wait_table_loading(timeout=Capybara.default_max_wait_time)
      TS.wait_loading timeout
      TS.wait_until(timeout) { page.all("//div[@class='k-loading-image']").count == 0 }
    end

    # Wait until the grey overlay is disabled
    # Ex TS.wait_grey_overlay_to_be_disabled
    def self.wait_grey_overlay_to_be_disabled(timeout=Capybara.default_max_wait_time)
      TS.wait_until(timeout) { page.all("//div[contains(@class,'k-overlay') and contains(@style,'block')]").count == 0 }
    end


    # Wait until the condition is false, with the given timeout where the default
    # is the default value for capybara
    # Ex: TS.wait_until { page.all(xpath).count > 0 }
    def self.wait_until(timeout=Capybara.default_max_wait_time)
      result=nil
      Timeout::timeout timeout do
        using_wait_time 0 do
          sleep 0.3 until result=yield
        end
      end
      result
    end

    def self.get_popup_message
      Components::PlainText.new("//div[@id='msgDiv']/table/tbody/tr[2]/td[2]").get_text.downcase
    end

    # Helper to send log to system output. This will set as prefix the object
    # class name.
    # Ex: TS.log self, "Message of #{some_place_holder}"
    def self.log(obj,text)
      @logger ||= Logger.new(STDOUT)
      @logger.progname = obj.class.name.blue
      @logger.info text.light_blue
    end

    # Helper to send log to system output. This will set as prefix the object
    # class name.
    # Ex: TS.warn self, "Warning message of #{some_place_holder}!"
    def self.warn(obj,text)
      @logger ||= Logger.new(STDOUT)
      @logger.progname = obj.class.name
      @logger.warn text.red
    end

    # Helper to send log to system output. This will set as prefix the object
    # class name.
    # Ex: TS.error self, "Error message of #{some_place_holder}!"
    def self.error(obj,text)
      @logger ||= Logger.new(STDOUT)
      @logger.progname = obj.class.name
      @logger.error text.red
    end

    # In some cases the components will have the same label and the composition
    # will include a numeration in the end to avoid overwrite. This method is to
    # help to select all items by the given key including the duplications.
    # Ex:
    # fields = {'label' => 1, 'label 2' => 2, 'other' => 3}
    # expect(TS.select_all(fields, 'label').count).to eq 2
    def self.select_all(hash,key)
      hash.select{ |hash_key,_value| hash_key =~ /^#{key}( \d+)?$/ }
    end

    # Transform the given key in a symbol replacing any special character to "_"
    # Ex:
    #    TS.to_sym('* Something...') == :something
    #    TS.to_sym('Number #')       == :number_no
    #    TS.to_sym('*Class')         == :clazz
    def self.to_sym(key,prefix=nil)
      key = "#{prefix}_#{key}" if prefix
      return key if key.is_a? Symbol
      result_key = key.downcase           # lower case
                      .gsub('#','no')     # replace # to no
                      .gsub('\n','_')     # replace plain text '\n' to '_'
                      .gsub('&','and')     # replace plain text '\n' to '_'
                      .gsub(/\W/,'_')     # special character to '_'
                      .gsub(/^_*|_*$/,'') # trim '_' at start and end
                      .gsub(/_{2,}/,'_')  # trim extra '_' in the middle
                      .to_sym
      result_key = :clazz if result_key == :class
      result_key
    end

    # Transform the label to be valid at the xpath. The break line will be
    # removed, the html space tag will be transformed to unicode.
    # - '\n' is the html tag representation for break line(<br>)
    # - '\ ' is the html tag represensation for space(&nbsp;)
    # * take care when using with double quotes because the backslash will be
    #   converted, escape it if needed("\\n\\ ").
    # Ex:  TS.parse_label_to_xpath('First\nSecond\ Third') == 'FirstSecond\u00a0Third'
    def self.parse_label_to_xpath(label)
      label.gsub('\n','').gsub('\ ',"\u00a0") rescue label
    end

    # Transform the label skipping the special HTML placeholder to be a
    # normalized key. It will help at the features when we specify a table
    # parameter
    # Ex:  TS.parse_label_to_key('First\nSecond\ Third') == 'First Second Third'
    def self.parse_label_to_key(label)
      label.gsub(/\\n\\ |\\[n ]/,' ') rescue label
    end
  end
end
