module Components
  module TS
    module PageDesign
      # Enable check_layout for panels and also create the methods
      #   :given_panel_name    the own panel component
      #   :column              fast way to access the columns, see Components::TS::TableColumn
      #                          column('Label 2').value = 'New Value'
      #   :column_titles       fast way to retrive all the columns labels
      #   :fill_up             fast way to fill up the fields based on the
      #                        selected row, available only for editable query
      #   :fill_down           fast way to fill down the fields based on the
      #                        selected row, available only for editable query
      #   :fill_selected       fast way to fill the fields based on the selected
      #                        rows, available only for editable query
      #   :pagination          pagination handler object:
      #                          pagination.details        # like :rows_with_content_details
      #                          pagination.details_simple # like :rows_details
      #                          pagination.first          # go to the first page
      #                          pagination.previous       # go to the previous page
      #                          pagination.go_to(number)  # go to the given page number, default 1
      #                          pagination.next           # go to the next page
      #                          pagination.last           # go to the last page
      #   :row                 fast way to access the rows, see Components::TS::TableRow
      #                          row(3).column('Label 2').value = 'New Value'
      #                          row(3).is_checked?
      #   :rows_details        fast way to retrive the number of rows
      #   :rows_with_content_details  fast way to retrive the rows details like number
      #                               of registers, page, etc
      #
      # *   You can pass :collapsed to set the default status of the panel.
      #
      # **  You can pass the valid roles, or use "except:[]", to enable it only
      #     for specific roles. Inside the panels you can also use only_roles or
      #     except_roles to handle that.
      #
      # *** To enable editable actions, the "fill" methods, you need to pass
      #     :editable as a parameter
      #
      #  All the references for column use as default the row 1, but you can
      #  pass the row also:
      #    row(3).column('Label 2').value = 'New Value'  # row 3
      #    row(3).label_2.value = 'New Value'  # row 3
      #    column('Label 2').value = 'New Value', 3  # row 3
      #    column('Label 2').value(2) ==  'Value' # row 2
      #    given_panel_name.label_2.value(5) ==  'Value' # row 5
      #  * If more than one panel table is used, the column|row will overwrite another
      #  * If no freezed column is defined, it will assume that is a simple table
      #
      # Ex:
      #   table_panel 'Given Panel name', :collapsed do
      #     # Enable the checbox in the table, also create the method :checkbox
      #     # Ex: given_panel_name.checkbox.check
      #     # Ex: given_panel_name.checkbox.check
      #     has_checkbox
      #     # Assign a plain text to the table in the freezed part, also create a method :label_1 on
      #     # the result panel. Ex:
      #     #   given_panel_name.label_1.value == 'Line 1'
      #     #   given_panel_name.label_1.value(4) == 'Line 4'
      #     column 'Label 1', :plain_text, :freezed
      #     # plain_text is the defaulat type
      #     column 'Label 2', :freezed
      #     column 'Label 3', :select
      #     ...
      #
      #     role :admin, :gp
      #       # columns for admin and gp
      #     end
      #     excpet_role :vendor
      #       # columns except for vendor
      #     end
      #   end
      #
      #   table_panel 'Panel only for ct', :ct do
      #      ....
      #   end
      #
      #   table_panel 'Panel except for gp', except: :gp do
      #      ....
      #   end
      module TablePanelDsl

        def table_panel(title, *tags, &block)
          collapsed = tags.include? :collapsed
          editable = tags.include?(:editable)
          enabled_roles = tags - [:collapsed,:editable]
          method_name = TS.to_sym(title)
          add_component :panels, enabled_roles, method_name
          define_method method_name do
            panels[title] ||= Components::TS::TablePanel.new self, title, collapsed, editable, &block
          end
          define_method :column,                     lambda{ |label| send(method_name).table.column(label) }
          define_method :row,                        lambda{ |row_index| send(method_name).table.row(row_index) }
          define_method :column_titles,              lambda{ send(method_name).table.column_titles[:all] }
          define_method :rows_details,               lambda{ pagination.simple_details }
          define_method :rows_with_content_details,  lambda{ pagination.details }
          define_method :pagination,                 lambda{ send(method_name).pagination }
          if editable
            define_method :fill_up,           lambda{ send(method_name).fill_up }
            define_method :fill_down,         lambda{ send(method_name).fill_down }
            define_method :fill_selected,     lambda{ send(method_name).fill_selected }
          end
        end

      end
    end
  end
end
