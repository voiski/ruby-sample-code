module Components
  module TS
    module PageDesign
      # This allows block to be executed for specific roles. To enable it on the
      # class inlcude like this:
      #  include Components::TS::PageDesign::EmbeddedRolesDsl
      #
      # Ex:
      # role :ct, :gp do
      #   field 'Label for ct gp', :select
      #   ...
      # end
      #
      # only_role :ct, :tp do
      #   field 'Label for ct tp', :select
      #   ...
      # end
      #
      # except_role :vendor do
      #   field 'Label will not be visible for vendor', :select
      #   ...
      # end
      #
      # field 'Label for ct gp', :select if roles? :ct, :gp
      # field 'Label not for ct gp', :select if not_roles? :ct, :gp
      # field 'Label will not be visible for vendor', :select unless role? :vendor
      module EmbeddedRolesDsl

        protected

        def role(*enabled_roles)
          yield if roles? *enabled_roles
        end
        alias :only_role :role

        def except_role(*disabled_roles)
          yield unless roles? *disabled_roles
        end

        def roles?(*roles)
          roles.include? PageModule::Page.current_role.to_sym
        end
        alias :role? :roles?

        def not_roles?(*roles)
          !roles?(*roles)
        end
        alias :not_role? :not_roles?

      end
    end
  end
end
