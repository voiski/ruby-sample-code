module Components
  module TS
    module PageDesign
      # Enable check_layout for panels and also create the method
      #   :given_label    the own panel component considering the given_label as
      #                   the label converted to symbol
      #   :field          fast way to access the fields cross the panels, ex:
      #                     field('Label 1').value = 'New Value'
      #
      # *  You can pass :collapsed to set the default status of the panel.
      #
      # ** You can pass the valid roles, or use "except:[]", to enable it only
      #    for specific roles. Inside the panels you can also use only_roles or
      #     except_roles to handle that.
      #
      # Ex:
      #   panel 'Option Information', :collapsed do
      #     # Assign a text field to the parent panel, also create a method :year
      #     # Ex: panel_option_information.year.value = 2016
      #     #     panel_option_information.fields['*Year'].input.value = 2016
      #     field '*Year', :text
      #     # Assign a combobox field to the parent panel, also create a method :category
      #     # Ex: panel_option_information.category.value = 'TOP'
      #     field 'Category', :select
      #     # Like year, create a method :bom_no where # will be transformed to "#""
      #     # Ex: panel_option_information.bom_no.value = '0900909'
      #     field 'Bom #', :text
      #
      #     role :admin, :gp
      #       # fields for admin and gp
      #     end
      #     excpet_role :vendor
      #       # fields except for vendor
      #     end
      #   end
      #
      #   panel 'Panel only for ct', :ct do
      #      ....
      #   end
      #
      #   panel 'Panel except for gp', except: :gp do
      #      ....
      #   end
      module SimplePanelDsl

        def panel(label, *tags, &block)
          collapsed = tags.include? :collapsed
          enabled_roles = tags - [:collapsed]
          method_name = TS.to_sym(label)
          add_component :panels, enabled_roles, method_name
          define_method method_name do
            panels[label] ||= Components::TS::SimplePanel.new self, label, collapsed, &block
          end
          define_method :field do |label,type=[]|
            panel_reference = panels.find{|_label, panel| panel.respond_to?(:fields) && panel.fields[label] }
            panel_reference.last.fields[label].input *[type].flatten if panel_reference
          end
        end

      end
    end
  end
end
