module Components
  module TS
    module PageDesign
      # Enable check_layout for panels and also create the methods
      #   :result_panel        the own panel component
      #   :listing_area        alias to :result_panel
      #   :column              fast way to access the columns, see Components::TS::TableColumn
      #                          column('Label 2').value = 'New Value'
      #   :column_titles       fast way to retrive all the columns labels
      #   :delete_my_view      fast way to delete a "my view", pass the name
      #   :delete_saved_search fast way to delete a "search", pass the name
      #   :delete_all_saved_searches fast way to delete all the saved "search"
      #   :export_pdf          export to pdf the current result
      #   :export_to_excel     export to excel the current result
      #   :fill_up             fast way to fill up the fields based on the
      #                        selected row, available only for editable query
      #   :fill_down           fast way to fill down the fields based on the
      #                        selected row, available only for editable query
      #   :fill_selected       fast way to fill the fields based on the selected
      #                        rows, available only for editable query
      #   :pagination          pagination handler object:
      #                          pagination.details        # like :rows_with_content_details
      #                          pagination.details_simple # like :rows_details
      #                          pagination.first          # go to the first page
      #                          pagination.previous       # go to the previous page
      #                          pagination.go_to(number)  # go to the given page number, default 1
      #                          pagination.next           # go to the next page
      #                          pagination.last           # go to the last page
      #   :row                 fast way to access the rows, see Components::TS::TableRow
      #                          row(3).column('Label 2').value = 'New Value'
      #                          row(3).is_checked?
      #   :rows_details        fast way to retrive the rows details like number
      #                        of registers, page, etc
      #   :save_my_view        fast way to save a new "my view"
      #                          save_my_view  # will save with default name
      #                          save_my_view 'Sample 1'  # will save with 'Sample 1'
      #                          save_my_view 'Sample 1', true  # will save only in page,
      #                                                         # default saves on dashboard
      #   :save_search         fast way to save a new "search", pass the name
      #   :select_my_view      fast way to select a "my view", pass the name
      #   :select_saved_search fast way to select a "search", pass the name
      #
      # * You can use only_roles or except_roles, inside the panel, to handle
      #   the visible columns for specific roles.
      #
      # ** To enable editable actions, the "fill" methods, you need to pass
      #    :editable as a parameter
      #
      #  All the references for column use as default the row 1, but you can pass the row also:
      #    row(3).column('Label 2').value = 'New Value'  # row 3
      #    row(3).label_2.value = 'New Value'  # row 3
      #    column('Label 2').value = 'New Value', 3  # row 3
      #    column('Label 2').value(2) ==  'Value' # row 2
      #    result_panel.label_2.value(5) ==  'Value' # row 5
      #
      # Ex:
      #   result_panel do
      #     # Enable the checbox in the table, also create the method :checkbox
      #     # Ex: result_panel.checkbox.check
      #     # Ex: result_panel.checkbox.check
      #     has_checkbox
      #     # Assign a plain text to the table in the freezed part, also create a method :label_1 on
      #     # the result panel. Ex:
      #     #   result_panel.label_1.value == 'Line 1'
      #     #   result_panel.label_1.value(4) == 'Line 4'
      #     column 'Label 1', :plain_text, :freezed
      #     # plain_text is the defaulat type
      #     column 'Label 2', :freezed
      #     column 'Label 3', :select
      #     ...
      #
      #     role :admin, :gp
      #       # columns for admin and gp
      #     end
      #     excpet_role :vendor
      #       # columns except for vendor
      #     end
      #   end
      #
      #   # to say that is editable just use the tag :editable, it will enable the "fill.." buttons
      #   result_panel :editable do
      #    .....
      #   end
      module ResultPanelDsl

        def result_panel(*tags,&block)
          add_component :panels, [:all], :result_panel
          editable = tags.include?(:editable)
          define_method :result_panel do
            panels[:result_panel] ||= Components::TS::ResultPanel.new self, editable, &block
          end
          define_method :listing_area,        lambda{ deprecated :listing_area, :result_panel }
          define_method :column,              lambda{ |label| result_panel.table.column(label) }
          define_method :row,                 lambda{ |row_index| result_panel.table.row(row_index) }
          define_method :column_titles,       lambda{ result_panel.table.column_titles[:all] }
          define_method :rows_details,        lambda{ pagination.details }
          define_method :pagination,          lambda{ result_panel.pagination }
          define_method :save_my_view,        lambda{ |name=nil,search_page_only=false| result_panel.header.save_my_view(name,search_page_only) }
          define_method :select_my_view,      lambda{ |name| result_panel.header.select_my_view(name) }
          define_method :delete_my_view,      lambda{ |name| result_panel.header.delete_my_view(name) }
          define_method :save_search,         lambda{ |name| result_panel.header.save_search(name) }
          define_method :select_saved_search, lambda{ |name| result_panel.header.select_saved_search(name) }
          define_method :delete_saved_search, lambda{ |name| result_panel.header.delete_saved_search(name) }
          define_method :delete_all_saved_searches, lambda{ result_panel.header.delete_all_saved_searches }
          define_method :saved_searches,      lambda{ result_panel.header.saved_searches }
          define_method :export_to_excel,     lambda{ result_panel.header.export_to_excel }
          define_method :export_pdf,          lambda{ result_panel.header.export_pdf }
          if editable
            define_method :fill_up,           lambda{ result_panel.header.fill_up }
            define_method :fill_down,         lambda{ result_panel.header.fill_down }
            define_method :fill_selected,     lambda{ result_panel.header.fill_selected }
          end
        end

      end
    end
  end
end
