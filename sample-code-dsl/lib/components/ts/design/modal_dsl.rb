module Components
  module TS
    module PageDesign
      # Create a modal, you need to call check_layout manually for this one. It also create the methods
      #   :modal_[given_name]  the own modal component
      #   :last_modal          the last created modal
      #
      #  All the references for column use as default the row 1, but you can
      #  pass the row also:
      #    modal.row(3).column('Label 2').value = 'New Value'  # row 3
      #    modal.row(3).label_2.value = 'New Value'  # row 3
      #    modal.column('Label 2').value = 'New Value', 3  # row 3
      #    modal.column('Label 2').value(2) ==  'Value' # row 2
      #
      # Ex:
      #   modal 'Given Modal name' do
      #     # Enable the checbox in the table, also create the method :checkbox
      #     # Ex: modal_given_name.checkbox.check
      #     # Ex: modal_given_name.checkbox.check
      #     has_checkbox
      #     # Assign a plain text to the table in the freezed part, also create a method :label_1 on
      #     # the result panel. Ex:
      #     #   modal_given_name.label_1.value == 'Line 1'
      #     #   modal_given_name.label_1.value(4) == 'Line 4'
      #     column 'Label 1', :plain_text, :freezed
      #     # plain_text is the defaulat type
      #     column 'Label 2', :freezed
      #     column 'Label 3', :select
      #     ...
      #
      #     role :admin, :gp
      #       # columns for admin and gp
      #     end
      #     excpet_role :vendor
      #       # columns except for vendor
      #     end
      #   end
      module ModalDsl

        def modal(title, &block)
          method_name = TS.to_sym(title)
          define_method :last_modal, lambda { @last_modal }
          define_method "modal_#{method_name}" do
            @last_modal = Components::TS::Modal.new title, &block
          end
        end

      end
    end
  end
end
