module Components
  module TS
    module PageDesign
      # Enable check_layout for panels and also create the methods
      #   :search_panel   the own panel component
      #   :search_area    alias to :search_panel
      #   :field          fast way to access the filter fields, ex:
      #                     field('Label 1').value = 'New Value'
      #   :clear_fields   call the action 'Clear Fields'
      #   :search         call the action 'Search'
      #   :show_all       call the action 'Show All'
      #
      # * You can use only_roles or except_roles, inside the panel, to handle
      #   the visible fields for specific roles.
      #
      # Ex:
      #   search_panel do
      #     # Assign a text field to the panel, also create a method :label_1
      #     field 'Label 1', :text
      #     # Assign a select field to the panel, also create a method :label_2
      #     field 'Label 2', :select
      #     ...
      #
      #     role :admin, :gp
      #       # fields for admin and gp
      #     end
      #     excpet_role :vendor
      #       # fields except for vendor
      #     end
      #   end
      module SearchPanelDsl

        def search_panel(&block)
          add_component :panels, [:all], :search_panel
          define_method :search_panel do
            panels[:search_panel] ||= Components::TS::SearchPanel.new self, &block
          end
          define_method :search_area,  lambda{ deprecated :search_area, :search_panel }
          define_method :field,        lambda{ |label| search_panel.search_fields[label].input }
          define_method :clear_fields, lambda{ search_panel.clear_fields }
          define_method :search,       lambda{ search_panel.search }
          define_method :show_all,     lambda{ search_panel.show_all }
        end

      end
    end
  end
end
