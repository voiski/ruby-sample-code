module Components
  module TS
    module PageDesign
      # Append the block of the given class. This is useful to split the
      # complexity of a class and also to promote reuse if a block is common.
      #
      # To write a class you can do in two ways:
      #
      # module ComponentToReuse
      #
      #   CONSTANT_NAME = lambda { |klass| klass.button 'Some label' }
      #
      #   def self.compose
      #     lambda do |klass|
      #       panel 'Some label' do
      #         ...
      #       end
      #     end
      #   end
      #
      # end
      #
      # Then you can append to another class passing the module and a symbol of
      # the constant/method name:
      #
      # class PageExample
      #   ....
      #   append ComponentToReuse, :constant_name  # lower case is better to read
      #   append ComponentToReuse, :compose
      #   append ComponentToReuse  # compose is the default value
      #   ....
      # end
      module ComposeDsl

        def append(reference, compose_block=:compose)
          if reference.respond_to? compose_block
            reference.send(compose_block).call self
          else
            reference.const_get(compose_block.to_s.upcase.to_sym).call self
          end
        end

      end
    end
  end
end
