module Components
  module TS
    module PageDesign
      # Enable check_layout for panels and also create the methods
      #   :given_label       the own panel component considering the given_label
      #                      as the label converted to symbol
      #   :advanced_search   the own panel component, a alias to previous method
      #   :field             fast way to access the fields cross the panels, ex:
      #                        field('Label 1').value = 'New Value'
      #   :clear_fields      get the 'Clear Fields' button component
      #   :clear_fields!     call the action 'Clear Fields'
      #   :cancel            get the 'Cancel' button component
      #   :cancel!           call the action 'Cancel'
      #   :save_search       get the 'Save Search' button component
      #   :save_search!      call the action 'Save Search'
      #   :search            get the 'Search' button component
      #   :search!           call the action 'Search'
      #
      # *   You can pass :collapsed to set the default status of the panel.
      #
      # **  Inside the panel you can use only_roles or except_roles to handle
      #     that.
      #
      # Ex:
      #   advanced_search_panel 'Panel Name' do
      #     role :admin, :gp
      #       section 'Section for admin and gp' do
      #            ... # fields
      #          excpet_role :gp
      #            ... # fields only for admin
      #          end
      #       end
      #     end
      #   end
      module AdvancedSearchPanelDsl

        def advanced_search_panel(name,*tags,&block)
          section_title = "Advanced Search: #{name}"
          sections_panel section_title, *tags do
            instance_eval(&block) if block

            button 'Clear Fields'
            button 'Cancel'
            button 'Save Search'
            button 'Search'
          end
          alias_method  :advanced_search, TS.to_sym(section_title)
          alias_method  TS.to_sym(name), :advanced_search
          define_method :clear_fields, lambda{ advanced_search.clear_fields }
          define_method :clear_fields!,lambda{ advanced_search.clear_fields! }
          define_method :cancel,       lambda{ advanced_search.cancel }
          define_method :cancel!,      lambda{ advanced_search.cancel! }
          define_method :save_search,  lambda{ advanced_search.save_search }
          define_method :save_search!, lambda{ advanced_search.save_search! }
          define_method :search,       lambda{ advanced_search.search }
          define_method :search!,      lambda{ advanced_search.search! }
        end

      end
    end
  end
end
