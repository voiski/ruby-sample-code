module Components
  module TS
    module PageDesign
      # Enable check_layout for title and also create the methods
      #   :titles           retrives a hash with the titles per role
      #   :title            retrives the given title
      #   :get_query_title  alias to :title for compatibility purpose
      #
      # * You can pass the valid roles, or use "except:[]", to enable it only
      #   for specific roles.
      #
      # Ex:
      #   title 'Title Sample'
      #   # Title for a specific role
      #   title 'Title Sample for CT', :ct
      module TitleDsl

        def titles(role)
          if @titles
            @titles[role] || @titles[:all]
          else
            superclass.titles role
          end
        end

        def title(page_title, *roles)
          @titles ||= {all: page_title}
          roles.each{|role| @titles[role] = page_title }
          define_method :title do
            self.class.titles(PageModule::Page.current_role.to_sym)
          end
          define_method :get_query_title, lambda { deprecated :get_query_title, :title }
        end

        def subtitles(role)
          if @subtitles
            @subtitles[role] || @subtitles[:all]
          else
            superclass.subtitles role
          end
        end

        def subtitle(page_subtitle, *roles)
          @subtitles ||= {all: page_subtitle}
          roles.each{|role| @subtitles[role] = page_subtitle }
          define_method :subtitle do
            self.class.subtitles(PageModule::Page.current_role.to_sym)
          end
        end

      end
    end
  end
end
