module Components
  module TS
    module PageDesign
      # Enable check_layout for roles blocking the access for the class.
      # By default it will be :all
      #
      # Ex:
      #   roles :admin, :tc, :tp, :gp
      module RolesDsl

        def roles(*enabled_roles)
          define_method :validate_role do
            unless enabled_roles.include? PageModule::Page.current_role.to_sym
              raise "Invalid Role: \"#{PageModule::Page.current_role}\" !"
            end
          end
        end

      end
    end
  end
end
