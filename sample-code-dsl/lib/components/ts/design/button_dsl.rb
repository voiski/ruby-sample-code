module Components
  module TS
    module PageDesign
      # Enable check_layout for buttons and also create the methods
      #   :button_name_as_symbol   the own button component
      #   :button_name_as_symbol!   make the action :click at the button
      #
      # * You can pass the valid roles, or use "except:[]", to enable it only
      #   for specific roles. Inside the block you can also use only_roles or
      #   except_roles to handle that.
      #
      # You can inheritance the buttons
      #   button_name_as_symbol.click.sub_button_name_as_symbol.click
      #   button_name_as_symbol!.sub_button_name_as_symbol!
      #
      # Ex:
      #   # Create the method :save and :save!
      #   button 'Save'
      #
      #   # Create the method :save and :save! only for admin and gp
      #   button 'New', :admin, :gp
      #
      #   # Create the method :go_to.
      #   button 'Go To...' do
      #     # Create the method :button_1 inside the button :go_to. You can click
      #     # in this using(click returns the own object):
      #     #                 go_to.click.button_1.click
      #     #                 go_to!.button_1!
      #     button 'Button 1'
      #     button 'Button 2'
      #
      #     only_role :ct do
      #       button 'CT Button'
      #     end
      #
      #   end
      module ButtonDsl

        def button(label, *enabled_roles, &block)
          add_component :buttons, enabled_roles, TS.to_sym(label)
          define_method TS.to_sym(label) do
            buttons[label] ||= Components::TS::ActionButton.new self, label, &block
          end
          define_method "#{TS.to_sym(label)}!", lambda{ TS.on_blur; send(TS.to_sym(label)).click }
        end

      end
    end
  end
end
