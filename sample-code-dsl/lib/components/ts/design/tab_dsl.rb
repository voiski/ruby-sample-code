module Components
  module TS
    module PageDesign
      # Enable check_layout for tabs and also create the methods
      #   :tab_[tab_name_as_symbol]   the own panel component
      #   :tabs                       hash of tabs
      #   :switch_to_tab              switch to the given tab by the name
      #
      # * You can pass the valid roles, or use "except:[]", to enable it only
      #   for specific roles.
      #
      # You have three ways to switch between tabs:
      #   tab_tab_name_as_symbol.switch
      #   tabs['Tab name'].switch
      #   switch_to_tab 'Tab name'
      #
      # Ex:
      #   tab 'Tab name'
      #   tab 'Tab that apears only for ct', :ct
      #   tab 'Tab that apears only for gp and admin', :gp, :admin
      module TabDsl

        def tab(label, *tags)
          tab_klass = tags.find{|tag| !tag.is_a?(Hash)&&!tag.is_a?(Symbol) }
          tags.delete(tab_klass)
          enabled_roles = tags
          method_name = "tab_#{TS.to_sym(label)}"
          add_component :tabs, enabled_roles, method_name
          define_method method_name do
            tabs[label] ||= Components::TS::Tab.new self, label, tab_klass
          end
          define_method :tabs, lambda { @tabs ||= {} }
          define_method :switch_to_tab, lambda { |label| (tabs[label]||send("tab_#{TS.to_sym(label)}")).switch }
        end

      end
    end
  end
end
