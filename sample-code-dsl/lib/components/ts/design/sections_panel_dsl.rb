module Components
  module TS
    module PageDesign
      # Enable check_layout for panels and also create the methods
      #   :given_label    the own panel component considering the given_label as
      #                   the label converted to symbol
      #   :field          fast way to access the fields cross the panels, ex:
      #                     field('Label 1').value = 'New Value'
      #   :column         fast way to access the columns, see Components::TS::TableColumn
      #                     column('Label 2').value = 'New Value'
      #   :row            fast way to access the rows, see Components::TS::TableRow
      #                     row(3).column('Label 2').value = 'New Value'
      #                     row(3).is_checked?
      #   :column_titles  fast way to retrive all the columns labels
      #   :rows_details   fast way to retrive the rows details like number of
      #                   registers, page, etc
      #
      # *   You can pass :collapsed to set the default status of the panel.
      #
      # **  You can pass the valid roles, or use "except:[]", to enable it only
      #     for specific roles. Inside the panels you can also use only_roles or
      #     except_roles to handle that.
      #
      # *** The table methods will work only if exists a table section and will
      #     reference always the first if it has more then one.
      #
      # Ex:
      #   sections_panel 'Option Overview', :collapsed do
      #     ...
      #     section 'Information' do
      #       # Assign a text field to the parent section, also create a method :year
      #       # Ex: given_label_panel.information.year.value = 2016
      #       #     given_label_panel.information.fields['*Year'].input.value = 2016
      #       field '*Year', :text
      #       # Assign a combobox field to the parent section, also create a method :category
      #       # Ex: given_label_panel.information.category.value = 'TOP'
      #       field 'Category', :select
      #       # Like year, create a method :bom_no where # will be transformed to "#""
      #       # Ex: given_label_panel.information.bom_no.value = '0900909'
      #       field 'Bom #', :text
      #     ...
      #     end
      #     table_section 'Table Name' do
      #       # Assign a text column to the parent section, also create a method :year
      #       # Ex: given_label_panel.table_name.year.value = 2016
      #       #     given_label_panel.table_name.column('*Year').value = 2016
      #       column '*Year', :text
      #       # Assign a combobox column to the parent section, also create a method :category
      #       # Ex: given_label_panel.table_name.category.value = 'TOP'
      #       column 'Category', :select
      #       # Like year, create a method :bom_no where # will be transformed to "#""
      #       # Ex: given_label_panel.table_name.bom_no.value = '0900909'
      #       column 'Bom #', :text
      #     ...
      #     end
      #
      #     role :admin, :gp
      #       section 'Section for admin and gp' do
      #            ... # fields
      #          excpet_role :gp
      #            ... # fields only for admin
      #          end
      #       end
      #     end
      #   end
      #
      #   sections_panel 'Panel only for ct', :ct do
      #      ....
      #   end
      #
      #   sections_panel 'Panel except for gp', except: :gp do
      #      ....
      #   end
      module SectionsPanelDsl

        def sections_panel(label, *tags, &block)
          collapsed = tags.include? :collapsed
          enabled_roles = tags - [:collapsed]
          method_name = TS.to_sym(label)
          add_component :panels, enabled_roles, method_name

          define_method method_name do
            panels[label] ||= Components::TS::SectionsPanel.new self, label, collapsed, &block
          end

          define_method :field do |label,type=[]|
            panel_reference = panels.find{|_label, panel| panel.respond_to?(:fields) && panel.fields[label] }
            panel_reference.last.fields[label].input *[type].flatten if panel_reference
          end

          # It will overwrite or will be overwritten if:
          # 1 - The page has a panel with table like the table panel
          # 2 - The page has another section panel
          define_method :column,        lambda{ |label| self.send(method_name).first_table_section.column(label) }
          define_method :row,           lambda{ |row_index| self.send(method_name).first_table_section.row(row_index) }
          define_method :column_titles, lambda{ self.send(method_name).first_table_section.column_titles[:all] }
          define_method :rows_details,  lambda{ self.send(method_name).first_table_section.rows_details }
        end

      end
    end
  end
end
