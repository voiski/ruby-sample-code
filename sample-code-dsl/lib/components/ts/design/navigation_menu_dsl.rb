module Components
  module TS
    module PageDesign
      # Enable check_layout for navegation menu and also create the methods
      #   :navegation_menu_name_as_symbol_menu   the own menu component
      #   :navegation_menu_name_as_symbol_menu!  make the action :click at the menu link
      #
      # * You can pass the valid roles, or use "except:[]", to enable it only
      #   for specific roles. Inside the block you can also use only_roles or
      #   except_roles to handle that.
      #
      # You can inheritance the buttons
      #   navegation_menu_name_as_symbol_menu.click.menu_link_name_as_symbol.click
      #   navegation_menu_name_as_symbol_menu!.menu_link_name_as_symbol!
      #
      # Ex:
      #   # Create the method :search_menu and :search_menu!
      #   navigation_menu 'Search' do
      #     # Create the method :vendor inside the :search_menu. You can click
      #     # in this using(click returns the own object):
      #     #                 search_menu.click.vendor.click
      #     #                 search_menu!.vendor!
      #     navigation_link 'Vendor'
      #     navigation_link 'Models' do
      #       navigation_link 'Cost Model'
      #     end
      #
      #     # creates the method :orders_s only for admin.
      #     only_role :admin do
      #       button 'Orders(s)'
      #     end
      #  end
      #
      #  # Create the method :admin_menu and :admin_menu! only for admin
      #  navigation_menu 'Admin', :admin
      module NavigationMenuDsl

        def ordered_menus
          @ordered_menus ||= []
        end

        def navigation_menu(label, *tags, &block)
          ordered_menus << label

          collapsed = tags.include? :collapsed
          enabled_roles = tags - [:collapsed]
          method_name = TS.to_sym("#{label}_menu")
          add_component :nav_menu, enabled_roles, method_name

          define_method method_name do
            nav_menu[label] ||= Components::TS::NavigationMenu.new label, collapsed, &block
          end
          define_method "#{method_name}!", lambda{ send(method_name).click }
        end
      end
    end
  end
end
