module Components
  module TS
    class Tab
      include RSpec::Matchers
      attr_reader :parent, :label

      def initialize(parent, label, tab_klass = nil)
        @parent = parent
        @label = label
        @tab_klass = tab_klass
        @parent.tabs[TS.to_sym label] = self
      end

      def check_layout
        TS.log self, "Checking #{@label} tab"
        using_wait_time 0 do
          expect(page).to have_xpath(header_xpath)
          expect(page).to have_xpath(content_xpath)
        end
      end

      def switch
        tab = page.find(header_xpath, visible: true)
        TS.wait_loading
        if tab.visible?
          tab.click
        else
          # The tab item had to be accessed by JS directly since it is invisible to Capybara
          page.execute_script <<-JS
            field = document.evaluate("//div[@id='Tabs']/table/descendant::label[contains(text(), '#{label}')]/..", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue
            field.click();
          JS
        end
        TS.wait_loading
        @tab_klass.new if @tab_klass
      end

      def content_xpath
        "#{header_xpath}/ancestor::div[2]"
      end

      private

      def header_xpath
        "//div[@id='Tabs']/table/descendant::label[contains(text(), '#{@label}')]/.."
      end

    end
  end
end
