module Components
  module TS
    # Panel of sections
    class SectionsPanel < CollapsiblePanel
      include RSpec::Matchers
      attr_reader :sections, :parent

      def initialize(parent, name, collapsed, &block)
        super
        @sections = {}
        @buttons = []
        instance_eval(&block) if block
      end

      def check_layout
        super
        @sections.values.each(&:check_layout)
        @buttons.each{|label| buttons_labels.keys.include? label }
      end

      def content_xpath
        "#{header_xpath}/tbody/tr[2]"
      end

      def first_table_section
        @first_table_section ||= @sections.values.find{|s| s.is_a? Components::TS::TableSection}
      end

      def fields
        unless @fields
          @fields = {}
          @sections.each{|_t,section| @fields.merge!(section.fields) if section.respond_to? :fields }
        end
        @fields
      end

      protected

      def header_xpath
        @xpath ||= "//table[@class='clssectionheader']/tbody/tr/td[2]/b[normalize-space(text())='#{@name}']/ancestor::table[@class='clsContentSection'][1]"
      end

      def buttons_labels
        unless @buttons_labels
          @buttons_labels = {}
          page.all("#{content_xpath}//td[@class='clsCenterButton']").each{|button| @buttons_labels[button.text.strip] = button['id'] }
        end
        @buttons_labels
      end

      def make_action(text)
        button = page.find "#{content_xpath}//td[@id='#{buttons_labels[text]}']"
        button.click
        TS.wait_loading
      end

      private

      # Just a method for query definition, should not be used in other way.
      def section(title=nil, &block)
        title ||= self.name
        @sections[title] = Components::TS::Section.new self, title, &block
        define_singleton_method TS.to_sym(title), lambda{ @sections[title] }
      end

      # Just a method for query definition, should not be used in other way.
      def table_section(title, &block)
        @sections[title] = Components::TS::TableSection.new self, title, &block
        define_singleton_method TS.to_sym(title), lambda{ @sections[title] }
        # It will overwrite or will be overwritten if:
        # 1 - This section panel has another table section
        define_singleton_method :column,        lambda{ |label| @sections[title].table.column(label) }
        define_singleton_method :row,           lambda{ |row_index| @sections[title].table.row(row_index) }
        define_singleton_method :column_titles, lambda{ @sections[title].table.column_titles[:all] }
        define_singleton_method :pagination,    lambda{ @sections[title].pagination }
        define_singleton_method :rows_details,  lambda{ pagination.details_simple }
        define_singleton_method :rows_with_content_details,  lambda{ pagination.details }
      end

      # Just a method for query definition, should not be used in other way.
      def panel_section(title, enabled_roles, &block)
        @sections[title] = Components::TS::PanelSection.new self, title, enabled_roles, &block
        define_singleton_method TS.to_sym(title), lambda{ @sections[title] }
      end

      # Just a method for query definition, should not be used in other way.
      def button(label)
        @buttons << label
        define_singleton_method "#{TS.to_sym(label)}!" do
          make_action label
          TS.wait_table_loading
        end
      end
    end
  end
end
