module Components
  module TS
    # Panel to handle the result list of a query page. See Components::TS::PageDesign.
    # Ex:
    # result_panel_block = Components::TS::ResultPanel.new do
    #                        has_checkbox
    #                        column 'Label 1', :plain_text, :freezed
    #                        # plain_text is the default type
    #                        column 'Label 2', :freezed
    #                        column 'Label 3', :select
    #                      end
    #
    # expect(result_panel_block.table.column_titles).to eq ['Label 1','Label 2','Label 3']
    #
    # expect(result_panel_block.label_2  ).to eq result_panel_block.table.column 'Label 2'
    # expect(result_panel_block.checkbox ).to eq result_panel_block.table.column ''
    #
    # result_panel_block.header.save_my_view   'My View name'
    # result_panel_block.header.select_my_view 'My View name'
    class ResultPanel < CollapsiblePanel
      include Components::TS::EmbeddedTable
      attr_reader :header

      def initialize(parent, editable, &block)
        super parent, 'Search List'
        @header = ResultHeader.new self, editable
        instance_eval(&block) if block
      end

      def check_layout
        super
        @header.check_layout
        @table.check_layout
      end

      def content_xpath
        "#{header_xpath}/following-sibling::*[last()]"
      end

      def get_number_of_records
        rows_details = nil
        begin
          rows_details = rows_with_content_details
        rescue
          rows_details = @parent.rows_details
        end
        if rows_details
          rows_details[:qt_on_page]
        else
          0
        end
      end

      def data_table_first_row_xpath
        "#{content_xpath}//div[@class='k-grid-content-locked']/table/tbody/tr[1]"
      end

      def wait_query_runtime
        using_wait_time 10 do
          page.find("(//table[@class='k-selectable']/tbody/tr)[1]", visible: true)
        end
      end

      protected

      def header_xpath
        "//b[contains(text(), 'Search List')]/ancestor::div[1]"
      end

    end
  end
end
