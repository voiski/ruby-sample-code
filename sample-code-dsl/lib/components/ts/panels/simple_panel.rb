module Components
  module TS
    # Panel to handle fields of a simple panel. See Components::TS::PageDesign.
    # Ex:
    # panel_block = Components::TS::SearchPanel.new do
    #                 field 'Label 1', :text
    #                 field 'Label 2', :select
    #               end
    #
    # expect(panel_block.field 'Label 2').to eq panel_block.fields['Label 2'].input
    class SimplePanel < CollapsiblePanel
      attr_reader :fields

      def initialize(parent, name, collapsed, &block)
        super
        @fields = {}
        instance_eval(&block) if block
      end

      def check_layout
        super
        @fields.values.each(&:check_layout)
        # This will remove the duplication that happens when the input value is a label
        fields_on_screen = []
        page.all(fields_xpath).each{|field| fields_on_screen << field['name'] }
        expect(fields_on_screen.uniq.length).to eq @fields.length
      end

      def content_xpath
        "#{header_xpath}/tbody/tr[2]"
      end

      protected

      def header_xpath
        "//table[@class='clssectionheader']/tbody/tr/td[2]/b[contains(text(),'#{@name}')]/ancestor::table[@class='clsContentSection'][1]"
      end

      private

      def fields_xpath
        "#{content_xpath}//label[contains(@class,'clsTextLabelNormal') and not(@docviewid)]"
      end

      # Just an alias for query definition, should not be used in other way.
      def field(label,*tags)
        label_key = if @fields[label]
                      position = TS.select_all(@fields,label).count + 1
                      tags << {position: position}
                      "#{label} #{position}"
                    else
                      TS.parse_label_to_key label
                    end
        @fields[label_key] = Components::TS::EmbeddedField.new(self, label, *tags)
        define_singleton_method TS.to_sym(label_key), lambda { @fields[label_key].input }
      end

    end
  end
end
