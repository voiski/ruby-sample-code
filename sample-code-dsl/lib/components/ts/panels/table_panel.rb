module Components
  module TS
    # Panel to handle a table panel. See Components::TS::PageDesign.
    # Ex:
    # panel_block = Components::TS::ResultPanel.new do
    #                 has_checkbox
    #                 column 'Label 1', :plain_text, :freezed
    #                 # plain_text is the default type
    #                 column 'Label 2', :freezed
    #                 column 'Label 3', :select
    #               end
    #
    # expect(panel_block.table.column_titles).to eq ['Label 1','Label 2','Label 3']
    #
    # expect(panel_block.label_2  ).to eq panel_block.table.column 'Label 2'
    # expect(panel_block.checkbox ).to eq panel_block.table.column ''
    class TablePanel < CollapsiblePanel
      include Components::TS::EmbeddedTable

      def initialize(parent, name, collapsed, editable, &block)
        super parent, name, collapsed
        @editable = editable
        if block
          @table_block = block
          instance_eval(&block)
        end
      end

      def fill_up
        make_action 'Fill Up'
        TS.wait_loading
      end

      def fill_down
        make_action 'Fill Down'
        TS.wait_loading
      end

      def fill_selected
        make_action 'Fill Selected'
        TS.wait_loading
      end

      def check_layout
        super
        @table.check_layout
        if @editable
          check_action 'Fill Up'
          check_action 'Fill Down'
          check_action 'Fill Selected'
        end
      end

      def content_xpath
        "#{header_xpath}/tbody/tr[2]"
      end

      def clone(next_parent=nil)
        if next_parent
          TablePanel.new next_parent, name, collapsed, @editable, &@table_block
        else
          super
        end
      end

      protected

      def header_xpath
        unless @header_xpath
          base_xpath  = if parent.is_a? Components::TS::TableColumn
                          "#{parent.content_xpath}/ancestor::tr[1]/following-sibling::tr[1]//table[@class='clssubsectionheader']"
                        else
                          "//table[@class='clssectionheader']"
                        end
          @header_xpath = "#{base_xpath}/tbody/tr/td[2]/b[contains(text(),'#{@name}')]/ancestor::table[@class='clsContentSection'][1]"
        end
        @header_xpath
      end

      private

      def make_action(text)
        using_wait_time 5 do
          button = page.find "#{content_xpath}//table[@class='clssubsectionheader']//label[text()='#{text}']"
          button.click
        end
      end

      def check_action(text)
        expect(page).to have_xpath "#{content_xpath}//table[@class='clssubsectionheader']//label[text()='#{text}']"
      end

    end
  end
end
