module Components
  module TS
    # Base panel with collapse feature
    class CollapsiblePanel
      include RSpec::Matchers
      include Components::TS::PageDesign::EmbeddedRolesDsl
      attr_accessor :collapsed
      attr_reader :name, :parent

      def initialize(parent, name, collapsed=false)
        @parent = parent
        @name = name
        @collapsed = collapsed
      end

      def name=(name)
        old_method_name = TS.to_sym @name
        parent.send :define_singleton_method, TS.to_sym(name), lambda{ self.send(old_method_name) }
        @name=name
      end

      def collapsed?
        not page.find(content_xpath+"/td/div").visible?
      end

      def collapse!
        page.find(header_xpath + "/descendant::img[@src='images/arrowdown.gif']/parent::td").click unless collapsed
        set_collapsed(true)
      end

      def content_xpath
        raise 'Use the concrete subclasses like SectionsPanel/SearchPanel/ResultPanel!'
      end

      def check_layout
        TS.log self, "Checking #{@name} panel"
        expect(page).to have_xpath header_xpath
        expect(page).to have_selector content_xpath, visible: !(@collapsed)
      end

      def expand!
        page.find(header_xpath + "/descendant::img[@src='images/Rarrow.gif']/parent::td").click if collapsed
        set_collapsed(false)
      end

      def set_collapsed(value)
        @collapsed = value
      end

      def visible?
        page.has_selector? header_xpath, visible: true
      end

      protected

      def check_action(text)
        expect(page).to have_xpath "#{content_xpath}//button[text()='#{text}']"
      end

      def header_xpath
        raise 'Use the concrete subclasses like SectionsPanel/SearchPanel/ResultPanel!'
      end

      def make_action(text)
        button = page.find "#{content_xpath}//button[text()='#{text}']"
        button.click
        TS.wait_loading
      end

    end
  end
end
