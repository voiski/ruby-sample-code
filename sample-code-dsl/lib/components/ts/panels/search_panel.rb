module Components
  module TS
    # Panel to handle search fields of a query page. See Components::TS::PageDesign.
    # Ex:
    # search_panel_block = Components::TS::SearchPanel.new do
    #                        field 'Label 1', :text
    #                        field 'Label 2', :select
    #                      end
    #
    # expect(search_panel_block.field 'Label 2').to eq search_panel_block.search_fields['Label 2'].input
    #
    # search_panel_block.search
    # search_panel_block.clear_fields
    # search_panel_block.show_all
    class SearchPanel < CollapsiblePanel
      attr_reader :search_fields, :auto_search

      def initialize(parent, &block)
        super parent, 'Search Fields'
        @search_fields = {}
        @auto_search = true
        instance_eval(&block) if block
      end

      def check_layout
        super
        check_action 'Clear Fields'
        check_action 'Search'
        check_action 'Show All'
        # Verifies if panel has all search fields and only those declared
        # on the query page
        @search_fields.values.each(&:check_layout)
        expect(page.all(search_fields_xpath).count).to eq @search_fields.length
      end

      def clear_fields
        make_action 'Clear Fields'
      end

      def search
        make_action 'Search'
        TS.wait_table_loading
      end

      def show_all
        make_action 'Show All'
        TS.wait_table_loading
      end

      def content_xpath
        "#{header_xpath}/following-sibling::div[1]"
      end

      protected

      def header_xpath
        "//b[contains(text(), 'Search Fields')]/ancestor::div[1]"
      end

      private

      def search_fields_xpath
        "#{content_xpath}//div[@class='search-fields']/div"
      end

      # Just an alias for query definition, should not be used in other way.
      def field(label,type,*tags)
        @auto_search = false if tags.include? :required
        @search_fields[label] = Components::TS::SearchField.new self, label, type, *tags
        define_singleton_method TS.to_sym(label), lambda { @search_fields[label].input }
        define_singleton_method TS.to_sym("#{label}_operator"), lambda { @search_fields[label].operator }
      end

    end
  end
end
