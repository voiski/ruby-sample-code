module Components
  module TS
    module ConfirmAlert
      def self.accept
        click! accept_button_xpath
      end

      def self.accept_present?
        button_present? accept_button_xpath
      end

      def self.cancel
        click! cancel_button_xpath
      end

      def self.cancel_present?
        button_present? cancel_button_xpath
      end

      def self.dismiss
        click! dismiss_button_xpath
      end

      def self.dismiss_present?
        button_present? dismiss_button_xpath
      end

      def self.present?
        using_wait_time 5 do
          page.has_xpath? base_alert_path
        end
      end

      def self.text
        Components::PlainText.new("#{base_alert_path}/table/tbody/tr[2]/td[2]").get_text
      end

      private

      def self.click!(xpath)
        find(xpath).click
      end

      def self.button_present?(xpath)
        using_wait_time 1 do
          page.has_xpath? xpath
        end
      end

      def self.accept_button_xpath
        "#{base_alert_path}//span[@class='clsokcancel' and text()='Ok']"
      end

      def self.cancel_button_xpath
        "#{base_alert_path}//span[@class='clsokcancel' and text()='Cancel']"
      end

      def self.dismiss_button_xpath
        "#{base_alert_path}//img[@id='_closeMsg' or @id='closeImg']"
      end

      def self.base_alert_path
        "//div[@id='msgDiv']"
      end
    end
  end
end

