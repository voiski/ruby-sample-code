module Components
  module TS
    module SavedSearch

      def delete_saved_search(saved_search_name)
        self.saved_searches.click
        using_wait_time 0 do
          page.find("//a[contains(text(),'#{saved_search_name}')]/preceding::input[@type='checkbox'][1]").click
          page.find("//div[@class='clsdeletesearch' and contains(text(),'Delete')]").click
        end
        Components::TS.wait_loading
        Components::Alert.dismiss if Components::Alert.present?
      end

      def get_saved_search(saved_search_name)
        self.advanced_search.expand!
        self.saved_searches.click
        saved_search = nil
        begin
          using_wait_time 0 do
            saved_search = page.find("//a[@class='clsTextLabelSavedSrch' and text()='#{saved_search_name}']")
            page.find("//img[@id='closeImg']").click
          end
        rescue Capybara::ElementNotFound
          saved_search = nil
        end
        saved_search
      end

      def save_saved_search(saved_search_name)
        self.advanced_search.save_search!
        using_wait_time 0 do
          page.find_by_id('_SRCH_TEXT').set saved_search_name if saved_search_name
          page.find("//div[@class='clsokcancel' and contains(text(),'Save')]").click
        end
        Components::TS.wait_loading Components::Alert.dismiss if Components::Alert.present?
      end

      def select_saved_search(saved_search_name)
        self.saved_searches.click
        using_wait_time 0 do
          page.find("//a[@class='clsTextLabelSavedSrch' and text()='#{saved_search_name}']").click
        end
        Components::TS.wait_loading
        Components::Alert.dismiss if Components::Alert.present?
      end

    end
  end
end
