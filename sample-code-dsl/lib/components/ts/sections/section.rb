module Components
  module TS
    # Normal section with fields, to use table see TableSection
    class Section
      include RSpec::Matchers
      include Components::TS::PageDesign::EmbeddedRolesDsl
      attr_reader :name, :fields, :parent
      def initialize(parent, name, &block)
        @parent = parent
        @name = name
        @fields = {}
        instance_eval(&block) if block
      end

      def name=(name)
        old_method_name = TS.to_sym @name
        parent.send :define_singleton_method, TS.to_sym(name), lambda{ self.send(old_method_name) }
        @name=name
      end

      def check_layout
        TS.log self, "Checking #{@name} section"
        expect(page).to have_xpath header_xpath
        expect(page).to have_xpath content_xpath
        total_of_fields = 0
        @fields.values.each do |field|
          total_of_fields += if field.is_above? || field.is_no_label?
                               1
                             else
                               2
                             end
          field.check_layout
        end
        expect(page.all(fields_xpath).count).to eq total_of_fields if fields_xpath
      end

      def content_xpath
        "#{header_xpath}/ancestor::table[2]"
      end

      protected

      def header_xpath
        "#{@parent.content_xpath}/descendant::table[@class='clsoverviewsectionheader' or @class='clsContentSection']/descendant::b[normalize-space(text()) = '#{@name}']"
      end

      def fields_xpath
        "#{content_xpath}/tbody/tr[3]/td/table/tbody/tr/td"
      end

      private

      # Just an alias for query definition, should not be used in other way.
      def field(label,*tags)
        label_key = if @fields[label]
                      position = TS.select_all(@fields,label).count + 1
                      tags << {position: position}
                      "#{label} #{position}"
                    else
                      TS.parse_label_to_key label
                    end
        @fields[label_key] = Components::TS::EmbeddedField.new(self, label, *tags)
        define_singleton_method TS.to_sym(label_key), lambda { @fields[label_key].input }
      end
    end
  end
end
