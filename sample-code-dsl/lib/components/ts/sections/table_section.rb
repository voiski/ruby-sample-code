module Components
  module TS
    class TableSection < Section
      include Components::TS::EmbeddedTable

      def check_layout
        super
        @table.check_layout if @table
      end

      protected

      # Skipped
      def fields_xpath; nil; end

    end
  end
end
