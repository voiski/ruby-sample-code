module Components
  module TS
    # Section to handle TS custom section panels. See Components::TS::PageDesign.
    # Ex:
    # panel_section = Components::TS::PanelSection.new 'Section Name' do
    #                   table_title 'Table Title!'
    #
    #                   has_checkbox
    #                   column 'Label 1', :plain_text, :freezed
    #                   # plain_text is the default type
    #                   column 'Label 2', :freezed
    #                   column 'Label 3', :select
    #                 end
    #
    # expect(panel_section.table.column_titles).to eq ['Label 1','Label 2','Label 3']
    #
    # expect(panel_section.label_2  ).to eq panel_section.table.column 'Label 2'
    # expect(panel_section.checkbox ).to eq panel_section.table.column ''
    #
    # panel_section.post!
    # panel_section.post.click  #the same of post!
    # panel_section.select_all!
    # panel_section.select!('Some CC')
    # panel_section.select_component('Some CC').click #the same of select!
    class PanelSection < Section
      include Components::TS::EmbeddedTable

      def initialize(parent, name, enabled_roles, &block)
        super(parent, name, &block)
        @enabled_roles = enabled_roles
      end

      def post!
        post.click
      end

      def post
        @post ||= Components::Link.new "#{content_xpath}//label[@title='Post']", :visible
      end

      def select_all!(check=true)
        select!('Select All', check)
      end

      def select!(label, check=true)
        if check
          select_component(label).check
        else
          select_component(label).uncheck
        end
      end

      def select_component(label)
        Components::Checkbox.new input_xpath(label)
      end

      def collapsed?
        not page.find(content_xpath).visible?
      end

      def check_layout
        super
        post.should_exist! if @enabled_roles[:post].include? PageModule::Page.current_role.to_sym
        select_component('Select All').should_exist!
        expect(page).to have_xpath "#{content_xpath}//table[@class='clssubsectionheader']//b[normalize-space()='#{@table_title}']"
        table.check_layout
      end

      def content_xpath
        "#{header_xpath}/ancestor::div[@class='clscustomsection']"
      end

      protected

      # Skipped
      def fields_xpath; nil; end

      private

      def table_title(table_title)
        @table_title = table_title
      end

      def input_xpath(label)
        "#{content_xpath}//td[label[text()='#{label}']]/input"
      end
    end
  end
end
