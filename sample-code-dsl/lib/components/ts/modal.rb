module Components
  module TS
    # Modal class to map the special modals like the ones that we have at the Sell Channels
    # It maps the links bellow:
    # fill_down       to fill down the fields
    # fill_up         to fill up the fields
    # fill_selected   to fill the fields form the selected rows
    # save            to save the changes
    # cancel          to cancel the cahnges
    # previous_item   to go to the previous items
    # go              to go to the bom # at the go_to field
    # go_to(value)    to go to the bom # from the given value
    # next_item       to go to the next items
    class Modal
      include Components::TS::EmbeddedTable
      include RSpec::Matchers
      attr_reader :label

      def initialize(label, &block)
        @label = label
        @buttons = []
        ['Fill Up','Fill Down', 'Fill Selected'].each{|b| button b, :label}
        ['< Previous Item', 'Save', 'Go', 'Next Item >'].each{|b| button b, :button}
        ['Cancel'].each{|b| button b, :link}
        instance_eval(&block) if block
      end

      def check_layout
        TS.log self, "Checking #{@label} modal"
        using_wait_time 0 do
          expect(page).to have_xpath(header_xpath)
          expect(page).to have_xpath(content_xpath)
          @table.check_layout if @table
          @buttons.each{|button| check_action button[:label], button[:type] }
          go_to_input.should_exist!
        end
      end

      def content_xpath
        "//div[@class='ui-dialog-content ui-widget-content']"
      end

      def go_to_input
        @go_to_input ||= Components::TextField.new "#{content_xpath}//input[@id='gototextbox']"
      end

      def go_to(value)
        go_to_input.value = value
        go!
      end

      private

      def make_action(text,type=:label)
        using_wait_time 5 do
          button = page.find button_xpath(text,type)
          button.click
        end
        TS.wait_loading
      end

      def check_action(text,type=:label)
        expect(page).to have_xpath button_xpath(text,type)
      end

      def button_xpath(text,type)
        case type
        when :label
          "#{content_xpath}//label[@class='clsTextLabelLink' and text()='#{text}']"
        when :disabled
          "#{content_xpath}//label[text()='#{text}']"
        when :button
          "#{content_xpath}//input[@type='button' and @value='#{text}']"
        else
          "#{content_xpath}//a[text()='#{text}']"
        end
      end

      def header_xpath
        "#{content_xpath}//div[@class='clsclrentertitle']"
      end

      # Just a method for query definition, should not be used in other way.
      def button(label, type=:label)
        @buttons << {label: label,type: type}
        define_singleton_method "#{TS.to_sym(label)}!" do
          make_action label, type
          TS.wait_table_loading
        end
      end

    end
  end
end
