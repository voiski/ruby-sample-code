module Components
  module TS
    # Class for calendar fields, should have the link to the calendar component
    class CalendarField < TextFieldWithAction

      protected

      def action_xpath
        "#{xpath}/../a/img"
      end

    end
  end
end
