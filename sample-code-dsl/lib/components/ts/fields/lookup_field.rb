module Components
  module TS
    # Class for lookup fields
    class LookupField < TextFieldWithAction

      def value=(value)
        trigger_blur_event
        get_element.set ''
        if (!value.to_s.empty?)
          trigger_click_event
          trigger_focus_event
          get_element.send_keys value
          trigger_keydown_event
        end
        trigger_blur_event
      end

      protected

      def action_xpath
        "#{xpath}/../*[self::img[@class='clsTextLabelLink'] or self::a[contains(@class,'val-icon')]]"
      end

    end
  end
end