module Components
  module TS
    # Generic Class for text fields with action
    class TextFieldWithAction < TextField

      def click
        action.click
      end

      def should_exist!
        super
        action.should_exist! unless @args.include? :readonly
      end

      def action
        @action ||= Components::Link.new action_xpath
      end

      protected

      def action_xpath
        raise 'Not implemented!'
      end
    end
  end
end
