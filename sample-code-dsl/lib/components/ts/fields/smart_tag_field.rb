module Components
  module TS
    # Class for smart tag fields
    class SmartTagField < TextFieldWithAction

      protected

      def action_xpath
        "#{xpath}/../img[@src='images/tip.gif']"
      end

    end
  end
end
