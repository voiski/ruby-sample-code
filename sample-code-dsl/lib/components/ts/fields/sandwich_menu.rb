module Components
  module TS
    # Class for sandwich menu, should have the link to the calendar component
    class SandwichMenu < Link

      def should_exist!
        super
        expect(page).to have_xpath "#{xpath}/../img[@src='images/m.gif']"
      end

    end
  end
end
