module Components
  module TS
    # Component to handle the search field of a query page. See Components::TS::SearchPanel.
    # Ex:
    # search_field.input.value='Filter value'
    # expect(search_field.input.value).to eq 'Filter value'
    # search_field.operator.value='Starts with'
    class SearchField
      attr_reader :parent

      def initialize(parent, label_text, *tags)
        @parent = parent
        @embedded_field = EmbeddedField.new self, label_text, :search, *tags
      end

      def label_text
        @embedded_field.label_text
      end

      def label
        @embedded_field.label
      end

      def input
        @embedded_field.input
      end

      def operator
        @operator ||= Components::Select.new "#{content_xpath}//select[1]"
      end

      def parent_panel=(panel)
        @parent_panel=panel
        @embedded_field.parent_panel=panel
      end

      def check_layout
        @embedded_field.check_layout
        operator.should_exist!
      end

      def content_xpath
        "#{@parent.content_xpath}//div[span[normalize-space(text())='#{label_text}']]"
      end
    end
  end
end
