module Components
  module TS
    # Embedded Field with a label and an input. Can be used in the search field
    # or in the sections. See more at Components::TS::SearchField or
    # Components::TS::Section.
    class EmbeddedField
      include Components::TS::Versions
      include RSpec::Matchers
      attr_reader :label_text, :parent

      def initialize(parent, label_text, *tags)
        @parent = parent
        @label_text = if label_text == TS::EMPTY_LABEL || label_text == ''
                        :empty_label
                      else
                        label_text
                      end
        @tags = tags
        @tags << :no_label if label_text == :no_label
      end

      def label
        @label ||= PlainText.new header_xpath
      end

      def input(input_type=nil)
        if input_type
          Browser.get_component content_xpath, 'html', input_type, *@tags
        else
          @input ||= Browser.get_component content_xpath, 'html', type, *@tags
        end
      end

      def check_layout
        TS.log self, "Checking #{@label_text} - #{type} field"
        label.should_exist! unless @tags.include? :no_label
        input.should_exist!
        if type == :select
          valid_options = @tags.find{|tag| tag.is_a?(Hash) && tag[:options]}
          expect(input.options).to eq valid_options[:options] if valid_options
        end
        expect("readonly #{input.get_element['readonly']}").to eq "readonly true" if @tags.include? :readonly
      end

      def is_above?
        @tags.include? :above
      end

      def is_no_label?
        @tags.include? :no_label
      end

      private

      def content_xpath
        @content_xpath ||= if is_no_label?
                             "#{@parent.content_xpath}//%{html}[1]"
                           elsif is_above?
                             "#{header_xpath.gsub(/%/,'%%')}/../%{html}[1]"
                           else
                             klass_xpaths :content_xpath, parent: @parent.content_xpath,
                                                          header_xpath: header_xpath.gsub(/%/,'%%'),
                                                          html: '%{html}'
                           end
      end

      def header_xpath
        if @label_text == :empty_label
          template = :empty_header_xpath
        else
          template = :header_xpath
        end

        @header_xpath ||= begin
                            position = @tags.find{|tag| tag.is_a?(Hash) && tag[:position]}
                            position ||= {position: 1}
                            klass_xpaths template, parent: @parent.content_xpath,
                                                        label: TS.parse_label_to_xpath(@label_text),
                                                        position: position[:position]
                          end
      end

      def type
        @type ||= (@tags & Components::Browser::COMPONENTS.keys).first
      end
    end
  end
end
