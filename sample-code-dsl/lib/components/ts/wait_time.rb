module Components
  module WaitTime

    DEFAULT_WAITING_TIME = 3

    def wait_until(options = {}, &block)
      block_timeout = options[:timeout] || DEFAULT_WAITING_TIME
      using_wait_time 1 do
        start_time = Time.now
        while true
          condition = yield
          if condition and (not time_over?(start_time, block_timeout))
            return condition
          elsif time_over?(start_time, block_timeout)
            raise "Could not match condition after #{block_timeout} seconds"
          end
        end
      end
    end

    private

    def time_over?(start_time, default_waiting_time)
      Time.now - start_time > default_waiting_time
    end
  end
end