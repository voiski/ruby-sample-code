module Components
  module TS
    # Page definition class to query pages.
    # Ex:
    # class PageExample
    #   include Components::TS::PageDesign
    #
    #   .....
    #
    # end
    #
    # The page object that implements this base class when instantiated will
    # have helper methods to access the composition objects.
    module PageDesign
      include Components::TS::PageDesign::EmbeddedRolesDsl

      def self.included(base)
        base.extend(ClassMethods)
      end

      def nav_menu_links
        @nav_menu_links ||= {}
      end

      def nav_menu
        @nav_menu ||= {}
      end

      def panels
        @panels ||= {}
      end

      def buttons
        @buttons ||= {}
      end

      def check_layout
        validate_role if self.class.method_defined? :validate_role
        if self.class.method_defined? :get_query_title
          TS.log Components::TS::PageDesign::TitleDsl, "Checking #{title} - page title"
          expect(page).to have_xpath "//label[(@class='clstitleText' or @class='clsTextLabelNormal') and contains(text(),\"#{title}\")]"
        end

        if self.class.method_defined? :subtitle
          TS.log Components::TS::PageDesign::TitleDsl, "Checking #{subtitle} - page subtitle"
          expect(page).to have_xpath "//label[@class='clsTextLabelNormal' and contains(text(),\"#{subtitle}\")]"
        end

        klass_components.each do |_type, content|
          content.each do |role,components|
            if [:all, PageModule::Page.current_role.to_sym].include? role
              components.each{|component| send(component).check_layout }
            end
          end
        end
      end

      def load!
        unless @loaded
          klass_components.each do |_type, content|
            content.each do |role,components|
              if [:all, PageModule::Page.current_role.to_sym].include? role
                components.each{|component| send(component) }
              end
            end
          end

          @loaded=:done
        end
      end

      protected

      def klass_components
        @components ||= if self.class.superclass.method_defined? :klass_components
                          superklass_components = self.class.superclass.new.klass_components
                          PageModule::Page.last_page = self
                          merge_components superklass_components, self.class.components
                        else
                          self.class.components
                        end
      end

      def deprecated(deprecated_method, method_to_use, *args)
        TS.warn self, "DEPRECATED! #{deprecated_method} is out of use, prefer #{method_to_use}!"
        send(method_to_use, *args)
      end

      private

      def merge_components(parent_components, current_components)
        parent_components.each do |type, content|
          if current_components[type]
            current_content = current_components[type]
            content.each do |role,components|
              current_content[role] ||= []
              # parent come first
              current_content[role] = (components + current_content[role]).uniq
            end
          else
            current_components[type] = content
          end
        end
        current_components
      end

      # Class methods that will be extended by the previous module
      module ClassMethods

        def components
          @components ||= {tabs:{},buttons:{},panels:{}, nav_menu:{}}
        end

        protected

        def add_component(type,roles,method_name)
          handle_given_roles(roles).each do |role|
            components[type][role] ||= []
            components[type][role] << method_name
          end
        end

        include PageDesign::TitleDsl
        include PageDesign::RolesDsl
        include PageDesign::TabDsl
        include PageDesign::ButtonDsl
        include PageDesign::AdvancedSearchPanelDsl
        include PageDesign::SearchPanelDsl
        include PageDesign::SectionsPanelDsl
        include PageDesign::SimplePanelDsl
        include PageDesign::TablePanelDsl
        include PageDesign::ResultPanelDsl
        include PageDesign::ComposeDsl
        include PageDesign::NavigationMenuDsl
        include PageDesign::ModalDsl

        private

        def handle_given_roles(roles)
          if roles.first.is_a? Hash
            roles = roles.first
            roles = if roles[:except]
                      [:admin,:gp,:tp,:ct,:vendor] - [roles[:except]].flatten
                    else
                      [roles[:only]]
                    end
          end

          roles << :all if roles.empty?
          roles
        end

      end
    end
  end
end
