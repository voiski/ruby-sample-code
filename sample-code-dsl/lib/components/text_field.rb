module Components
  class TextField < GenericComponent

    def set_text(value)
      # element = get_element
      # element.native.send_keys(value)
      # element.click
      # element.set(' ')
      # element.set(value)
      trigger_change_event value
      get_element.send_keys :tab
    end
    alias :value= :set_text

    def get_text
      trigger_blur_event
      get_element.value
    end
    alias :value :get_text

    def focus
      get_element.set('')
    end

  private

    def trigger_change_event(val)
      # clicking on the parent element to fix issue on setting values on the queries
      parent_xpath = xpath + "/.."
      if page.has_xpath? parent_xpath
        parent_element = page.find(xpath + "/..")
        parent_element.click if parent_element.visible?
      end
      page.execute_script <<-JS
        field = document.evaluate("#{xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        $(field).trigger('focus')
        if (!$(field).attr('readonly') && !$(field).attr('disabled')) {
          $(field).val("#{val}")
          $(field).trigger('change')
          $(field).trigger('blur')
        }
      JS
      hidden_field_xpath = xpath + "/following-sibling::input[@type='hidden']"
      page.execute_script <<-JS
        field = document.evaluate("#{hidden_field_xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        $(field).trigger('focus')
        if (!$(field).attr('readonly') && !$(field).attr('disabled')) {
          $(field).val("#{val}")
          $(field).trigger('change')
          $(field).trigger('blur')
        }
      JS
    end

    def trigger_blur_event
      page.execute_script <<-JS
        field = document.evaluate("#{xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        $(field).trigger('focus')
        if (!$(field).attr('readonly') && !$(field).attr('disabled')) {
          $(field).trigger('change')
          $(field).trigger('blur')
        }
      JS
    end

    def trigger_focus_event
      page.execute_script <<-JS
        field = document.evaluate("#{xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        $(field).trigger('focus')
        if (!$(field).attr('readonly') && !$(field).attr('disabled')) {
          $(field).trigger('focus')
        }
      JS
    end

    def trigger_keydown_event
      page.execute_script <<-JS
        field = document.evaluate("#{xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        $(field).trigger('focus')
        if (!$(field).attr('readonly') && !$(field).attr('disabled')) {
          $(field).trigger('keydown')
        }
      JS
    end

    def trigger_click_event
      page.execute_script <<-JS
        field = document.evaluate("#{xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        $(field).trigger('focus')
        if (!$(field).attr('readonly') && !$(field).attr('disabled')) {
          $(field).trigger('click')
        }
      JS
    end
  end
end
