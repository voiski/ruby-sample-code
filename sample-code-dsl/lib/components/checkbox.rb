module Components
  class Checkbox < GenericComponent
    def check
      click_on_field unless checked?
    end

    def checked?
      get_element['checked'] == 'true'
    end
    alias :value :checked?

    def uncheck
      click_on_field if checked?
    end

    def value=(value)
      if value == 'true'
        check
      else
        uncheck
      end
    end

    # Returns a hash containing the type and the values from this element.
    # Ex:
    #   component = Components::Checkbox.new(xpath_to_field).data
    #   component[:type] === :checkbox
    #   component[:options].each.with_index {|t,i| puts "At #{i+1} is {t}" }
    #   puts 'ok' if component[:visible]
    def data
      {
        type: :checkbox,
        options: [get_element.find('..').text],
        visible: get_element.visible?
      }
    end

    private

    def click_on_field
      Components::TS.wait_table_loading
      page.execute_script  <<-JS
         field = document.evaluate("#{xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
         $(field).click()
      JS
    end

  end
end
