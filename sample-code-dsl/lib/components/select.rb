module Components
  class Select < GenericComponent

    # Returns a hash containing the type and the values from this element.
    # Ex:
    #   component = Components::Select.new(xpath_to_field).data
    #   component[:type] === :select
    #   component[:options].each.with_index {|t,i| puts "At #{i+1} is {t}" }
    #   puts 'ok' if component[:visible]
    def data
      {
        type: (get_element['multiple']? :multiple_select : :select),
        options: options,
        visible: get_element.visible?
      }
    end

    def select(option)
      if get_element.visible?
          trigger_click_event
          get_element.select(option)
      else
        trigger_change_event option
      end
    end
    alias :value= :select

    def select_by_index(index)
      get_element.find("option[#{index}]").click
    end

    def option_value
      get_element.value
    end

    def value
      get_element.all("option[@value='#{option_value}']").each do |o|
        return o.text
      end
    end

    def get_text
      get_element.native.text
    end

    alias :text :get_text

    # Returns true if the option exist
    def has_option?(option)
      options.include? option
    end

    # Returns texts from the options.
    def options
      values = []
      # the value pattern check will avoid the default options that means no
      # selection option like '--Select--'
      get_element.all('option', text: /.+/).each do |o|
        values << o.text unless o.text === "--Select--"
      end
      values
    end

    def options_values_hash
      values = {}
      # the value pattern check will avoid the default options that means no
      # selection option like '--Select--'
      get_element.all('option', text: /.+/).each do |o|
         values[o.value] = o.text unless o.text === "--Select--"
      end
      values
    end

    # Returns the number of options.
    def options_count
      options.count
    end

    private

    def trigger_change_event(val)
      value = options_values_hash.key(val)
      page.execute_script <<-JS
        field = document.evaluate("#{xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        $(field).trigger('focus')
        if (!$(field).attr('readonly') && !$(field).attr('disabled')) {
          $(field).val("#{value}")
          $(field).trigger('change')
          $(field).trigger('blur')
        }
      JS
    end

    def trigger_click_event
      page.execute_script <<-JS
        field = document.evaluate("#{xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        $(field).trigger('focus')
        if (!$(field).attr('readonly') && !$(field).attr('disabled')) {
          $(field).trigger('change')
          $(field).trigger('click')
        }
      JS
    end

  end
end
