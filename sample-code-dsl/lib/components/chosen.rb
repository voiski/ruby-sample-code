module Components
  # Component class to help in manipulation of fields using the jquery plugin
  # named Chosen. The plugin rewrite the html code hiding the original select
  # field and creating a struct of divs and lists to interact with the user.
  #
  # Ex:
  #   component = Components::Chosen.new xpath_to_field
  #   component
  #     .clear # to clear
  #     .select(text_of_one_item) # select one
  #     .value # return the value of native select html component
  #   ....
  class Chosen < Select

    # Clear any selected options, even if it is a multi-select type. If none is
    # selected, will skip the action. The return will be the own object where
    # you can chains another action.
    #
    # Ex:
    #   component = Components::Chosen.new(xpath_to_field).clear
    def clear
      unless value.empty?||value==='?'
        close_buttons = get_element[:chosen].all(:css, '.search-choice-close').count
        if close_buttons>0
          for i in 1..close_buttons
            # Need to find again because the tree change after each click
            get_element[:chosen].first(:css, '.search-choice-close').click
          end
        # skip if dont exist values selected
        elsif get_element[:chosen][:class].include? 'chosen-container-single'
          get_element[:chosen].click
          get_element[:chosen].first(:css,'li').click if get_element[:chosen].first(:css,'li')
        end
      end
      self
    end

    # Returns a hash containing the type and the values from this element.
    # Ex:
    #   component = Components::Chosen.new(xpath_to_field).data
    #   component[:type] === :multiple_select
    #   component[:options].each.with_index {|t,i| puts "At #{i+1} is {t}" }
    #   puts 'ok' if component[:visible]
    def data
      {
        type: (get_element[:select]['multiple']? :multiple_select : :select),
        options: options,
        visible: get_element[:chosen].visible?
      }
    end

    # Returns a hash with the native select html component and the native div
    # that is the chosen element.
    #
    # Ex:
    #   Components::Chosen.new(xpath_to_field)
    #     .get_element.each{|k,e| puts "The element #{k} has the id #{e['id']}"}
    def get_element
      select_element = super
      {
        chosen: select_element.first('following-sibling::div'),
        select: select_element
      }
    end

    # Returns the text of the native select element.
    def get_text
      get_element[:select].native.text
    end
    alias :text :get_text

    # Returns the value of the native select element.
    def get_value
      get_element[:select].value
    end
    alias :value :get_value

    # Returns true if the option exist
    def has_option?(option)
      options.include? option
    end

    # Returns texts from the options in the native select element.
    def options
      values = []
      # the value pattern check will avoid the default options that means no
      # selection option like '--'
      get_element[:select].all('option', text: /.+/).each do |o|
        values << o.text
      end
      values
    end

    # Returns the number of options in the native select element.
    def options_count
      options.count
    end

    # Select the option informed in the chosen element, first put the text in
    # the search input and then select the given option filtered by the chosen.
    # The return will be the own object where you can chains another action.
    #
    # Ex:
    #   Components::Chosen.new(xpath_to_field)
    #     .select('Item 1')
    #     .select('Item 2')
    def select(option)
      if get_element[:chosen].all(:css, '.chosen-choices span', text: option).empty?
        get_element[:chosen].click while get_element[:chosen].all('.//input', visible: true).empty?
        get_element[:chosen].find('.//input').click
        get_element[:chosen].find('.//input').set(option)
        get_element[:chosen].first(:css, '.chosen-results em', text: option).click
      end
      self
    end

    # Select the index informed in the chosen element. The return will be the
    # own object where you can chains another action.
    #
    # Ex:
    #   Components::Chosen.new(xpath_to_field)
    #     .select_by_index(0)
    #     .select_by_index(1)
    def select_by_index(index)
      unless get_element[:select].all('option')[index].selected?
        get_element[:chosen].click while get_element[:chosen].all('.//input', visible: true).empty?
        get_element[:chosen].all(:css, '.chosen-results li')[index].click
      end
      self
    end

  end
end
