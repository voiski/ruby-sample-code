module Components
  class PlainText < GenericComponent
    def get_text
      get_element.text
    end
    alias :value :get_text

    def should_exist!
      expect(page).to have_xpath xpath

      using_wait_time 0 do
        expect(page).to_not have_xpath "#{xpath}//select", visible: true
        expect(page).to_not have_xpath "#{xpath}//input", visible: true
      end
    end

  end
end
