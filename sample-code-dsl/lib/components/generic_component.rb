module Components
  class GenericComponent
    include RSpec::Matchers

    def initialize(*args)
      @args = args
    end

    def get_element
      element = page.send search_args[:method]||:all, *search_args[:args]
      search_args[:method]? element : element.first
    end

    def get_elements
      page.all *search_args(false)[:args]
    end

    def [](property)
      get_element[property]
    end

    def []=(property,value)
      get_element[property]

      page.execute_script <<-JS
        field = document.evaluate("#{xpath.tr(?", ?')}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        $(field).attr('#{property}','#{value}')
      JS
    end

    def exist?
      page.has_selector? xpath
    end

    def readonly?
      self.get_element['readonly'] == 'true'
    end

    def should_exist!
      expect(page).to have_xpath xpath
    end

    def should_not_exist!
      expect(page).to_not have_xpath xpath
    end

    def xpath
      @xpath ||= @args.select{|a| !a.is_a? Symbol }.first
    end

    private

    def search_args(handle_path=true)
      search_definition = {args: []}
      # kind
      search_definition[:args] << if @args.include?(:css)
                                    :css
                                  else
                                    :xpath
                                  end
      # locator
      search_definition[:args] << if handle_path && @args.include?(:last)
                                    "(#{xpath})[last()]"
                                  elsif handle_path && @args.include?(:first)
                                    "(#{xpath})[1]"
                                  else
                                    search_definition[:method] = :find
                                    xpath
                                  end
      # options
      search_definition[:args] << {visible: true} if @args.include? :visible

      search_definition
    end

  end
end
