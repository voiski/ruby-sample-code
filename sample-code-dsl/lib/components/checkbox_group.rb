module Components
  # Component class to help in manipulation of a group of checkbox fields. It
  # will handle a check by the given text, and will uncheck all options if
  # needed.
  #
  # Ex:
  #   component = Components::CheckboxGroup.new xpath_to_field
  #   component
  #     .clear # to clear
  #     .check # to check all
  #     .uncheck # to uncheck all
  #     .check_only([{text:'Item 1'}]) # to select the given option
  #     .get_element # to return an array of checkbox input elements
  #   ....
  class CheckboxGroup < Checkbox

    # Check all checkboxes. The return will be the own object where you can
    # chains another action.
    def check
      get_element.each{ |f| f.set true }
      self
    end

    # Check the given options. To select them inform a array of hashes with one
    # of the two options, id or text. The return will be the own object where
    # you can chains another action.
    #
    # Ex:
    #  options = []
    #  options << {id: 'id_item_1'}
    #  options << {text: 'Last item text'}
    #  Components::CheckboxGroup.new(xpath_to_field).check_only(options)
    def check_only(options)
      get_element.each do |f|
        f.set(
          options.include?({id: f['id']}) ||
          options.include?({text: f.find('..').text})
        )
      end
      self
    end

    # The same of uncheck, uncheck all checkboxes. The return will be the own
    # object where you can chains another action.
    def clear
      uncheck
    end

    # Returns a hash containing the type and the values from this element.
    # Ex:
    #   component = Components::CheckboxGroup.new(xpath_to_field).data
    #   component[:type] === :checkbox
    #   component[:options].each.with_index {|t,i| puts "At #{i+1} is {t}" }
    #   puts 'ok' if component[:visible]
    def data
      {
        type: :checkbox,
        options: options,
        visible: get_element[0].visible?
      }
    end

    # Returns an array of checkbox input elements
    def get_element
      super.find('../..').all('.//input')
    end

    # Returns texts from the options in the native select element.
    def options
      values = []
      get_element.each do |o|
        values << o.find('..').text
      end
      values
    end

    # Uncheck all checkboxes. The return will be the own object where you can
    # chains another action.
    def uncheck
      get_element.each{ |f| f.set false }
      self
    end

    # Returns the value of the elements.
    def value
      get_element.select{|f| f.selected?}.map{|f| f.value}
    end

  end
end