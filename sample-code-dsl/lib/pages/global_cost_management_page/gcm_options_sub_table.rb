module PageModule
  # Embedded Options table for a rfq item at the GCM screen
  module GCMOptionsSubTable

    def self.compose
      lambda do |klass|
        klass.sub_table_panel 'Options' do
          has_checkbox
          column :expand, :expand, append: PageModule::GCMOptionMarketChannelsSubTable
          only_role :admin, :tp, :vendor do
            sandwich_menu
          end
          column 'Option No', :link
          if role? :vendor
            column 'Option Status', :text, :readonly
            column 'Option Description', :text, :readonly
            column 'Country of Origin', :text, :lookup
            column 'Freight Paid By', :select
            column 'CT', :text, :readonly
            column 'First Cost', :smart_tag, :readonly
          else
            ct_fields = :readonly unless role? :ct

            column 'Option Status', :text, :readonly
            column 'Rollup', :text, :readonly
            only_role :admin do
              column 'Liked Option?', :text, :readonly
            end
            column 'Like Option Date', :text, :readonly
            if role? :admin
              column 'Auto Re-Like?', :select, :readonly
            else
              column 'Auto Re-Like?', :text, :readonly
            end
            column 'Option Description', :text, ct_fields
            column 'Vendor Name', :lookup, ct_fields
            column 'Country of Origin', :lookup, readonly?{role? :gp}
            if role? :gp
              column 'Freight Paid By', :text, :readonly
            else
              column 'Freight Paid By', :select, options: ['--Select--', 'PURCHASER','PURCHASER/VENDOR','VENDOR','VENDOR PAY 50%']
            end
            column 'Agent Name', :text, ct_fields
            column 'CT', :text, :readonly
            column 'First Cost', :smart_tag, :readonly
            column 'ELC ', :smart_tag, :readonly
            only_role :ct do
              column 'CT Comments', :text
            end
          end

          if role? :admin,:ct,:tp,:vendor
            column 'Duty Class. Code', :lookup
            column 'Import Category', :text, :readonly
            column 'Product Type', :text, :readonly
            column 'Chief Weight Fiber', :text, :readonly
          end
        end
      end # end lambda
    end

  end
end
