module PageModule
  # Embedded Option Market/Channels table for the Option table at the GCM screen
  module GCMOptionMarketChannelsSubTable

    def self.compose
      lambda do |klass|
        klass.sub_table_panel 'Option Market/Channels' do
          has_checkbox
          column :expand, :expand, append: PageModule::GCMCostExceptionSubTable
          column 'Market/Channel', :lookup, readonly?{ not_role? :admin }
          column 'First Cost', :text, :readonly
          except_role :vendor do
            column 'ELC', :smart_tag, readonly?{ role? :gp, :tp }
          end
          column 'Destination Country', :lookup, readonly?{ not_role? :admin }
          column 'Duty %', :text, :readonly
          except_role :gp do
            column 'Manual Duty', :text
          end
          column 'Transfer Point', :lookup, readonly?{ role? :gp }
          if role? :gp
            column 'Ship Mode', :text, :readonly
            column 'Terms of Sale', :text, :readonly
            column 'Ctn Type', :text, :readonly
          else
            column 'Ship Mode', :select, options: ['--Select--', 'Air', 'Ocean', 'Truck']
            column 'Terms of Sale', :lookup
            column 'Ctn Type', :select
          end
          column 'Ctn Height', :text, :readonly
          column 'Height UM', :text, :readonly
          column 'Ctn Width', :text, :readonly
          column 'Width UM', :text, :readonly
          column 'Ctn Length', :text, :readonly
          column 'Length UM', :text, :readonly
          column 'Pack Factor', :text, readonly?{ role? :gp }
          only_role :admin do
            column 'Pack Meas', :text, :readonly
            column 'UM Pack Meas', :text, :readonly
            column 'Case Pack Wgt', :text, :readonly
            column 'UM Case Pack Wgt', :text, :readonly
          end
        end
      end # end lambda
    end

  end
end
