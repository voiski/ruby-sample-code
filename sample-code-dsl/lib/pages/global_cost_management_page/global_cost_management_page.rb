module PageModule
  class GlobalCostManagementPage < Page
    include Components::TS::PageDesign
    include Components::TS::SavedSearch

    title 'Global Cost Management'

    button 'More Actions...' do
      role :admin, :gp do
        button 'Ready for CT'
        button 'Like Option'
        button 'Re-request to CT'
        button 'Confirm Option'
        button 'Non-Confirm Option'
        button 'Mark Primary'
        button 'RFQ Complete'
        button 'RFQ Non-Complete'
      end

      role :admin, :ct, :tp do
        button 'Submit (Summary)'
        button 'Submit (Detailed)'
        button 'Rebid (Summary)'
        button 'Rebid (Detailed)'
        button 'Client Reject'
      end

      role :tp do
        button 'Offer Option'
        button 'Like Option'
        button 'Confirm Option'
      end

      role :admin, :ct do
        button 'Promote to GP'
      end

      role :admin, :vendor do
        button 'Offer Option'
        button 'Reject Option by Vendor'
      end

      button 'Compare BOM'
      button 'Compare BOM PDF'
    end
    button 'Saved Searches'
    button 'Save'

    advanced_search_panel 'Global Cost Management' do
      section 'Option Search' do
        field 'Season', :select, :above,  options: ['FALL','HOLIDAY','SPRING','SUMMER']
        field 'Year', :text, :above
        field 'BOM #', :text, :above
        field 'Style Description', :text, :above
        field 'Master Style #', :text, :above
        field 'Master Style Description', :text, :above
        field 'Option Description', :text, :above
        field 'CT', :select, :above

        role :admin do
          field 'Market/Channel', :select, :above, options: ['CANADA ONLINE', 'CANADA RETAIL', 'CHINA RETAIL (CHINA MAINLAND)', 'CHINA RETAIL (HONG KONG)', 'EU FRANCHISE', 'EU ONLINE', 'EU RETAIL', 'HONG KONG FRANCHISE', 'JAPAN RETAIL', 'US ONLINE', 'US RETAIL']
        end
        except_role :admin do
          field 'Market/Channel', :select, :above, options: ['CANADA-ONLINE', 'CANADA-RETAIL', 'CHINA-RETAIL(CHN)', 'CHINA-RETAIL(HKG)', 'EU-FRANCHISE', 'EU-ONLINE', 'EU-RETAIL', 'HK-FRANCHISE', 'JAPAN-RETAIL', 'US-ONLINE', 'US-RETAIL']
        end

        except_role :vendor do
          field 'Option Status', :select, :above,  options: ['CONFIRM', 'EMAILED', 'Client REJECT', 'NEW', 'NON-CONFIRM', 'OFFERED', 'PROMOTE TO GP', 'RE-REQUEST TO CT', 'VENDOR REBID', 'VENDOR REJECT', 'WORK']
          field 'Agent Name', :lookup,  :above
          field 'Vendor Name', :lookup,  :above
          field 'Liked\n\ Option?', :select,  :above,  :options => ['Yes']
          field 'Global Production', :select,  :above
          field 'RFQ Split Reason', :text,  :above
          field 'Merch Type', :select,  :above,  :options => ['BASIC', 'FASHION', 'SEASONAL BASIC']
        end

        role :vendor do
          field 'Option Status', :select, :above,  options: ['CONFIRM', 'EMAILED', 'Client REJECT', 'NEW', 'NON-CONFIRM', 'VENDOR REBID', 'VENDOR REJECT', 'WORK']
        end

        except_role :tp, :vendor do
          field 'Rollup', :text,  :above
        end
      end
      section 'Hierarchy Search' do
        field 'Brand', :lookup, :above
        field 'Category', :lookup, :above
        field 'Sub Category', :lookup, :above
      end
    end

    table_panel 'Search List' do
      has_checkbox
      column :expand, :expand, append: [PageModule::GCMSellChannelsSubTable, PageModule::GCMOptionsSubTable]
      column 'BOM #', :text, :readonly
      column 'RFQ Split Reason', :text, :readonly
      column 'RFQ Status', :text, :readonly
      column 'Style Description', :text, :readonly
      column 'Master Style #', :text, :readonly
      column 'Master Style Description', :text, :readonly
      column 'Season', :text, :readonly
      column 'Year', :text, :readonly
      column 'Global Style Target Qty', :text, :readonly
      column 'Brand', :text, :readonly
      column 'Division', :text, :readonly
      column 'Dept', :text, :readonly
      column 'Class', :text, :readonly
      column 'Sub Class', :text, :readonly
      column 'Category', :text, :readonly
      column 'Sub Category', :text, :readonly

      role :vendor do
        column 'Style RFQ No', :text, :readonly
      end
      except_role :vendor do
        column 'Style RFQ No', :link
        column 'Merch Type ', :text,  :readonly
        column 'Global Production', :text, :readonly
      end
    end

end


