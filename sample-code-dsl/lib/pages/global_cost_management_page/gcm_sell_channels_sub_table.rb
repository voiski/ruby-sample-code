module PageModule
  # Embedded Sell Channels table for a rfq item at the GCM screen
  module GCMSellChannelsSubTable

    def self.compose
      lambda do |klass|
        klass.sub_table_panel 'Sell Channels', :collapsed do
          has_checkbox
          column 'Market/Channel', :lookup, readonly?{ not_role? :admin }
          except_role :vendor do
            column 'Initial Retail Price', :text, readonly?{ role? :ct }
            column 'Cur', :lookup, readonly?{ role? :ct }
          end
          column 'Packaging Upcharge', :text, readonly?{ role? :vendor }
        end
      end # end lambda
    end

  end
end
