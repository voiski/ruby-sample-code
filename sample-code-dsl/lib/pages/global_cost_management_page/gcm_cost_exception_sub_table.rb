module PageModule
  # Embedded Cost Exception table for the Option Market/Channels table at the GCM screen
  module GCMCostExceptionSubTable

    def self.compose
      lambda do |klass|
        klass.sub_table_panel 'Cost Exception' do
          has_checkbox
          column 'BOM CC #', :text, :readonly
          column 'Fabric Variance', :text, :readonly
          column 'Primary Fabric YY Variance', :text, :readonly
          column 'Trim Variance', :text, :readonly
          column 'Graphic Variance', :text, :readonly
          column 'Wash Variance', :text, :readonly
          column 'C&M Variance', :text, :readonly
          column 'First Cost', :text, :readonly
          except_role :vendor do
            column 'ELC', :smart_tag, :readonly
          end
          column 'Cost Exception Comments ', :text, :readonly
        end
      end # end lambda
    end

  end
end
