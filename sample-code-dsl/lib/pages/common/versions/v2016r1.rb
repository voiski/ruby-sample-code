module PageModule
  module Versions
    # Definition for the TS version 2016R1
    class V2016R1
      include PageModule::Versions::VersionBase

      group :login do
        user_id  '//input[@name=\'user_id\']'
        password '//input[@name=\'pswd\']'
        submit   '//button[@name=\'Submit\']'
      end

      group :header, '//nav[@id=\'tssHeader\']' do
        logout '//a[text()=\'Logout\']' do |value|
          page.find(header_user_name).click
          value
        end
        home_link '//a[@title=\'Home\']'
        user_name '//a[@data-dropdown=\'userCtrlDrop\']'
      end

      group :footer do
        area "/html/body/form/div[@id='TSSBOT']"
      end

    end # end class
  end
end
