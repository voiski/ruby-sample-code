module PageModule
  # This module enables the version feature on the class that includes it.
  #
  # To change the version, default will be 2
  # PageModule::Versions.init 2
  #
  # Then ask for the XPath like this:
  # PageModule::Versions.key_that_you_need
  module Versions

    def self.xpath(key, placeholders={})
      result = current_version[key]
      if result.is_a? String
        result % placeholders
      else
        instance_exec placeholders, &result
      end
    end

    def self.init(version)
      @current_version = case version
      when /2015R2/
        PageModule::Versions::V2015R2.new
      when /2016R1/
        PageModule::Versions::V2016R1.new
      else
        init '2016R1'
      end
    end

    def self.method_missing(m)
      current_version.send m
    end

    def self.current_version
      # Default value
      @current_version ||= init TS_VERSION
    end

  end
end
