module PageModule
  module Versions
    # Definition for the TS version 2015R2
    class V2015R2
      include PageModule::Versions::VersionBase

      group :login do
        user_id  '//input[@name=\'user_id\']'
        password '//input[@name=\'pswd\']'
        submit   '//input[@name=\'Submit\']'
      end

      group :header, '//tr[@id=\'TSSHEADER\']' do
        logout    "//label[contains(string(),'logout')]"
        home_link "//label[contains(string(),'Dashboard')]"
        user_name "/td[@class='clsBanner']//td[@class='clsUserGreetingText']"
      end

    end # end class
  end
end
