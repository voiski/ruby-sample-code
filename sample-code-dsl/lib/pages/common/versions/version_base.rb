module PageModule
  module Versions
    # Base class for the version definition.
    module VersionBase

      def self.included(base)
        base.extend(ClassMethods)
      end

      module ClassMethods

        # Only a wrap for organization
        def group(key,base_xpath=nil)
          @context = key
          @base_xpath = base_xpath
          yield
          @context = @base_xpath = nil
        end

        def xpaths
          @xpaths||={}
        end

        # Defines a new xpath, you can pass a string or a block.
        # Ex:
        #   xpath :base_header, '//xpath/for/the/field'
        #   xpath :user_name { "#{base_header}/input" }
        def xpath(key, value=nil, &block)
          key = "#{@context}_#{key}".to_sym if @context
          value = "#{@base_xpath}#{value}" if (@base_xpath&&value)
          xpaths[key] = if block
            define_method key do |*args|
              instance_exec value, *args, &block
            end
            block
          else
            define_method key do
              value
            end
            value
          end
          @xpaths
        end

        # This enable the usage of the method name as the key for the xpath.
        # Ex:
        #   user_name '//xpath/for/the/field'
        def method_missing(m, *args, &block)
          xpath m.to_sym, *args, &block
        end

      end
    end
  end
end
