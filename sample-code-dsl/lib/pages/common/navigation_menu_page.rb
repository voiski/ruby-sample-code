module PageModule
  class NavigationMenuPage < Page
    include Components::TS::PageDesign

    navigation_menu 'ORDER MANAGEMENT', :collapsed, :admin do
      navigation_link 'ORDER MANAGEMENT DASHBOARD'
      navigation_link 'Dashboard'
      navigation_link 'Order(s)'
    end

    navigation_menu 'TRADE MANAGEMENT', :collapsed, :admin do
      navigation_link 'TRADE MANAGEMENT DASHBOARD'
      navigation_link 'Dashboard'
      navigation_link 'Sales Order'
    end

    navigation_menu 'VENDOR MANAGEMENT', :collapsed, :admin do
      navigation_link 'VENDOR MANAGEMENT DASHBOARD'
      navigation_link 'Dashboard'
      navigation_link 'Party'
    end

    navigation_menu 'SOURCING', :collapsed do
      navigation_link 'SOURCING DASHBOARD'
      navigation_link 'Dashboard'
      navigation_link 'Global Cost Management'
      except_role :vendor do
        navigation_link 'Style/RFQ(s)'
      end
      only_role :admin do
        navigation_link 'Quantities by Delivery'
      end
    end

    navigation_menu 'SEARCH', except: :vendor do
      only_role :admin do
        navigation_link 'Models' do
          navigation_link 'Bom Model(s)'
          navigation_link 'Cost Model(s)'
          navigation_link 'Email Model(s)'
          navigation_link 'Notes Model(s)'
        end
      end
      navigation_link 'Vendor'
      only_role :admin do
        navigation_link 'View/Create Bundles'
        navigation_link 'Order(s)'
        navigation_link 'Respond to Option(s)'
      end
    end

    navigation_menu 'ADMIN', :collapsed, :admin do
      navigation_link 'Reload'
      navigation_link 'Look up Admin'
      navigation_link 'Configure App'
      navigation_link 'Security/Menu Setup' do
        navigation_link 'Security Role Profile'
        navigation_link 'Security Setup'
        navigation_link 'Dashboard Menu Setup'
        navigation_link 'Menu Label Setup'
        navigation_link 'WS Security Setup'
        navigation_link 'Security Snapshot'
        navigation_link 'Rearrange Dashboard'
        navigation_link 'Security Policy'
        navigation_link 'Copy Security'
        navigation_link 'Help Menu Setup'
        navigation_link 'New Security Setup'
      end
      navigation_link 'Edit I18N Resources'
      navigation_link 'Configure Numbers/Dates'
      navigation_link 'Tool Set' do
        navigation_link 'Step Builder'
        navigation_link 'Configure Screens'
        navigation_link 'Map Builder'
        navigation_link 'Data Channel Setup'
        navigation_link 'Validation Filter'
        navigation_link 'Validation Filter Report'
      end
      navigation_link 'System Security' do
        navigation_link 'Encryption'
        navigation_link 'Data Key Setup'
      end
      navigation_link 'Validations' do
        navigation_link 'New Configuration'
        navigation_link 'Basic Data Configuration'
        navigation_link 'New Validation Reports'
      end
      navigation_link 'Query' do
        navigation_link 'Query Builder'
        navigation_link 'Query Group'
        navigation_link 'Assign Query'
        navigation_link 'Assign Group'
        navigation_link 'Query Group Count'
        navigation_link 'Query List'
      end
      navigation_link 'User Profiles' do
        navigation_link 'Search Users'
        navigation_link 'Search Suppliers'
      end
      navigation_link 'Web Services' do
        navigation_link 'Test Web Services'
        navigation_link 'Search Activity Log'
      end
      navigation_link 'Layout Profile'
      navigation_link 'Language Profile'
      navigation_link 'Collaboration Field Setup'
      navigation_link 'Copy Messages'
      navigation_link 'Diagnostics'
      navigation_link 'Batch Image Conversion'
      navigation_link 'Scheduler Monitor'
      navigation_link 'Dashboard'
    end

    def validate_menu_order
      expect(get_navigation_menus).to eq self.class.ordered_menus
    end

    private

    def get_navigation_menus
      navigation_menus = []
      using_wait_time 1 do
        page.all(:xpath, "//div[@id='_divNavArea']/table/tbody/tr//table/tbody/tr[1]//div[@class='navTitle']").each do |menu_title|
          navigation_menus << menu_title.text
        end
      end
      navigation_menus
    end
  end
end
