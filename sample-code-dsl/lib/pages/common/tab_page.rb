module PageModule
  class TabPage < Page

    TABS_LABEL    = "//tr[@id='Tabsrow']/td/label"

    def get_tabs
      page.all(:xpath,TABS_LABEL).collect(&:text)
    end

    def switch_tab_to(tab_name)
      page.find("#{TABS_LABEL}[contains(text(),'#{tab_name}')]").click
    end

  end
end
