module PageModule
  module QueryModule
    class QueryPage < Page
      include Components::WaitTime

      def click_action(action)
        more_actions.click.send("#{Components::TS.to_sym(action)}")
      end

      def click_search
        search_panel.search
        wait_for_search_to_finish
      end

      def fill_search_filter(filter_name, filter_value)
        search_panel.send("#{Components::TS.to_sym(filter_name)}").value = filter_value
      end

      def get_column_values(column)
        result_panel.send("#{Components::TS.to_sym(column)}").values
      end

      def navigate_to_option_by_status(status)
        option_status = nil
        row = 0
        while option_status != status do
          row = row + 1
          option_status = listing_area.row(row).option_status.value
        end
        result_panel.row(row).column('Option No').field_at_row.click
        result_panel.row(row).column('Option No').field_at_row.click
      end

      def search_by_season_year_bomno(season,year,bom_no)
        search_panel.season.value = season
        search_panel.year.value = year
        search_panel.bom_no.value = bom_no
        click_search
      end

      def select_record(row)
        result_panel.row(row).check
      end

      def submit_options
        if self.instance_of? PageModule::QueryModule::ViewOptionsBomCostQuery
          more_actions!.rebid_summary
        elsif self.instance_of? PageModule::QueryModule::ViewOptionsSubmitQuery
          more_actions!.submit_summary
        end
      end

      def wait_auto_search
        wait_for_search_to_finish
        Components::TS.close_warning
      end

      def wait_load_search_list_panel
        wait_until { page.all("//div[@id='angular-grid']//div[@class='k-grid-header']").count > 0 }
      end

      private

      def wait_for_search_to_finish
        wait_load_search_list_panel
        wait_until { page.all(result_panel.data_table_first_row_xpath).count > 0 }
      end

    end
  end
end
