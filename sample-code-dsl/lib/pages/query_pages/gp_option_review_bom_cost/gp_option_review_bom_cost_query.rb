module PageModule
  module QueryModule
    class GpOptionReviewBomCostQuery < QueryPage
      include Components::TS::PageDesign

      roles :gp, :admin

      title 'GP Option Review - BOM Cost', :gp, :admin

      button 'More Actions...' do
        button 'Refresh'
        button 'Export'
        button 'Re-request to CT'
        button 'Non-Confirm Option'
      end

      search_panel do
        field 'Season', :select, :required, options: ['--', 'FALL', 'HOLIDAY', 'SPRING', 'SUMMER']
        field 'Year', :text, :required
        field 'BOM #', :text
        field 'Master Style #', :text
        field 'Master Style Description', :text
        field 'Brand', :text
        field 'Division', :text
        field 'Dept', :text
        field 'Class', :text
        field 'Global Production', :select
        field 'Option Description', :text
        field 'Liked Option?', :select, options: ['--', 'Yes']
        field 'Vendor Name', :text
      end

      result_panel do
        has_checkbox
        column 'Primary Sketch', :freezed
        column 'Season', :freezed
        column 'Year', :freezed
        column 'BOM #', :freezed, :link

        column 'Master Style #'
        column 'Master Style Description'
        column 'Style RFQ No'
        column 'RFQ Split Reason'
        column 'Style Description'
        column 'Brand'
        column 'Division'
        column 'Dept'
        column 'Class'
        column 'Global Production'
        column 'Option Number', :link
        column 'Option Description'
        column 'Liked Option?'
        column 'Like Option Date'
        column 'Option Status'
        column 'Country Of Origin'
        column 'Cost Exception'
        column 'BOM CC #'
        column 'BOM CC Description'
        column 'Fabric Variance'
        column 'Primary Fabric YY Variance'
        column 'Trim Variance'
        column 'Graphic Variance'
        column 'Wash Variance'
        column 'C&M Variance'
        column 'First Cost'
        column 'Landed Factor'
        column 'ELC'
        column 'Primary Fabric Gauge'
        column 'Primary Fabric Stitch'
        column 'Primary Fabric Ends'
        column 'Primary Fabric Cost'
        column 'Primary Fabric Wastage %'
        column 'Primary Fabric Pounds per Dozen'
        column 'Primary Fabric YY'
        column 'Ttl Fabric Cost'
        column 'Ttl Graphic Cost'
        column 'Ttl Wash Cost'
        column 'Ttl Trim Cost'
        column 'Cut and Make Cost'
        column 'Ttl Packaging Cost'
        column 'Actual Response Date'
        column 'Vendor Name'
        column 'Category Manager'
      end

    end
  end
end
