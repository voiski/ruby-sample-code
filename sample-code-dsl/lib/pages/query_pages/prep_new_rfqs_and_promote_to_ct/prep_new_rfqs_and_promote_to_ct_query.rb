module PageModule
  module QueryModule
    class PrepNewRfqsAndPromoteToCtQuery < EditableQueryPage
      include Components::TS::PageDesign

      roles :gp, :admin

      title 'Prep New RFQs & Promote to CT'

      button 'More Actions...' do
        button 'Export'
        button 'Refresh'
        button 'Ready for CT'
      end

      search_panel do
        field 'Season', :select, options: ['--', 'FALL', 'HOLIDAY', 'SPRING', 'SUMMER']
        field 'Year', :text
        field 'BOM #', :text
        field 'Master Style #', :text
        field 'CT Response Due Date', :text
        field 'Global Production', :select
        field 'Brand', :text
        field 'Division', :text
        field 'Dept', :text
        field 'Class', :text
        field 'Category', :text
        field 'Sub Category', :text
        field 'Age', :select, options: ['--', 'ADULT', 'INFANT', 'KIDS', 'N/A', 'NEWBORN', 'TODDLER']
        field 'Gender', :select, options: ['--', 'FEMALE', 'MALE', 'N/A', 'UNISEX']
        field 'Merch Type', :select, options: ['--', 'BASIC', 'FASHION', 'SEASONAL BASIC']
      end

      result_panel do
        has_checkbox
        column 'Primary Sketch', :freezed
        column 'Season', :freezed
        column 'Year', :freezed
        column 'BOM #', :freezed

        column 'Master Style #'
        column 'Style RFQ No', :link
        column 'RFQ Status'
        column 'RFQ Split Reason'
        column 'Style Description'
        column 'CT Response Due Date', :text
        column 'Target AUC', :text
        column 'Global Production'
        column 'Brand'
        column 'Division'
        column 'Dept'
        column 'Class'
        column 'Category'
        column 'Sub Category'
        column 'Age'
        column 'Gender'
        column 'Merch Type'
      end

    end
  end
end

