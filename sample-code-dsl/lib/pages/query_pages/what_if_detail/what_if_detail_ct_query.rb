module PageModule
  module QueryModule
    # TODO: merge with gp one, we can use the dsl to check the fill up, fill down and map the buttons like the save button
    class WhatIfDetailCtQuery < QueryPage
      include Components::TS::PageDesign

      roles :ct, :admin

      title 'What If Detail'

      search_panel do
        field 'Season', :select, options: ['--', 'FALL', 'HOLIDAY', 'SPRING', 'SUMMER']
        field 'Year', :text
        field 'Rollup', :text
        field 'BOM #', :text
        field 'Master Style #', :text
        field 'Style RFQ No', :text
        field 'Brand', :text
        field 'Division', :text
        field 'Dept', :text
        field 'Class', :text
        field 'Sub Class', :text
        field 'Category', :text
        field 'Sub Category', :text
        field 'Option No', :text
        field 'Option Description', :text
        field 'Option Status', :select, options: ['--', 'RE-REQUEST TO CT', 'GAP REJECT', 'VENDOR REBID', 'PROMOTE TO GP', 'VENDOR REJECT', 'NON-CONFIRM', 'OFFERED', 'CONFIRM', 'EMAILED', 'NEW', 'WORK']
        field 'Vendor Name', :text
      end

      result_panel do
        column 'Primary Sketch', :freezed
        column 'Season', :freezed
        column 'Year', :freezed
        column 'Rollup', :freezed

        column 'BOM #'
        column 'Master Style #'
        column 'Style RFQ No', :link
        column 'Style Description'
        column 'RFQ Split Reason'
        column 'Brand'
        column 'Division'
        column 'Dept'
        column 'Class'
        column 'Sub Class'
        column 'Category'
        column 'Sub Category'
        column 'Option No', :link
        column 'Option Description'
        column 'Option Status'
        column 'Vendor Name'
        column 'First Cost'
        column 'Landed Factor'
        column 'ELC'
        column 'IMU%'
        column 'Est First Cost'
        column 'Est ELC'
        column 'What-If Comments'
        column 'Est IMU%'
        column 'Global Style Target Qty'
        column 'Target AUC'
        column 'Target IMU%'
        column 'Terms of Sale'
        column 'Ship Mode'
        column 'Country of Origin'
      end
    end
  end
end

