module PageModule
  module QueryModule

    class EditableQueryPage < QueryPage
      SAVE_BUTTON_XPATH = "//table[@id='qvSave']//label[contains(text(), 'Save')]"
      FILL_UP_BUTTON_XPATH = "//div[@id='edit-components']/label[@id='fillUp']"
      FILL_DOWN_BUTTON_XPATH = "//div[@id='edit-components']/label[@id='fillDown']"
      FILL_SELECTED_BUTTON_XPATH = "//div[@id='edit-components']/label[@id='fillSelected']"

      def initialize(check_elements = false)
        super(check_elements)
        @save_button = Components::Link.new(SAVE_BUTTON_XPATH)
        @fill_down_button = Components::Link.new(FILL_DOWN_BUTTON_XPATH)
      end

      def check_layout
        super
        page.should have_xpath SAVE_BUTTON_XPATH
        page.should have_xpath FILL_UP_BUTTON_XPATH
        page.should have_xpath FILL_DOWN_BUTTON_XPATH
        page.should have_xpath FILL_SELECTED_BUTTON_XPATH
      end

      def click_on_save
        @save_button.click
      end

      def click_on_fill_down
        @fill_down_button.click
      end
    end

  end
end
