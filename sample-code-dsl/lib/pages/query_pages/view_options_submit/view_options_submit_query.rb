module PageModule
  module QueryModule
    class ViewOptionsSubmitQuery < QueryPage
      include Components::TS::PageDesign

      title 'View CT Options: Submit', :ct, :admin
      title 'View Options: Submit', :tp

      roles :ct, :tp, :admin

      button 'More Actions...' do
        button 'Export'
        button 'Refresh'
        button 'Submit (Summary)'
        button 'Submit (Detailed)'
      end

      search_panel do
        field 'Season', :select, options: ['--', 'FALL', 'HOLIDAY', 'SPRING', 'SUMMER']
        field 'Year', :text
        field 'BOM #', :text
        field 'Master Style #', :text
        field 'Brand', :text
        field 'Division', :text
        field 'Dept', :text
        field 'Class', :text
        field 'Category', :text
        field 'Sub Category', :text
        field 'Vendor Name', :text
        field 'CT', :select
      end

      result_panel do
        has_checkbox
        column 'Primary Sketch', :freezed
        column 'Season', :freezed
        column 'Year', :freezed
        column 'BOM #', :freezed, :link

        column 'Master Style #'
        column 'Style RFQ No'
        column 'RFQ Split Reason'
        column 'Style Description'
        column 'Option No', :link
        column 'Option Description'
        column 'Option Status'
        column 'Brand'
        column 'Division'
        column 'Dept'
        column 'Class'
        column 'Category'
        column 'Sub Category'
        column 'First Cost'
        column 'Actual Response Date'
        column 'Vendor Name'
        column 'CT'
        column 'CT Comments'
        column 'Vendor Comments'
      end

    end
  end
end

