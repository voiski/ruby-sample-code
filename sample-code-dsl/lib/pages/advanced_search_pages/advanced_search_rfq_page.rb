module PageModule
  module QueryModule
    class AdvancedSearchRfqPage < QueryPage
      include Components::TS::PageDesign
      include Components::TS::SavedSearch

      roles :gp, :ct, :tp, :admin

      title 'Request: '
      subtitle 'List'

      button 'Saved Searches'

      advanced_search_panel 'Request For Quote' do
        section 'Field Search' do
          field 'Season', :select, :above,  options: ['FALL','HOLIDAY','SPRING','SUMMER']
          field 'Year', :text, :lookup, :above
          field 'Style RFQ No', :text, :above
          field 'RFQ Status', :select, :above,  options: ["COMPLETED", "CONFIRM", "DROPPED", "NEW", "READY FOR CT"]
          field 'Brand', :text, :lookup, :above
          field 'Division', :text, :lookup, :above
          field 'Dept', :text, :lookup, :above
          field 'Class', :text, :lookup, :above
          field 'Sub Class', :text, :lookup, :above
          field 'Merch Type', :select, :above,  options: ["BASIC", "FASHION", "SEASONAL BASIC"]
          field 'Category', :text, :lookup, :above
          field 'Sub Category', :text, :lookup, :above
          field 'Item Type', :text, :lookup, :above
          field 'Age', :select, :above,  options: ["ADULT", "INFANT", "KIDS", "N/A", "NEWBORN", "TODDLER"]
          field 'Gender', :select, :above,  options: ["FEMALE", "MALE", "N/A", "UNISEX"]
          field 'BOM #', :text, :above
          field 'Style Description', :text, :above
          field 'Master Style #', :text, :above
          field 'Master Style Description', :text, :above
          field 'CT Response Due Date', :calendar, :above
          field 'Global Production', :select,  :above
          field 'CT', :select, :above
          field 'RFQ Split Reason', :text, :above
        end
        section 'Change Tracking Search' do
          field 'Changes Since', :select, :above,  options: ["Today", "Yesterday", "Last Week", "Last Month", "All"]
        end
      end

      table_panel 'Search List' do
        has_checkbox
        # column 'Sketch'
        column 'Style RFQ No', :link
        column 'BOM #', :text, :readonly
        column 'Style Description', :text, :readonly
        column 'Brand', :text, :readonly
        column 'Division', :text, :readonly
        column 'Dept', :text, :readonly
        column 'Master Style #', :text, :readonly
        column 'Master Style Description', :text, :readonly
        column 'Season', :text, :readonly
        column 'Year', :text, :readonly
        column 'RFQ Status', :text, :readonly
        column 'RFQ Split Reason', :text, :readonly
        column 'CT Response Due Date', :text, :readonly
      end


    end
  end
end


