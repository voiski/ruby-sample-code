# Base page module with the factory for pages based on the group and name
module PageModule

  # Create a instance of the page based on the given group and query. Inside it
  # will choose the factory method based on the group and wil pass the query and
  # the others args.
  # Ex:
  #  PageModule.create_page 'Category Manager Workflow', 'Upcharge & Sell Channel Maint.'
  def self.create_page(group,query, *args)
    case group
    when 'Category Manager Workflow'
      page_from_category_manager_workflow query, *args
    when 'Global Production Workflow'
      page_from_global_production_workflow query, *args
    when 'Ext Designed Workflow'
      page_from_ext_designed_workflow query, *args
    when 'Vendor Workflow'
      page_from_vendor_workflow query, *args
    else
      raise "The group #{group} doesn't exist!"
    end
  end

  # Category Manager Workflow page object factory
  def self.page_from_category_manager_workflow(query, *args)
    page_from_query_workflow(query, *args)||case query
      when 'View CT Options: ReRequest'
        PageModule::QueryModule::ViewCtOptionsReRequestQuery.new
      else
        nil # maybe it is a saved myview, handle it on the step
      end
  end

  # Global Production Workflow page object factory
  def self.page_from_global_production_workflow(query, *args)
    page_from_query_workflow(query, *args)||case query
      when 'View GP RFQ\'s: NEW'
        PageModule::QueryModule::ViewGpRfqsNewQuery.new
      when 'Prep New RFQs & Promote to CT'
        PageModule::QueryModule::PrepNewRfqsAndPromoteToCtQuery.new
      when 'GP Option Review - BOM Cost'
        PageModule::QueryModule::GpOptionReviewBomCostQuery.new
      else
        nil # maybe it is a saved myview, handle it on the step
      end
  end

  # Ext Designed Workflow page object factory
  def self.page_from_ext_designed_workflow(query, *args)
    page_from_query_workflow(query, *args)
  end

  # Vendor Workflow page object factory
  def self.page_from_vendor_workflow(query, *args)
    page_from_query_workflow(query, *args)||case query
      when 'View and Respond to Options'
        PageModule::QueryModule::ViewAndRespondToOptionsQuery.new
      when 'View Options: In Work'
        PageModule::QueryModule::ViewOptionsInWorkQuery.new
      when 'View Options: Offered'
        PageModule::QueryModule::ViewOptionsOfferedQuery.new
      when 'View Options: Rejected'
        PageModule::QueryModule::ViewOptionsRejectedQuery.new
      when 'View Options: Confirmed'
        PageModule::QueryModule::ViewOptionsConfirmedQuery.new
      when 'BOM Components Mass Update'
        PageModule::QueryModule::BomComponentMassUpdateQuery.new
      when 'Cost Exception Maintenance'
        PageModule::QueryModule::CostExceptionMaintenanceQuery.new
      when 'View Sell Channel Quantities'
        PageModule::QueryModule::ViewSellChannelQuantitiesQuery.new
      when 'Upcharge Maintenance'
        PageModule::QueryModule::UpchargeMaintenanceQuery.new
      else
          nil # maybe it is a saved myview, handle it on the step
      end
  end

  private

  # Category Manager Workflow page object factory
  def self.page_from_query_workflow(query, *args)
    case query
    when /^View( CT| GP)? Options: All$/
      PageModule::QueryModule::ViewOptionsAllQuery.new
    when /^View( CT)? Options: New$/
      PageModule::QueryModule::ViewOptionsNewQuery.new
    when /^View( CT)? Options: Submit$/
      PageModule::QueryModule::ViewOptionsSubmitQuery.new
    when /^View( CT)? Options: Confirm$/
      PageModule::QueryModule::ViewOptionsConfirmQuery.new
    when /^View( CT)? Options: BOM Cost$/
      PageModule::QueryModule::ViewOptionsBomCostQuery.new
    when 'Assign Bundles'
      PageModule::QueryModule::AssignBundlesQuery.new
    when 'Update Sourcing Info'
      PageModule::QueryModule::UpdateSourcingInfoQuery.new
    when 'Rate Card Comparison'
      PageModule::QueryModule::RateCardComparisonQuery.new
    when 'Upcharge & Sell Channel Maint.'
      PageModule::QueryModule::UpchargeAndSellChannelMaintQuery.new
    when 'RSC Attribute'
      PageModule::QueryModule::RscAttributeQuery.new
    when 'Update CC LFL Assignments'
      PageModule::QueryModule::UpdateCcLflAssignmentsQuery.new
    when 'Update Target Information'
      PageModule::QueryModule::UpdateTargetInformationQuery.new
    when 'Update Comp Style'
      PageModule::QueryModule::UpdateCompStyleQuery.new
    else
      nil # maybe it is a saved myview, handle it on the step
    end
  end
end
