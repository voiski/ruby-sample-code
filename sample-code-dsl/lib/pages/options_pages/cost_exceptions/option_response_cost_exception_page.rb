module PageModule
  class OptionResponseCostExceptionPage < OptionResponseTab

    button 'Delete', except: :gp
    button 'Save', except: :gp

    append PageModule::OptionInformationPanel

    sections_panel 'Cost Exceptions' do
      panel_section 'Select CCs for Cost Exceptions', post:[:admin,:ct,:tp,:vendor] do
        table_title 'Cost Exceptions'

        has_checkbox
        column 'BOM CC #'
        column 'Fabric Variance', :text
        column 'Primary Fabric YY Variance', :text
        column 'Trim Variance', :text
        column 'Graphic Variance', :text
        column 'Wash Variance', :text
        column 'C&M Variance', :text
        column 'First Cost'
        if not roles? :vendor
          column 'ELC'
        end
        column 'Cost Exception Comments', :text
      end
    end

  end
end
