module PageModule
  class OptionResponseAttachmentsPage < OptionResponseTab
    include Components::TS::PageDesign

    button 'More Actions', except: :vendor do
      button 'Delete'
      button 'Refresh'
    end

    button 'Save'

    append PageModule::OptionInformationPanel

    table_panel 'Option Attachments' do
      has_checkbox
      column 'File Name', :text, :readonly
      column 'Thumbnail', :attachment
      column 'Description', :textarea
      column 'Modified By', :text, :readonly
      column 'Date Modified', :text, :readonly
    end
  end
end
