module PageModule
  module OptionInformationPanel

    def self.compose
      lambda do |klass|
        klass.panel 'Option Information', :collapsed do
          field 'Style RFQ No', :plain_text
          field 'BOM #', :text, :readonly
          field 'Style Description', :text, :readonly
          field 'Option No', :plain_text
          field 'Option Description', :text, :readonly
          field 'Option Status', :text, :readonly
          except_role :vendor do
            field 'Vendor Name', :plain_text
          end
          field 'Costing Level', :text, :readonly
          field 'First Cost', :text, :readonly
        end
      end
    end

  end
end
