module PageModule
  class OptionResponseTab < Page
    include Components::TS::PageDesign

    tab 'Overview'
    tab 'Attachments'
    tab 'Notes', only: :admin
    tab 'Bill Of Material'
    tab 'Costing Detail', only: :admin
    tab 'Cost Exceptions'
    tab 'Carton Code', only: :admin
    tab 'Change Tracking', except: :vendor

    title 'Offer Response'

    def get_rfq_and_option_number_from_title
      Components::PlainText.new("//table[@id='_contentviewerHeaderBar_divWorkArea']//label[@class='clsTextLabelNormal']").get_text
    end

  end
end
