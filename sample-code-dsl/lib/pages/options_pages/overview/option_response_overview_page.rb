module PageModule
  class OptionResponseOverviewPage < OptionResponseTab

    button 'Actions...' do
      button 'Copy Option'
      button 'Refresh'
      only_role :admin do
        button 'Print Offer'
      end
      only_role :admin, :ct, :vendor, :tp do
        button 'Offer Option'
      end
      only_role :vendor do
        button 'Vendor Reject'
      end
      only_role :admin, :ct, :tp do
        button 'Rte Crd Cst OK'
      end
    end

    button 'Save'

    button 'Global Cost Management', only: :vendor

    button 'Go To...', except: :vendor do
      button 'Global Cost Management'
      button 'View Request'
    end

    sections_panel 'Sketch & Tech Pack', :collapsed do
      section 'Primary Sketch' do
        field :empty_label, :link, :above
      end
      section 'Tech Pack' do
        field :empty_label, :link, :above
      end
    end

    sections_panel 'Option Overview' do
      section 'Style Information' do
        field 'BOM #', :above, :readonly, :text
        field 'Style Description', :above, :readonly, :text
        field 'Master Style #', :above, :readonly, :text
        field 'Master Style Description', :above, :readonly, :text
        except_role :gp, :tp do
          field 'Merch Type', :above, :readonly, :select
        end
        only_role :ct, :tp do
          field 'LFL Style?', :above, :select, :readonly, :options => ['No','Yes']
          field 'LFL Style #', :above, :text, :readonly
        end
        only_role :gp, :tp do
          field 'Season', :above, :readonly, :text
          field 'Year', :above, :readonly, :text
          field 'Brand', :above, :readonly, :text
          field 'Division', :above, :readonly, :text
          field 'Dept', :above, :readonly, :text
          field 'Class', :above, :readonly, :text
          field 'Sub Class', :above, :readonly, :text
          field 'Fit Type', :above, :readonly, :text
        end
      end
      section 'Option Information' do
        field 'Option Status', :above, :readonly, :select
        field 'Option No', :above, :readonly, :text
        only_role :gp do
          field 'Option Description', :above, :readonly, :text
        end
        only_role :ct, :tp, :admin, :vendor do
          field 'Option Description', :above, :text
        end
        field 'Original Option No', :above, :readonly, :text
        field 'Vendor', :above, :readonly, :smart_tag
        field 'First Cost', :above, :readonly, :text
        except_role :vendor do
          field 'ELC', :above, :readonly, :text
        end
        field 'Cur', :above, :readonly, :text
        field 'Costing Level', :above, :readonly, :text
        except_role :gp do
          field 'Vendor Response\nDue Date', :above, :readonly, :text
          field 'Actual Response\n\ Date', :above, :readonly, :text
          field 'Costs Valid Till', :above, :calendar
        end
        only_role :gp, :tp do
          field 'Like Option Date', :above, :readonly, :text
          field 'Auto\n\ Re-Like?', :above, :readonly, :select, :options => ['YES']
        end
      end
      section 'Production Information' do
        only_role :admin, :gp, :tp do
          field 'Season',:select, :above, :readonly
          field 'Year', :above, :readonly, :text
        end
        field 'In DC Date', :above, :calendar, :readonly
        field 'Flow', :above, :readonly, :text
        only_role :vendor do
          field 'Global Style\nTarget Qty', :above, :readonly, :text
        end
        except_role :vendor do
          field 'Global Style Target Qty', :above, :readonly, :text
        end
        field 'CC Confirmed Qty', :above, :readonly, :text
        only_role :ct, :gp, :tp do
          field 'CC Units Source', :above, :select, :readonly
        end
        only_role :gp, :tp do
          field 'Merch Type',:above, :readonly, :select
        end
        only_role :gp, :tp do
          field 'Target AUC',:above, :readonly, :text
          field 'Buy Size',:above, :readonly, :select
          field 'Adoptable Style?',:above, :readonly, :select
          field 'Production Comp',:above, :readonly, :text
        end
      end

      section 'RFQ Information' do
        field 'Style RFQ No', :above, :readonly, :text
        field 'RFQ Status', :above, :readonly, :select
        only_role :ct, :vendor do
          field 'Season',:select, :above, :readonly
          field 'Year', :above, :readonly, :text
        end
        except_role :vendor do
          field 'CT Response Due Date', :above, :readonly, :text
          field 'Global Production', :above, :readonly, :select
        end
        only_role :admin do
          field 'Category Team', :above, :readonly, :select
        end
        except_role :admin do
          field 'CT', :above, :select, :readonly
        end
        only_role :gp, :tp do
          field 'RFQ Split Reason', :above, :readonly, :textarea
        end
      end
      except_role :gp do
        section 'Logistics Information' do
          only_role :admin do
            field 'Terms of Sale', :above, :lookup
            field 'Ship Mode', :above, :select,:options => ['Air', 'Ocean', 'Truck']
            field 'Destination\n\ Country', :above, :readonly, :text
            field 'Transfer\n\ Point', :above, :lookup
            field 'Country\n\ of Origin', :above, :lookup
            field 'Freight Paid By', :above, :select, :options => ['PURCHASER', 'PURCHASER/ VENDOR', 'VENDOR', 'VENDOR PAY 50%']
            field 'Pack\n\ Factor', :above, :text
          end
          except_role :admin do
            except_role :vendor do
              field 'Terms of Sale', :above, :readonly
              field 'Ship Mode', :above, :select, :readonly, :options => ['Air', 'Ocean', 'Truck']
              field 'Destination\n\ Country', :above, :readonly, :text
            end
            readonly = :readonly if (roles? :ct or roles? :tp)
            field 'Transfer\n\ Point', :above, :readonly
            field 'Country\n\ of Origin', :above, :readonly
            field 'Freight Paid By', :above, :select, :readonly, :options => ['PURCHASER', 'PURCHASER/ VENDOR', 'VENDOR', 'VENDOR PAY 50%']
            field 'Pack\n\ Factor', :above, :text, readonly
          end
        end
        except_role :vendor, :tp, :ct do
          section 'Carton Packaging'do
            field 'Carton Type', :select,:options => ['CUSTOM','FF3-Centimeters','FF3-Inches','GID-Mini-Centimeters','GID-Mini-Inches',
                                                      'GID-Other-Centimeters','GID-Other-Inches','GID-Small-Centimeters','GID-Small-Inches',
                                                      'GID-Standard-Centimeters','GID-Standard-Inches','Mini-Centimeters','Mini-Inches',
                                                      'Small-Centimeters','Small-Inches','Standard-Centimeters','Standard-Inches']
            field 'Length', :text
            field 'UM', :text
            field 'Width', :text
            field 'UM', :text
            field 'Height', :text
            field 'UM', :text
          end
        end
        section 'Duty Classification' do
          readonly = :readonly if (roles? :ct or roles? :tp)
          field 'Duty Class. Code', :lookup, :above, readonly
          field 'Import Category', :text, :above, :readonly
          field 'Product Type', :text, :above, :readonly
          field 'Chief Weight Fiber', :text, :above, :readonly
        end
      end
    end

    sections_panel 'BOM Cost Subtotals' do
      section 'BOM Cost Subtotals' do
        field :empty_label, :textarea, :readonly
      end
    end

    sections_panel 'Cost Exceptions' do
      table_section 'Cost Exceptions' do
        has_checkbox
        column 'BOM CC #', :readonly, :text
        column 'Fabric Variance', :text
        column 'Primary Fabric YY Variance', :text
        column 'Trim Variance', :text
        column 'Graphic Variance', :text
        column 'Wash Variance', :text
        column 'C&M Variance', :text
        column 'First Cost', :readonly, :text
        if not roles? :vendor
          column 'ELC', :readonly, :text
        end
        column 'Cost Exception Comments', :text
      end
    end

    sections_panel 'Customer Choices', :collapsed do
      table_section 'Customer Choices' do
        has_checkbox
        column 'On RFQ?', :select, :readonly
        column 'CC Status', :select, :readonly
        column 'BOM CC #', :text, :readonly
        column 'BOM CC Description', :text, :readonly
        column 'CC Qty', :text, :readonly
        only_role :ct, :tp do
          column 'LFL BOM CC #', :text
        end
      end
    end

    sections_panel 'Comments & Notes', :collapsed, :admin, :ct, :vendor do
      self.collapsed = false if roles? :vendor
      readonly_ct = :readonly if not (roles? :ct)
      readonly_ct_or_admin = :readonly if not (roles? :ct or roles? :admin)
      readonly_vendor = :readonly if not (roles? :vendor)
      readonly_vendor_or_admin = :readonly if not (roles? :vendor or roles? :admin)
      section 'Option Comments' do
        field 'CT Comments', :textarea, readonly_ct_or_admin
        field 'Vendor Comments', :textarea, readonly_vendor_or_admin
      end
      section 'CT Notes' do
        field :empty_label, :textarea, readonly_ct
      end
      section 'Vendor Notes' do
        field :empty_label, :textarea, readonly_vendor
      end
    end

    sections_panel 'Rate Cards', :collapsed, :admin, :ct do
      section 'Rate Card Information' do
        readonly = :readonly if roles? :ct
        field 'Associated \n\ Rate Card', :above, :select, readonly, :options => ['YES']
        field 'Garment Applied\nVolume Tier', :above, :readonly, :text
        field 'Wash Applied\nVolume Tier', :above, :readonly, :text
        field 'YY', :above, :readonly, :text
        field 'Fin %', :above, :readonly, :text
        field 'CM', :above, :readonly, :text
        field 'RteCrd Cst OK?', :above, :select, readonly, :options => ['YES']
        field 'Rate Card Updated?', :above, :select, readonly, :options => ['YES']
      end
      section 'Wash Rate Card' do
        readonly = :readonly if (roles? :ct or roles? :vendor)
        field :empty_label, :textarea, readonly
      end
    end

    sections_panel 'Miscellaneous', :collapsed, :admin do
      section 'Bundles' do
        field 'Bundle No', :text, :readonly
        field 'Bundle Desc', :textarea, :readonly
      end
    end

  end
end
