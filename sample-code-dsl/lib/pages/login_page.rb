module PageModule
  class LoginPage < Page

    #Initialize components
    def initialize(check_elements = true)
      super(check_elements)
      @logger         = Logger.new(STDOUT)
      @login_field    = Components::TextField.new PageModule::Versions.login_user_id
      @password_field = Components::TextField.new PageModule::Versions.login_password
    end

    #logs into the application
    def fill_login_form(username, password)
      @login_field.set_text username.upcase
      @password_field.set_text password
    end

    #Submit filled information
    def submit
      find(PageModule::Versions.login_submit).click
    end

    def login(role)
      user_name = Object.const_get("AUTOMATION_" + role.upcase)
      password = USER[role.downcase]['password']
      credentials = {
          'username' => user_name,
          'password' => password
      }
      @logger.info("Login user: '#{credentials['username']}'")
      fill_login_form(credentials['username'], credentials['password'])
      submit()
      Page.log_in_user
      Page.set_current_role role.downcase == 'ct2' ? 'ct' : role.downcase

      using_wait_time 10 do
        page.find("//div[@id='_divNavArea']")
      end
      wait_until({:timeout => 10}) { page.all("//div[@class='dashOuter']").count > 0 }
      DashboardPage.create
    end

    def self.logged(role)
      Page.log_in_user
      Page.set_current_role role
    end

    def has_vendor_key_expired
      using_wait_time 3 do
        page.has_text?('The Tradestone Key has expired.')
      end
    end

    def go_to_renewal_page
      renewal_link = Components::Link.new("//a[contains(@href,'devkey.xxx.com')]")
      renewal_link.click
    end
  end
end
