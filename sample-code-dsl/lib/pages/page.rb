module PageModule
  class Page
    include Components::WaitTime
    include RSpec::Matchers
    DEFAULT_WAITING_TIME = 90

    def self.last_page
      @last_page
    end

    def self.last_page=(last_page)
      @last_page=last_page
    end

    def initialize(check_elements = true)
      Page.last_page = self
    end

    #check current page title
    def check_title(title)
      Components::Browser.is_current_page_title_equals?(title)
    end

    def logout
      Components::Link.new(PageModule::Versions.header_logout).click
      Page.log_out_user
    end

    def navigate_to_dashboard
      Components::Link.new(PageModule::Versions.header_home_link).click
    end

    def self.current_role
      SessionManager.instance.current_role
    end

    def self.set_current_role(current_role)
      SessionManager.instance.current_role = current_role
    end

    def self.user_logged_in?
      SessionManager.instance.user_logged_in
    end

    def self.log_in_user
      SessionManager.instance.user_logged_in = true
    end

    def self.log_out_user
      SessionManager.instance.user_logged_in = false
      SessionManager.instance.current_role = nil
    end

    def self.logged_user_name
      Components::PlainText.new(PageModule::Versions.header_user_name).value.match(/(Hi, )?(.*)/i).captures.last.strip
    end

    def present? element
      begin
        find(element)
        true
      rescue
        false
      end
    end

    def verify_xpath_present?(xpath)
      expect(page).to have_xpath(xpath)
    end

    def get_text_as_array(xpath_for_elements)
      arr = []
      page.all(:xpath, xpath_for_elements).each do |element|
        arr << element.text
      end
      arr
    end

    def get_value_as_array(xpath_for_elements)
      arr = []
      page.all(:xpath, xpath_for_elements).each do |element|
        arr << element.value
      end
      arr
    end

    def hasnt_xpath?(xpath)
      using_wait_time 3 do
        expect(page.driver).to_not have_xpath xpath
      end
    end

    def has_xpath?(xpath)
      expect(page.driver).to have_xpath xpath
    end

    def time_over?(start_time, default_waiting_time)
      Time.now - start_time > default_waiting_time
    end

    def wait_until(options = {}, &block)
      start_time = Time.now
      while true
        condition = yield
        if condition and (not time_over?(start_time, options[:timeout] || DEFAULT_WAITING_TIME))
          return condition
        elsif time_over?(start_time, options[:timeout] || DEFAULT_WAITING_TIME)
          raise "Could not match condition after #{DEFAULT_WAITING_TIME} seconds"
        end
      end
    end

    def check_layout
      @page.check_layout
    end

    def switch_to_tab(tab_name)
      @page.switch_to_tab tab_name
    end

  end
end
