module PageModule
  class RfqSplitPage < Page

    def initialize(check_elements = true)
      super(check_elements)
    end

    def click_link(master_item_id)
      page.all("//a[contains(string(),'#{master_item_id},')]").first.click
    end
  end
end