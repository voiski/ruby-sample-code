module PageModule
  class RfqBillOfMaterialPage < RfqTab

    button 'Save'

    button 'More Actions...' do
      only_role :gp do
        button 'Copy Option'
      end
      button 'Refresh'
      only_role :admin do
        button 'Delete'
      end
    end

    append PageModule::RfqInformationPanel

    sections_panel 'Bill of Material' do
      readonly = :readonly if roles? :ct
      table_section 'Detail' do
        has_checkbox :freezed
        only_role :admin do
          column 'Component Id', :text, :freezed
        end
        column 'Status', :select, :freezed, readonly, :options => [ "ACTIVE", "DROPPED" ]
        column 'Component Type', :select, :freezed, :options => [ "CUT AND MAKE", "DENIM", "DIRECT ENTRY FULL PACKAGE", "FABRIC", "FOOTBED", "GRAPHIC", "HISTORICAL DELTA",
                                                                  "LABEL", "LINING", "MISCELLANEOUS", "OUTSOLE", "PACKAGING", "TRIM", "UPPER", "WASH",  ]
        column 'Component Sub-Type', :lookup, :freezed
        column 'Fabric Lookup', :lookup, :freezed
        column 'RD#', :text, :freezed, readonly
        column 'Primary', :select, :freezed, :options => ['Yes', 'No']
        column 'Description', :text
        column 'Quality Details', :text
        only_role :admin do
          column 'Fabric Mill ID', :text
        end
        column 'Fabric Mill', :text, readonly
        column 'Gauge', :select, :options => [ "HAND KNIT", "1.5GG", "3GG", "5GG", "7GG", "9GG", "12GG", "14GG" ]
        column 'Stitch', :text
        column 'Ends', :select, :options => [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ]
        column 'UM', :lookup
        column 'Fabric FOB', :text
        column 'Fabric CIF', :text
        column 'Wastage %', :text
        column 'Pounds per Dozen', :text
        column 'YY', :text
        column 'Finance %', :text
        column 'Unit Cost', :text
        column 'Qty', :text
        column 'Extended Cost', :text
        column 'Cost Alternative', :select, :options => [ "ALTERNATIVE ACCEPTED", "ALTERNATIVE PROPOSED", "ALTERNATIVE REJECTED" ]
        column 'Comments', :text
      end
    end

  end
end
