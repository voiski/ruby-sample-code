module PageModule
  class RfqTab < Page
    include Components::TS::PageDesign

    tab 'Overview'
    tab 'Details', PageModule::RFQDetailsPage
    tab 'Attachments', only: :admin
    tab 'Notes', only: :admin
    tab 'Bill Of Material', only: :admin
    tab 'Customer Choices', PageModule::RFQCustomerChoicePage
    tab 'Change Tracking'

    title 'Request For Quote'

    def get_rfq_number_from_title
      Components::PlainText.new("//table[@id='_contentviewerHeaderBar_divWorkArea']//label[@class='clsTextLabelNormal']").get_text
    end

  end
end
