module PageModule
  class RFQDetailsPage < RfqTab

    button 'Actions...' do


      only_role :admin do
        button 'Copy Option'
        button 'Refresh'
        button 'Delete Option'
        button 'client Reject'
        button 'Promote to GP'
        button 'Rebid (Detailed)'
        button 'Rebid (Summary)'
        button 'Submit (Detailed)'
        button 'Submit (Summary)'
        button 'Confirm Option'
        button 'Like Option'
        button 'Mark Primary'
        button 'Non-Confirm Option'
        button 'Re-request to CT'
        button 'Costing Re-calc'
        button 'Vendor Reject'
      end

      only_role :ct do
        button 'Copy Option'
        button 'Refresh'
        button 'Delete Option'
        button 'Client Reject'
        button 'Promote to GP'
        button 'Rebid (Detailed)'
        button 'Rebid (Summary)'
        button 'Submit (Detailed)'
        button 'Submit (Summary)'
      end

      only_role :tp do
        button 'Confirm Option'
        button 'Copy Option'
        button 'client Reject'
        button 'Like Option'
        button 'Mark Primary'
        button 'Non-Confirm Option'
        button 'Rebid (Detailed)'
        button 'Rebid (Summary)'
        button 'Submit (Detailed)'
        button 'Submit (Summary)'
        button 'Re-request to CT'
        button 'Refresh'
        button 'Delete Option'
      end

      only_role :gp do
        button 'Confirm Option'
        button 'Copy Option'
        button 'Like Option'
        button 'Mark Primary'
        button 'Non-Confirm Option'
        button 'Re-request to CT'
        button 'Refresh'
        button 'Delete Option'
      end
    end
    button 'Save'
    button 'Go To...' do
     except_role :admin, :ct, :gp, :tp do
       button 'Change Tracking'
     end
      button 'Global Cost Management'
      if role? :admin, :ct, :gp, :tp
        button 'Option Overview'
      else
        button 'Offer'
        button_alias 'Option Overview', 'Offer'
      end
      button 'Sell Channels'
    end

    table_panel 'Cost Options' do
      has_checkbox :freezed
      sandwich_menu :freezed

      only_role :admin  do
          column 'PLM', :select, :r
          column 'PLM  Modified', :calendar, :freezed
          column 'Rollup', :plain_text, :freezed
          column 'Option No', :link, :readonly, :freezed
          column 'Option Status', :select, :freezed, :options => ['CONFIRM', 'EMAILED', 'client REJECT', 'NEW', 'NON-CONFIRM', 'OFFERED', 'PROMOTE TO GP',
                                                                'RE-REQUEST TO CT', 'VENDOR REBID', 'VENDOR REJECT', 'WORK']
          column 'Option Description', :text
          column 'Liked Option?', :select, :options => ['YES']
          column 'Like Option Date', :calendar
          column 'Auto Re-Like?', :select, :options => ['YES']
          column 'First Cost', :plain_text
          column 'Landed  Factor', :plain_text
          column 'ELC', :plain_text, :smart_tag
          column 'Cur', :lookup
          column 'Duty %', :text, :readonly
          column 'Vendor Name', :lookup, :smart_tag
          column 'Agent Name', :lookup
          column 'CT', :select
          column 'Destination Country', :lookup
          column 'Country  of Origin', :lookup
          column 'Transfer Point', :lookup, :smart_tag
          column 'Ship Mode', :select, :options => ['Air', 'Ocean', 'Truck']
          column 'Terms of Sale', :lookup
          column 'Freight Paid By', :select, :options => ['PURCHASER', 'PURCHASER/ VENDOR', 'VENDOR', 'VENDOR PAY 50%']
          column 'Pack Factor', :text
          column 'Carton Type', :select
          column 'Length', :text
          column 'UM', :lookup, :readonly
          column 'Width', :text, :readonly
          column 'UM', :lookup, :readonly
          column 'Height', :text, :readonly
          column 'UM', :lookup, :readonly
          column 'Carton Weight', :text
          column 'UM', :lookup
          column 'Carton  Measurement', :text
          column 'UM', :lookup
          column 'CT Comments', :text
          column 'Vendor Comments', :text
          column 'Original  Option No', :plain_text
          column 'GP Initiated? ', :plain_text
          column 'Global Style  Target Qty', :text
          column 'Initial Retail  Price', :plain_text
          column 'IMU%', :plain_text
          column 'Request Date', :plain_text
          column 'Vendor Response Due Date', :calendar
          column 'Actual Response Date', :plain_text
          column 'Costs Valid Till', :calendar
          column 'Confirmed Date', :plain_text
          column 'Estimated ELC', :text
      end
      only_role :ct do
        column 'Option No', :link, :readonly, :freezed
        column 'PLM  Modified', :calendar, :freezed, :readonly
        column 'Like Option Date', :calendar, :readonly
        column 'Auto Re-Like?', :select, :readonly, :options => ['YES']
        column 'Option Status', :select, :readonly, :freezed, :options => ['CONFIRM', 'EMAILED', 'client REJECT', 'NEW', 'NON-CONFIRM', 'OFFERED', 'PROMOTE TO GP',
                                                                'RE-REQUEST TO CT', 'VENDOR REBID', 'VENDOR REJECT', 'WORK']
        column 'Option Description', :text
        column 'CT', :select
        column 'Agent Name', :lookup
        column 'Vendor Name', :lookup, :smart_tag
        column 'Country  of Origin', :lookup
        column 'Duty %', :text, :readonly
        column 'First Cost', :plain_text
        column 'Landed  Factor', :plain_text
        column 'ELC', :plain_text, :smart_tag
        column 'CT Comments', :text
        column 'Vendor Comments', :text, :readonly
        column 'Transfer Point', :lookup, :smart_tag
        column 'Ship Mode', :select, :options => ['Air', 'Ocean', 'Truck']
        column 'Terms of Sale', :lookup, :readonly
        column 'Freight Paid By', :select, :options => ['PURCHASER', 'PURCHASER/ VENDOR', 'VENDOR', 'VENDOR PAY 50%']
        column 'Pack Factor', :text
        column 'Original  Option No', :plain_text
        column 'GP Initiated? ', :plain_text
        column 'Request Date', :plain_text
        column 'Vendor Response Due Date', :calendar
        column 'Actual Response Date', :plain_text
        column 'Costs Valid Till', :calendar
        column 'Confirmed Date', :plain_text
      end
      only_role :gp do
        column 'Option No', :link, :readonly, :freezed
        column 'PLM  Modified', :calendar, :freezed, :readonly
        column 'Like Option Date', :calendar, :readonly, :freezed
        column 'Auto Re-Like?', :select, :readonly, :freezed, :options => ['YES']
        column 'Rollup', :plain_text, :freezed
        column 'Option Status', :select, :readonly, :freezed, :options => ['CONFIRM', 'EMAILED', 'client REJECT', 'NEW', 'NON-CONFIRM', 'OFFERED', 'PROMOTE TO GP',
                                                                           'RE-REQUEST TO CT', 'VENDOR REBID', 'VENDOR REJECT', 'WORK']
        column 'Option Description', :text
        column 'First Cost', :plain_text
        column 'Landed  Factor', :plain_text
        column 'ELC', :plain_text, :smart_tag
        column 'CT', :select, :readonly
        column 'Agent Name', :lookup, :readonly
        column 'Vendor Name', :lookup, :readonly
        column 'Country  of Origin', :lookup, :readonly
        column 'Duty %', :text, :readonly
        column 'Transfer Point', :lookup, :smart_tag, :readonly
        column 'Ship Mode', :select, :readonly, :options => ['Air', 'Ocean', 'Truck']
        column 'Terms of Sale', :lookup, :readonly
        column 'Freight Paid By', :select, :readonly, :options => ['PURCHASER', 'PURCHASER/ VENDOR', 'VENDOR', 'VENDOR PAY 50%']
        column 'Pack Factor', :text, :readonly
        column 'GP Initiated? ', :plain_text
        column 'Original  Option No', :plain_text
        column 'CT Comments', :text, :readonly
        column 'Vendor Comments', :text, :readonly
        column 'Costs Valid Till', :calendar, :readonly
        column 'Confirmed Date', :plain_text
      end

      only_role :tp do
        column 'Option No', :link, :readonly, :freezed
        column 'PLM  Modified', :calendar, :freezed, :readonly
        column 'Like Option Date', :calendar, :freezed, :readonly
        column 'Auto Re-Like?', :select, :readonly, :options => ['YES']
        column 'Rollup', :plain_text, :freezed
        column 'Option Status', :select, :readonly, :freezed, :options => ['CONFIRM', 'EMAILED', 'client REJECT', 'NEW', 'NON-CONFIRM', 'OFFERED', 'PROMOTE TO GP',
                                                                           'RE-REQUEST TO CT', 'VENDOR REBID', 'VENDOR REJECT', 'WORK']
        column 'Option Description', :text
        column 'CT', :select
        column 'Agent Name', :lookup
        column 'Vendor Name', :lookup, :smart_tag
        column 'Country  of Origin', :lookup
        column 'Duty %', :text, :readonly
        column 'First Cost', :plain_text
        column 'Landed  Factor', :plain_text
        column 'ELC', :plain_text, :smart_tag
        column 'CT Comments', :text
        column 'Vendor Comments', :text, :readonly
        column 'Transfer Point', :lookup, :smart_tag
        column 'Ship Mode', :select, :options => ['Air', 'Ocean', 'Truck']
        column 'Terms of Sale', :lookup, :readonly
        column 'Freight Paid By', :select, :options => ['PURCHASER', 'PURCHASER/ VENDOR', 'VENDOR', 'VENDOR PAY 50%']
        column 'Pack Factor', :text
        column 'Original  Option No', :plain_text
        column 'GP Initiated? ', :plain_text
        column 'Request Date', :plain_text
        column 'Vendor Response Due Date', :calendar
        column 'Actual Response Date', :plain_text
        column 'Costs Valid Till', :calendar
        column 'Confirmed Date', :plain_text
      end
    end

  end
end


