module PageModule
  class RFQOverviewPage < RfqTab

    button 'Actions...' do
      button 'Copy RFQ'
      only_role :admin, :gp, :tp do
        button 'Ready for CT'
        button 'RFQ Complete'
        button 'RFQ Non-Complete'
      end
      button 'Delete RFQ'
    end

    button 'Save'

    button 'Go To...' do
      button 'Global Cost Management'
      button 'Sell Channels'
    end

    sections_panel 'Sketch & Tech Pack', :collapsed do
      section 'Primary Sketch' do
        field :no_label, :attachment
      end
      only_role :ct, :gp, :tp do
        section 'Change Notes' do
          field :no_label, :textarea
        end
      end
      section 'Tech Pack' do
        field :no_label, :attachment
      end
    end

    sections_panel 'Request for Quote Overview' do
      section 'Style Information' do
        readonly = :readonly if roles? :ct, :gp

        field 'BOM #', :text, :above, :readonly
        field 'Style Description', :text, :above, readonly
        field 'Master Style #', :text, :above, readonly
        field 'Master Style Description', :text, :above, readonly
        only_role :admin do
          field 'Adoptable Style?', :select, :above, options: ['No', 'Yes']
          field 'Merch Type', :select, :above, options: ['BASIC', 'FASHION', 'SEASONAL BASIC']
        end
        except_role :admin do
          field 'Season', :select, :above, :readonly, options: ['FALL', 'HOLIDAY', 'SPRING', 'SUMMER']
          field 'Year', :lookup, :above, :readonly
          field 'Brand', :text, :above, :readonly
          field 'Division', :text, :above, :readonly
          field 'Dept', :text, :above, :readonly
          field 'Class', :text, :above, :readonly
          field 'Sub Class', :text, :above, :readonly
          field 'Fit Type', :select, :above, options: ['ASIA', 'HUSKY', 'LONG', 'MATERNITY', 'PETITE', 'PLUS', 'REGULAR', 'SLIM']
        end
      end
      section 'Production Information' do
        readonly = :readonly if roles? :ct, :gp
        readonlyTp = :readonly if roles? :tp
        readonlyCt = :readonly if roles? :ct
        readonlyCtTp = readonlyTp || readonlyCt

        except_role :ct do
          field 'Season', :select, :above, options: ['FALL', 'HOLIDAY', 'SPRING', 'SUMMER']
          field 'Year', :lookup, :above
        end
        field 'In DC Date', :calendar, :above
        field 'Flow', :text, :above, readonly
        except_role :admin do
          field 'Merch Type', :select, :above, readonlyCt, options: ['BASIC', 'FASHION', 'SEASONAL BASIC']
        end
        field 'Global Style Target Qty', :text, :above, readonlyCt
        field 'Buy Size', :select, :above, readonlyCt, options: ['client-EXCLUSIVE', 'client-FLAG', 'client-GLOBAL CORE', 'client-KEY ITEM', 'client-LEVEL', 'ON-BIG STARS', 'ON-COWS', 'ON-QUESTION MARKS', 'ON-SMALL STARS']
        field 'CC Confirmed Qty', :text, :above, :readonly
        field 'CC Units Source', :select, :above, :readonly, options: ['ASSORTMENT', 'CC UPLOADED']
        except_role :admin do
          field 'Adoptable Style?', :select, :above, options: ['No', 'Yes']
        end
        only_role :gp, :tp do
          field 'Production Comp', :text, :above
        end
        only_role :gp do
          field 'Good/Better/Best', :above, :select, options: ['BEST', 'BETTER', 'GOOD']
        end
        field 'Target AUC', :text, :above, readonlyCt
        only_role :admin do
          field 'Brand AUC', :text, :above, :readonly
          field 'Division AUC', :text, :above, :readonly
          field 'Department AUC', :text, :above, :readonly
          field 'Class AUC', :text, :above, :readonly
        end
      end
      section 'Category & Duty Classification' do
        readonly = :readonly unless roles? :admin
        except_role :gp do
          field 'Brand', :text, :above, :readonly if role? :admin
          field 'Division', :text, :above, :readonly if role? :admin
          field 'Dept', :text, :above, :readonly if role? :admin
          field 'Class', :text, :above, :readonly if role? :admin
          field 'Sub Class', :text, :above, :readonly if role? :admin
          field 'Fit Type', :select, :above, options: ['ASIA', 'HUSKY', 'LONG', 'MATERNITY', 'PETITE', 'PLUS', 'REGULAR', 'SLIM'] if role? :admin
          field 'Category Hierarchy', :lookup, :above
        end

        field 'Category', :text, :above, :readonly
        field 'Sub Category', :text, :above, :readonly
        field 'Item Type', :text, :above, :readonly
        except_role :tp do
          field 'Age', :select, :above, readonly, options: ['ADULT', 'INFANT', 'KIDS', 'N/A', 'NEWBORN', 'TODDLER']
          field 'Gender', :select, :above, readonly, options: ['FEMALE', 'MALE', 'N/A', 'UNISEX']
        end
        only_role :tp do
          field 'Age', :select, :above, options: ['ADULT', 'INFANT', 'KIDS', 'N/A', 'NEWBORN', 'TODDLER']
          field 'Gender', :select, :above, options: ['FEMALE', 'MALE', 'N/A', 'UNISEX']
        end
        only_role :ct, :tp, :admin do
          field 'Duty Class. Code', :lookup, :above
          field 'Import Category', :text, :above, :readonly
          field 'Product Type', :text, :above, :readonly
          field 'Chief Weight Fiber', :text, :above, :readonly
        end
        only_role :ct, :tp do
          field 'LFL Style?', :above, :select, options: ['No', 'Yes']
          field 'LFL Style #', :above, :text
        end
      end
      section 'RFQ Information' do
        readonlyCtTp = :readonly if roles? :ct, :tp
        readonlyCt = :readonly if roles? :ct
        field 'Style RFQ No', :text, :above, :readonly
        field 'RFQ Status', :select, :above, readonlyCtTp, options: ['COMPLETED', 'CONFIRM', 'DROPPED', 'NEW', 'READY FOR CT']
        field 'RFQ Split Reason', :textarea, :above
        field 'Global Production', :select, :above, :readonly
        field 'Category Team', :select, :above, :readonly
        field 'CT Response Due Date', :calendar, :above, readonlyCt
      end
      only_role :admin do
        table_section 'Customer Choices' do
          column 'On RFQ?', :select, options: ['ACTIVE', 'DROPPED']
          column 'CC Status', :select
          column 'BOM CC #', :lookup
          column 'BOM CC Description', :text
          column 'CC Qty', :text, :readonly
          column 'LFL BOM CC #', :text
        end
      end
    end

    sections_panel 'Comments & Change Info', :collapsed do
      self.name = 'Comments' unless role? :admin

      section 'GP/CT RFQ Comments' do
        field :no_label, :textarea
      end

      only_role :admin do
        section 'Change Notes' do
          field :no_label, :textarea
        end
        only_role :admin do
          section 'Dates' do
            field 'Date Created', :text, :above, :readonly
            field 'Year', :text, :above, :readonly
            # FIXME lucas.meirelles: Add this field back when we decide which one should hold the SendTo processing flag
            # field 'RFQ Last Updated', :text, :above, :readonly
          end
        end
      end
    end

    sections_panel 'Miscellaneous', :collapsed, :admin do
      section 'Bundles' do
        field 'Bundle No', :lookup
        field 'Bundle Desc', :textarea, :readonly
      end
      section 'Comp Style Info' do
        field 'LFL Style #', :text
        field 'LFL Style?', :select, options: ['No', 'Yes']
        field 'Production Comp', :text
        field 'Season', :select, options: ['FALL', 'HOLIDAY', 'SPRING', 'SUMMER']
        field 'Year', :lookup
        field 'Merch Comp', :text
        field 'Season', :select, options: ['FALL', 'HOLIDAY', 'SPRING', 'SUMMER']
        field 'Year', :lookup
        field 'Good/Better/Best', :select, options: ['BEST', 'BETTER', 'GOOD']
      end
      section 'Market Info' do
        field 'Market', :select
        field 'Channel', :select
        field 'Destination\n\ Country', :select
        field 'Destination Port', :text, :readonly
      end
    end

    sections_panel 'Customer Choices', :collapsed, :ct, :vendor do
      table_section 'Customer Choices' do
        has_checkbox
        column 'On RFQ?', :select
        column 'CC Status', :select, :readonly
        column 'BOM CC #', :text, :readonly
        column 'BOM CC Description', :text, :readonly
        column 'CC Qty', :text, :readonly
        only_role :ct, :tp do
          column 'LFL BOM CC #', :text
        end
      end
    end

    sections_panel 'Cost Options' do
      table_section 'Options' do
        readonly = :readonly if roles? :ct, :gp, :tp
        column 'Option No', :link, :readonly
        only_role :gp do
          column 'PLM  Modified', :text, :readonly
          column 'Like Option Date', :text, :readonly
          column 'Auto Re-Like?', :text, :readonly
          column 'Rollup', :text, :readonly
        end
        only_role :ct do
          column 'Like Option Date', :text, :readonly
          column 'Auto Re-Like?', :select, :readonly
        end
        column 'Option Status', :select, readonly
        column 'Option Description', :text
        except_role :gp do
          column 'Vendor Name', :lookup
          column 'Agent Name', :lookup
          column 'CT', :select
          column 'CT Comments', :text
        end
        column 'First Cost', :text, :readonly
        column 'Landed  Factor', :text, :readonly
        column 'ELC', :text, :readonly
        only_role :gp do
          column 'Vendor Name', :lookup, :readonly
        end
        only_role :gp do
          column 'CT', :select, :readonly
        end
        except_role :gp do
          column 'Duty %', :text, :readonly
        end
      end
    end

  end
end
