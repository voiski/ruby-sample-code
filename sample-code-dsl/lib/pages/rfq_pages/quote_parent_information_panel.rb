module PageModule
  module RfqInformationPanel

    def self.compose
      lambda do |klass|
        klass.panel 'RFQ Information' do
          field 'Style RFQ No', :plain_text
          except_role :admin do
            if roles? :ct or roles? :gp or role? :tp
              field 'Global Style Target Qty', :plain_text
            else
              field 'Global Style\nTarget Qty', :plain_text
            end
          end
          only_role :admin do
            field 'BOM #', :text, :readonly
            field 'Style Description', :text, :readonly
            field 'Season', :text, :readonly
            field 'Year', :text, :readonly
            field 'RFQ Split Reason', :textarea, :readonly
          end
        end
      end
    end
  end
end
