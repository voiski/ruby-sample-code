module PageModule
  class RFQCustomerChoicePage < RfqTab

    button 'Save'

    append PageModule::RfqInformationPanel

    sections_panel 'Customer Choices' do
      table_section 'Customer Choices' do
        readonly = :readonly if roles? :ct
        readonlyCcStatus = :readonly if not roles? :admin
        readonlyLFLBomCC = :readonly if roles? :gp

        has_checkbox
        column 'On RFQ?', :select, :options => ['Yes', 'No']
        column 'CC Status', :select, readonlyCcStatus, :options => ['ACTIVE', 'DROPPED']
        column 'BOM CC #', :text, readonly
        column 'BOM CC Description', :text, readonly
        column 'CC Qty', :text, :readonly
        except_role :vendor do
          column 'LFL BOM CC #', :text, readonlyLFLBomCC
        end
      end
    end

  end
end
