module PageModule
  class RfqAttachmentsPage < RfqTab

    button 'More Actions' do
      button 'Delete'
      button 'Refresh'
    end

    button 'Save'

    append PageModule::RfqInformationPanel

    table_panel 'Option Attachments' do
      has_checkbox
      column 'File Name', :text, :readonly
      column 'Thumbnail', :attachment
      column 'Description', :textarea
      column 'Modified By', :text, :readonly
      column 'Date Modified', :text, :readonly
    end

  end
end
