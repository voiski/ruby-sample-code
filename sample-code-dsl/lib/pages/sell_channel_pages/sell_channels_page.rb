module PageModule
  class SellChannelsPage < Page
    include Components::TS::PageDesign

    title 'Sell Channels'

    button 'Saved Searches'
    button 'Save', except: :gp

    roles :gp, :ct, :tp, :admin

    advanced_search_panel 'Sell Channels', :collapsed do
      section do
        field 'Season', :select,  :above, options: ['FALL','HOLIDAY','SPRING','SUMMER']
        field 'Year', :text, :above
        field 'BOM #', :text, :above
        field 'Style Description', :text, :above
        field 'Style RFQ No', :text, :above
      end
    end

    table_panel 'Search List' do

      readonly = :readonly if roles? :gp or roles? :admin

      has_checkbox
      column 'BOM #', :attachment
      column 'Style Description', :label_link do
        item :label
        item 'Style RFQ No', :label
        item :label_link
      end
      column 'Season', :text, :readonly
      column 'Year', :text, :readonly
      column 'RFQ Status'
      only_role :admin do
        column 'RFQ Split Reason', :textarea
      end
      except_role :admin do
        column 'RFQ Split Reason', :text, readonly
      end
      # FIXME: Check this, the COST-569 tells that it need to be a link, not just a blue label
      column 'Confirmed Option'
      column 'Manage Sell Channels', :label_link do
        item 'Manage Sell Channels', :label_link
        item 'View CC Quantities', :label_link
        item :expand
      end
      # TODO: it should have a expand also, check how to have some information here
      column 'CC Confirmed Qty', :text, :readonly
    end

    modal 'View CC Quantities' do
      button 'Showing All Channels', :disabled
      button 'Hide Channels'

      # TODO: map the table
    end

    modal 'Manage Sell Channels' do
      button 'Add Deliveries'
      button 'Showing All Channels', :disabled
      button 'Hide Channels'

      has_checkbox
      column :expand, :expand
      column 'Sell Channel', :text, :readonly
      column 'Status', :select, options: ['ACTIVE','INACTIVE']
    end

  end
end
