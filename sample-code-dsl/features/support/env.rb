ENV["PATH"] += ";" + File.dirname(File.join(File.dirname(__FILE__), '..', '..', '..', '..', '..', 'build', 'tools', 'Oracle', 'oracle_drivers.zip'))

require 'rubygems'
require 'require_all'
require 'capybara'
require 'capybara/dsl'
require 'selenium-webdriver'
require 'yaml'
require 'savon'
require 'rspec/expectations'
require 'erb'
require 'chronic'
require 'rest_client'
require 'mongo'
require 'xmlsimple'
require 'bson'
require 'tiny_tds'
require 'capybara/poltergeist'
require 'cucumber_statistics/autoload'
require 'rspec'
require 'logger'
require 'colorize'
require 'parallel'

Dir["#{File.dirname(__FILE__)}../../lib/extension/*.rb"].each {|file| require file }

World(RSpec::Matchers)
include Capybara::DSL

# Log handler
LOG_PATH = ENV['LOG_PATH'] || 'logs'
@logger = Logger.new(STDOUT)
if ENV['DISABLE_COLORIZATION']
  String.disable_colorization = true
  Cucumber::Term::ANSIColor.coloring = false
end

# Defines the environment
ENVIRONMENT = YAML.load ERB.new(File.read(File.join(File.dirname(__FILE__), '..', '..', 'config', 'environments.yml'))).result
CURRENT_ENVIRONMENT = (ENV["CURRENT_ENV"] || 'dev').strip
@logger.info("Running features against #{CURRENT_ENVIRONMENT} environment".green)

# Defines the driver
CURRENT_DRIVER = (ENV["CURRENT_DRIVER"] || 'selenium')
@logger.info("Selected driver is #{CURRENT_DRIVER}".green)

# TS version
TS_VERSION = ARGV.select{|a| a.start_with? 'ts='}.map{|a| a[3..-1].upcase}.first
@logger.info("Running TS Version #{TS_VERSION}".green) if TS_VERSION

# TS java test services
TS_JAVA_SERVICE = File.expand_path("../../../../../tradestone-test-services/build/libs", "#{File.dirname(__FILE__)}")

# Tags to play
tags = ARGV.select { |element| /\@[\w-]+/ === element }.map{|t| t.split.last.slice(/[\w-]+/)}
# The layout check doesn't need to worry about a old issue when you keep the
# session, so to speed up the migration we should disable the logout hook.
ENABLE_LOGOUT_HOOK = 'layout_check' != tags.first
@logger.info("Running with tags #{tags}".green) unless tags.empty?

# User profiles
user_profile_file =  if CURRENT_ENVIRONMENT == 'prod'
                       'users_prod.yml'
                     elsif tags.size == 1 &&
                           tags.first =~ /(layout_check|regression|prod_regression)\d*/ &&
                           ['qa','dev'].include?(CURRENT_ENVIRONMENT)
                       tag_base = tags.first.gsub(/\d/,'')
                       "#{tag_base}_users.yml"
                     else
                       'users.yml'
                     end
USER = YAML.load ERB.new(File.read(File.join(File.dirname(__FILE__), '..', '..', 'config', user_profile_file))).result
# Will concat the regression thread number to the login username.
# This is to avoid impacts and also to provide a parallel to the tests.
# regression2,...,regression10,...

number = tags.find{|t|t =~ /regression([2-9]|\d{2,})/}

if number
  number = number.match(/regression(\d+)/)[1]
  USER.reject{|role,user| role =~ /ct2|vendor|webservice/ }
      .each  {|role,user| user['username'] << number }

  unless CURRENT_ENVIRONMENT == 'prod'
    USER['vendor']['username'] = "RAUTOMATION VEN#{number}"
  end
end

# Run with your user to avoid user concurrence.
if ENV['TS_CORE_USER']
  USER.reject{|role,user| role =~ /webservice/}.each do |role,user|
    user['username'] = "#{ENV['TS_CORE_USER']} #{role}"
  end
end

AUTOMATION_ADMIN  = USER['admin' ]['username'].upcase
AUTOMATION_GP     = USER['gp'    ]['username'].upcase
AUTOMATION_CT     = USER['ct'    ]['username'].upcase
AUTOMATION_CT2    = USER['ct2'   ]['username'].upcase
AUTOMATION_TP     = USER['tp'    ]['username'].upcase
AUTOMATION_VENDOR = USER['vendor']['username'].upcase

@logger.info "## Users:"
USER.each{|role,user| @logger.info "# #{role.light_blue}: #{user['username'].upcase.blue}" }

# Download path for testing
require_rel '../../lib'
DOWNLOAD_PATH = ENV["COSTING_DOWNLOAD_PATH"] || ((OS.windows?) ? "c:\\downloads" : File.join(Dir.home, 'costing-tradestone-test_downloads'))

# Load the color codes
if CURRENT_ENVIRONMENT != "prod" && ENV['CURRENT_ENV'] && ENV['SKIP_COLOR_LOAD'].nil?
  Helpers::ColorCodes::COLORS.each do |key, value|
    TradeStoneWebService.create_colorways_codes(key, value)
  end
end

# -----------------------
# -- Capybara Settings --
# -----------------------
# -----------------------
ENV['no_proxy'] = "127.0.0.1"
require_rel 'drivers/driver_register'
# SELENIUM_SERVER is the IP address or hostname of the system running Selenium
# Server, this is used to determine where to connect to when using one of the
# selenium_remote_* drivers
SELENIUM_SERVER = ENV['SELENIUM_SERVER']||'localhost'
DriverRegister.register CURRENT_DRIVER.to_sym

RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end

if ['dev','local'].include?(CURRENT_ENVIRONMENT) || ENV['DEBUGGER_MODE']=='true'
  # --------------------------
  # -- Pry debug Activation --
  # --------------------------
  # --------------------------
  # Create a breakpoint putting binding.pry in the line.
  # Use next|continue|reload-code, see more:
  #
  require 'pry'
end
