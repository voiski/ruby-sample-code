def to_file_name(name)
  name.gsub(' ', '_').gsub(/[^0-9A-Za-z_]/, '')[0..128]
end

def create_screenshot_directory
  FileUtils.mkpath "#{LOG_PATH}/screenshots" unless File.directory? "#{LOG_PATH}/screenshots"
end

def capture_html(name)
  create_screenshot_directory
  File.open("#{LOG_PATH}/screenshots/#{name}", 'wb') do |file|
    file.write(page.html)
  end
rescue Errno::ECONNREFUSED
  @logger.error 'Impossible to capture the HTML, the browser was closed.'.red
end

def capture_image(path=nil)
  if path
    page.save_screenshot path
  else
    encoded_img = page.driver.browser.screenshot_as(:base64)
    embed("#{encoded_img}",'image/png')
  end
rescue Errno::ECONNREFUSED
  @logger.error 'Impossible to save the page screenshot, the browser was closed.'.red
end

def renew_vendor_key
  renewal_page = PageModule::RenewalPage.new
  renewal_page.renew_vendor_key
  renewal_page.navigate_back_to_ts
end

# --------------------------
# -- HeadLess  Activation --
# --------------------------
# --------------------------
# To enable it set the env 'HEADLESS_MODE' or pass the parameter 'headless'.
# see http://makandracards.com/makandra/1391-how-to-hide-your-selenium-browser-window-with-headless
#
# NOTICE: It will only work for unix system due the dependency with the xvfb.
#
headless_mode = ARGV.find{|c|c =~ /headless=.+/}||ENV['HEADLESS_MODE']||'false'

if (OS.mac? && headless_mode != 'false')
  @logger.warn('WARNING: Headless mode is not supported on MacOS'.light_red.on_light_yellow)
  headless_mode = 'false'
elsif (OS.unix? && headless_mode != 'false')
  @logger.info("Headless mode is activated (#{headless_mode})".green)
  require 'headless'

  if headless_mode.include? 'screen:'
    headless_resolution = headless_mode.match(/screen:(\d+[xX]\d+)/).captures.first.downcase
    puts 'Using screen resolution', headless_resolution
  else
    headless_resolution = '1920x1080'
  end

  headless = Headless.new dimensions: "#{headless_resolution}x24", reuse: true, destroy_at_exit: false, display: 100
  headless.start
  @logger.info(ENV['DISPLAY'].to_s)

  Before("~@no-headless") do
    page.driver.browser.manage.window.resize_to *headless_resolution.split('x')
  end

  Before do |scenario|
    @scenario = {
      step: 1,
      name: scenario.name.gsub(/\W/,'_'),
      path: "#{LOG_PATH}/headless/#{scenario.feature.name.gsub(/\W/, '_')}",
      time: Time.now.strftime('%Y%m%d_%H%M%S')
    }
    @scenario[:path_file] = "#{@scenario[:path]}/#{@scenario[:time]}_#{@scenario[:name]}.%{extension}"
    FileUtils.rm Dir.glob("#{@scenario[:path]}/*#{@scenario[:name]}*.*")
    FileUtils.mkdir_p @scenario[:path]
  end

  After do |scenario|
    if scenario.failed?
      if PageModule::Page.current_role && !@skip_screenshot
        capture_image @scenario[:path_file] % { extension: "#{@scenario[:step]}.ERROR.png" }
      end
    else
      FileUtils.rm Dir.glob("#{@scenario[:path]}/*#{@scenario[:name]}*.*")
    end
    #PageModule::Page.set_current_role nil
  end

  # Screenshot in headless mode
  if headless_mode.include? 'screenshot'
    require 'fileutils'

    AfterStep do |s|
      if PageModule::Page.current_role && !@skip_screenshot
        begin
          capture_image @scenario[:path_file] % { extension: "#{@scenario[:step]}.png" }
        rescue
          @skip_screenshot = true
        end
      end
      @scenario[:step] += 1
    end

  end

  # Video record in headless mode
  if headless_mode.include? 'recorder'
    AfterStep do |s|
      if @recording.nil? && PageModule::Page.current_role
        headless.video.start_capture
        @recording=true
      end
    end

    After do |scenario|
      if scenario.failed?
        video_file = @scenario[:path_file] % { extension: 'mov' }
        headless.video.stop_and_save video_file

        puts "Video recorder at:\n#{video_file}"
      else
        headless.video.stop_and_discard
      end
    end
  end
end

Before("@query_layout_check") do
  if USER.find { |_k,credentials| credentials['username_query'] }
    USER.each do |_k,credentials|
      credentials['username']=credentials['username_query'] if credentials['username_query']
    end
  end
end

# It enables the feature where is possible to redo the last step using the saved
# Html. It will not have a session, but is useful for layout checks.
# Simple creates a scenario with the tag @continue and also use the last step.
# Then just: cucumber --tags @continue
Before("@continue") do
  @continue_mode = true
  PageModule::Page.send(:define_singleton_method, :current_role) do
    'admin'
  end
  path_to_last_step = File.absolute_path "#{LOG_PATH}/screenshots/last_step.html"
  visit "file://#{path_to_last_step}"
  sleep 5
  Capybara.default_max_wait_time = 0
end

Before("~@no-browser") do
  @parallel_activated = (headless_mode == 'false')
  Components::Browser.maximize unless CURRENT_DRIVER == 'poltergeist'
end

Before do |scenario|
  unless $not_first_run

    if ENV['RENEW_LINCENSE'] == 'Y'
        puts "Verify if needs to renew license"
        Components::TS.navigate_to_TS()
        login_page = PageModule::LoginPage.new
        login_page.login('vendor')
        if login_page.has_vendor_key_expired
          puts "trying to renew license"
          login_page.go_to_renewal_page
          renew_vendor_key
          login_page.login('vendor')
        end
       login_page.logout
       puts "License renewed"
    end

    puts "Reports generated for last run at #{Time.now}"
  end
  $not_first_run = true

  @created_rfqs = []
end

Before do |scenario|
  $start_time = Time.now
end

Before('@pry') do |scenario|
  @debug_scenario = {current_step:0,after_step:1}
  step_to_jump = scenario.tags.find{|t|t.name =~ /@step-\d+/}
  @debug_scenario[:after_step] = step_to_jump.name[6..-1].to_i if step_to_jump
end

After do |scenario|
  puts "[#{Time.now.inspect}] - #{scenario.name} : #{Time.now - $start_time} secs\n"
  FileUtils.rm_rf(DOWNLOAD_PATH)
end

After("@set_up_data") do |scenario|
  @@data_set_up ||= {}
  @@data_set_up[@created_rfqs.last] = @cost_model
  print @@data_set_up.to_s
end

After('~@no-browser') do |scenario|
  @parallel_activated = false
  if scenario.failed?
    html_name = to_file_name(scenario.name) + ".html"
    capture_html html_name
    capture_image
  end
  capture_html 'last_step.html' unless @continue_mode
end

After('~@no-browser', '~@skip_logout') do |scenario|
  if ENABLE_LOGOUT_HOOK && PageModule::Page.user_logged_in?
    page ||= PageModule::Page.new
    page.logout
  end
end

After('~@debug') do |scenario|
  TradeStoneWebService.delete_all_rfqs(@created_rfqs)
end

After('@pry') do |scenario|
  @debug_scenario = nil
end

AfterStep do |s|
  if @debug_scenario
    @debug_scenario[:current_step]+=1
    binding.pry if @debug_scenario[:current_step] >= @debug_scenario[:after_step]
  end
end

Before('@java_ts_services') do |scenario|
  @java_ts_services_path = TS_JAVA_SERVICE
  if @java_ts_services_pid.nil?
    @java_ts_services_pid = Process.spawn("SPRING_PROFILES_ACTIVE=" + CURRENT_ENVIRONMENT.to_s + " java -jar tradestone-test-services-1.0.0.jar", :chdir=>@java_ts_services_path)
  end
  puts "@java_ts_services started process " + @java_ts_services_pid.to_s + " at " + TS_JAVA_SERVICE + "/tradestone-test-services-1.0.0.jar" + " on ENV: " + CURRENT_ENVIRONMENT.to_s
end

After('@java_ts_services') do |scenario|
  @kill_cmd = "kill " + @java_ts_services_pid.to_s
  system(@kill_cmd)
  puts "@java_ts_services " + @kill_cmd.to_s + " process"
end
