Given /^I log in TS as an? (.*)$/ do |user_type|
  login_block = lambda{ do_loggin(user_type) }

  if @pending_rfq_table
    blocks = [login_block]
    blocks << lambda{ create_rfq(@pending_rfq_table) }
    results = Parallel.map(blocks){ |b| b.call }
    results.each do |result|
      if result[:role]
        PageModule::LoginPage.logged result[:role]
      else
        result.each { |name, value| instance_variable_set("@#{name}", value) }
      end
    end
    @pending_rfq_table = nil
  else
    login_block.call
  end
end

When /^I log out the (?:application|TS)$/ do
  page = PageModule::Page.new()
  page.logout
end

def do_loggin(user_type)
   if CURRENT_ENVIRONMENT != 'prod' && PageModule::Page.current_role === user_type.downcase
     Components::TS.navigate_to_TS 'dash'
   else
    Components::TS.navigate_to_TS
    login = PageModule::LoginPage.new
    login.login(user_type)
   end
  { role: PageModule::Page.current_role }
end
