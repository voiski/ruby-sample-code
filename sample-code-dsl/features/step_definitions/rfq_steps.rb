
When /^I update the RFQ Comments to "(.*?)"$/ do |rfq_comments|
  @rfq_overview_page ||= PageModule::RFQOverviewPage.new
  @rfq_overview_page.comments_and_change_info.expand! if @rfq_overview_page.comments_and_change_info.collapsed?
  @rfq_overview_page.comments_and_change_info.gp_ct_rfq_comments.no_label.value = rfq_comments
end

When /^I update the RFQ CT Response Due Date to "(.*?)"$/ do |rfq_ct_reponse_due_date|
  @rfq_overview_page ||= PageModule::RFQOverviewPage.new
  @rfq_overview_page.request_for_quote_overview.rfq_information.ct_response_due_date.value = Helpers::DateUtils.get_formatted_date(rfq_ct_reponse_due_date)
end

When(/^The copied RFQ has a Style RFQ No different than the original RFQ$/) do
  @rfq_overview_page ||= PageModule::RFQOverviewPage.new
  expect(@rfq_overview_page.request_for_quote_overview.rfq_information.style_rfq_no.value).not_to eq nil
  expect(@rfq_overview_page.request_for_quote_overview.rfq_information.style_rfq_no.value).not_to eq ""
  expect(@rfq_overview_page.request_for_quote_overview.rfq_information.style_rfq_no.value).not_to eq @created_rfqs.first
  @copied_rfq = @rfq_overview_page.request_for_quote_overview.rfq_information.style_rfq_no.value
end
