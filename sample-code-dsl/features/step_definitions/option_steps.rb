Then(/^I can't edit the option description for that option$/) do
  option_response_overview_page = PageModule::OptionResponseOverviewPage.new
  expect(option_response_overview_page.option_overview.option_information.option_description.readonly?).to eq true
end


When(/^I edit option description to "(.*)"$/) do |description|
  option_response_overview_page = PageModule::OptionResponseOverviewPage.new
  expect(option_response_overview_page.option_overview.option_information.option_description.readonly?).to eq false
  option_response_overview_page.option_overview.option_information.option_description.value = description
  option_response_overview_page.save.click
end

And(/^I click on Rebid\(Detailed\) Option to Vendor$/) do
  @rfq_details_page = PageModule::RFQDetailsPage.new
  @rfq_details_page.actions.click.rebid_detailed
end

And(/^I click on Rebid\(Summary\) Option to Vendor$/) do
  @rfq_details_page = PageModule::RFQDetailsPage.new
  @rfq_details_page.actions.click.rebid_summary
end

Then(/^the option Costing Level should be "([^"]*)"$/) do |costing_level|
  option_response_page ||= PageModule::OptionResponseOverviewPage.new
  expect(option_response_page.option_overview.option_information.costing_level.value).to eq costing_level
end
