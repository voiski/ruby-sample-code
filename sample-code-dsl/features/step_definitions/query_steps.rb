Then(/^I select "([^"]*)" season$/) do |season|
  @query_page ||= PageModule::QueryModule::QueryPage.new
  Components::TS::ConfirmAlert.dismiss if Components::TS::ConfirmAlert.present?
  @query_page.search_panel.season.value = season
end

Then(/^I select "([^"]*)" season in the advanced search$/) do |season|
  @advanced_search_page ||= PageModule::AdvancedSearchRfqPage.new
  Components::Alert.dismiss if Components::Alert.present?
  @advanced_search_page.advanced_search.field_search.season.value = season
end

Then(/^I select the value ([^"]*) for the field ([^"]*) in the advanced search$/) do |value, field|
  #Components::Alert.dismiss if Components::Alert.present?
  @advanced_search_page.advanced_search.field_search.send("#{Components::TS.to_sym(field)}").value = value
end

Then(/^I clear the fields$/) do
  @advanced_search_page ||= PageModule::AdvancedSearchRfqPage.new
  @advanced_search_page.advanced_search.clear_fields!
end

When(/^I should have ([^"]*) records? on the search list of the advanced search$/) do |number_of_records|
  @advanced_search_page ||= PageModule::QueryModule::AdvancedSearchRfqPage.new
  expect(@advanced_search_page.search_list.rows_details[:qt_on_page]).to eq number_of_records.to_i
end

When(/^I should at least have ([^"]*) records? on the search list of the advanced search$/) do |number_of_records|
  @advanced_search_page ||= PageModule::QueryModule::AdvancedSearchRfqPage.new
  Components::TS.wait_loading
  expect(@advanced_search_page.search_list.rows_details[:qt_on_page]).to be > number_of_records.to_i
end

When(/^I should see the value ([^"]*) for the field ([^"]*) of the (#{CAPTURE_ORDINAL_INDEX}) record$/) do |value, field, row|
  expect(@advanced_search_page.search_list.row(row.to_i).column(field).value).to eq value
end

When(/^I should see the following values for the (#{CAPTURE_ORDINAL_INDEX}) record/) do |row, table|
  table.rows_hash["BOM #"] = @created_master_item_id if table.rows_hash["BOM #"] == "previous"
  table.rows_hash["Style RFQ No"] = @created_rfqs.last if table.rows_hash["Style RFQ No"] == "previous"
  @advanced_search_page ||= PageModule::QueryModule::AdvancedSearchRfqPage.new
  table.rows_hash.each do |field, value|
    expect(@advanced_search_page.search_list.row(row.to_i).column(field).value).to eq value
  end
end

Then(/^I select Year (\d+)$/) do |year|
  @query_page ||= PageModule::QueryModule::QueryPage.new
  @query_page.search_panel.year.value = year
end

Then(/^I select BOM # (\d+)$/) do |bom_number|
  @query_page ||= PageModule::QueryModule::QueryPage.new
  @query_page.search_panel.bom_no.value = bom_number
end
