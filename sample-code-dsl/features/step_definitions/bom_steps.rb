And /^I change the (#{CAPTURE_ORDINAL_INDEX}) component "(.*)" to "(.*)"/ do |row, column, value|
  bill_of_materials_page = PageModule::OptionResponseBillOfMaterialPage.new
  bill_of_materials_page.bill_of_material.row(row).column(column).value = value
end

And /^I change the(?: (#{CAPTURE_ORDINAL_INDEX}))? component to have the following values/ do |row, table|
  bill_of_materials_page = PageModule::OptionResponseBillOfMaterialPage.new
  row ||= 1
  table.rows_hash.each do |column, value|
    bill_of_materials_page.bill_of_material.row(row).column(column).value = value
  end
end

And /^The (#{CAPTURE_ORDINAL_INDEX}) component "(.*)" should be "(.*)"/ do |row, column, value|
  bill_of_materials_page = PageModule::OptionResponseBillOfMaterialPage.new
  expect(bill_of_materials_page.bill_of_material.row(row).column(column).value).to eq value
end

And /^The (#{CAPTURE_ORDINAL_INDEX}) component "(.*)" should( not)? be readonly/ do |row, column, negate|
  bill_of_materials_page = PageModule::OptionResponseBillOfMaterialPage.new
  value = !negate
  expect(bill_of_materials_page.bill_of_material.row(row).column(column).readonly?).to eq value
end


And /^The(?: (#{CAPTURE_ORDINAL_INDEX}))? component should have the following values/ do |row, table|
  bill_of_materials_page = PageModule::OptionResponseBillOfMaterialPage.new
  row ||= 1
  table.rows_hash.each do |column, value|
    expect(bill_of_materials_page.bill_of_material.row(row).column(column).value).to eq value
  end
end
