Given /^I have the following RFQ template$/ do |table|
  @base_rfq_table = table
end

Given /^I have an RFQ with the following (?:values|differences)$/ do |table|
  table.rows_hash.merge! @base_rfq_table.rows_hash.reject{|k,_v|table.rows_hash[k]} if @base_rfq_table
  if @parallel_activated
    @pending_rfq_table ||= []
    @pending_rfq_table << table
  else
    create_rfq table
  end
end

def create_rfq(table)
  #remove to avoid cache
  @pending_rfq_table = nil
  return table.map{ |t| create_rfq(t) }.last if table.is_a? Array

  table.rows_hash["variation_desc"] = @created_master_item_id if table.rows_hash["variation_desc"] == 'previous'
  table.rows_hash["master_item_id"] = @created_master_item_id if table.rows_hash["master_item_id"] == 'previous'

  @created_master_item_id = (table.rows_hash["master_item_id"] || @created_master_item_id)

  created_quote_info = QuoteCreator.create_quote(table.rows_hash, {:created_master_item_id => @created_master_item_id})

  @contract = created_quote_info[:contract]
  @new_contract = created_quote_info[:contract]
  @created_rfqs << created_quote_info[:created_rfq_no]
  @created_master_item_id = created_quote_info[:created_master_item_id]
  @created_master_item_id_list ||= []
  @created_master_item_id_list << @created_master_item_id
  @created_offers = created_quote_info[:created_offers]
  p "RFQ: #{@created_rfqs.last}, Offers: #{@created_offers.first}, MasterItemId: #{@created_master_item_id}"
  {
    contract:                    @contract,
    created_master_item_id:      @created_master_item_id,
    created_master_item_id_list: @created_master_item_id_list,
    created_offers:              @created_offers,
    created_rfqs:                @created_rfqs,
    new_contract:                @new_contract
  }
end
