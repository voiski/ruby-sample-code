When(/^I update the (#{CAPTURE_ORDINAL_INDEX}) row as following$/) do |row_index,table|
  @last_changes = {
    row_index: row_index,
    table: table.rows_hash
  }
  PageModule::Page.last_page.row(row_index).check
  @last_changes[:table].each do |key, value|
    PageModule::Page.last_page.row(row_index).column(key).value=value
  end
end

When(/^I click on ([^"]*) action$/) do |action|
  PageModule::Page.last_page.send("#{Components::TS.to_sym(action)}!")
end

When(/^I click on ([^"]*) action of the modal$/) do |action|
  PageModule::Page.last_page.last_modal.send("#{Components::TS.to_sym(action)}!")
end

When(/^I click on "([^"]*)"\/"([^"]*)" action$/) do |action_group,action|
  PageModule::Page.last_page
                  .send("#{Components::TS.to_sym(action_group)}!")
                  .send("#{Components::TS.to_sym(action)}!")
end

When(/^I navigate to the (.*) from the column "([^"]*)"$/) do |title,field|
  PageModule::Page.last_page.load!
  field_component = PageModule::Page.last_page.column field
  field_component.click
  Components::TS.wait_table_loading
end

When(/^I change the field "([^"]*)" to "([^"]*)"$/) do |field,value|
  PageModule::Page.last_page.load!
  field_component = PageModule::Page.last_page.field field
  field_component.value = value
end

Then(/^I should have (\d+)(?: options?| items?)$/) do |number_of_options|
  expect(PageModule::Page.last_page.rows_details[:qt_on_page]).to eq number_of_options.to_i
end

Then(/^I should see my last changes$/) do
  @last_changes[:table].each do |key, value|
    expect(PageModule::Page.last_page.row(@last_changes[:row_index]).column(key).value).to eq value
  end
end

Then(/^I should see the "([^"]*)" as "([^"]*)"$/) do |field,value|
  PageModule::Page.last_page.load!
  field_component = PageModule::Page.last_page.field field
  expect(field_component.value).to eq value
end

Then(/^I should see the "([^"]*)" as N\/A$/) do |field|
  PageModule::Page.last_page.load!
  field_component = PageModule::Page.last_page.field field, :plain_text
  expect(field_component.value).to eq 'N/A'
end

When(/^I select the (#{CAPTURE_ORDINAL_INDEX}) row$/) do |row|
  PageModule::Page.last_page.row(row).check
end

Then(/^I should see at the (#{CAPTURE_ORDINAL_INDEX}) row the "([^"]*)" as "([^"]*)"$/) do |row,field,value|
  column_component = PageModule::Page.last_page.row(row).column field
  expect(column_component.value).to eq value
end
