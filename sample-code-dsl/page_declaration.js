/*
 * Script to extract the page object definition form the real page.
 * Usage:
 *  Copy and paste it on the browser console, them run `decorate()`.
 *  To run with debug logs jsut pass true: decorate(true);
 */
var decorate = function(debug_mode) {

    var Util = function() {
        return {
            normalize: function(text) {
                return text.toUpperCase().replace('*', '').replace('#', '').replace(/ /g, '_').replace(/&/g, 'AND');
            }
        }
    }();

    var Xpath = function() {
        return {
            forEach: function(xpath, funktion) {
                $x(xpath).forEach(funktion);
            },
            first: function(xpath) {
                return $x(xpath)[0];
            }
        }
    }();

    var Log = function() {
        return {
            DEBUG_MODE: debug_mode,
            WARNS: 0,
            warn: function() {
                console.warn.apply(console, arguments);
                Log.WARNS++;
            },
            info: function() {
                console.log.apply(console, arguments);
            },
            debug: function() {
                Log.DEBUG_MODE && console.log.apply(console, arguments);
            }
        }
    }();

    var Field = function() {
        var hasSmartTag = function(element) {
            return element.nextSibling && element.nextSibling.tagName == 'IMG'
                                       && element.nextSibling.attributes.src.textContent == 'images/tip.gif';
        }

        var hasCalendar = function(element) {
            return element.nextSibling && element.nextSibling.tagName == 'A'
                                       && element.nextSibling.childNodes.length > 0
                                       && element.nextSibling.childNodes[0].attributes.src.textContent == 'images/calen.gif';
        }

        var hasLookup = function(element) {
            return element.nextSibling && element.nextSibling.tagName == 'IMG'
                                       && element.nextSibling.attributes.src.textContent == 'images/valsearch.gif';
        }
        return {
            type: function(field) {
                var type='!DONT_KNOW!';
                if(field){
                  switch (field.nodeName) {
                      case 'INPUT':
                          field.attributes.type.textContent == 'checkbox' && (type='checkbox')
                          || hasSmartTag(field) && (type='smart_tag')
                          || hasCalendar(field) && (type='calendar')
                          || hasLookup(field) && (type='lookup')
                          || (type='text');
                          break;
                      case 'IMG':
                        field.id.indexOf('moredetails_')==0 && (type='sandwich_menu')
                        || (type='attachment');
                        break;
                      case 'A':     type='link';       break;
                      case 'LABEL': type='plain_text'; break;
                      default:      type=field.nodeName.toLowerCase();
                  }
                }
                return type;
            }
        }
    }();

    var Table = function() {
        return {
            input: function(header) {
                var idx = 0;
                $(header.parentNode.parentElement).find('td').each(function(i, td) {
                    idx === 0 && td.children && (
                        td.children[0].textContent == header.textContent && (idx=i)
                        || (idx=0)
                    );
                });
                return header.parentNode.parentNode.nextSibling.children[idx].children[0];
            },
            freezedColumn: function(element) {
                Log.debug('---------has freeze--------');
                Log.debug(element);
                Log.debug('Table ID: ' + element.parentNode.parentNode.parentNode.parentNode.id);
                Log.debug('---------------------------');
                Log.debug(element.parentNode.parentNode.parentNode.parentNode.id === 'detail_section_left_table');

                return element.parentNode.parentNode.parentNode.parentNode.id === 'detail_section_left_table';
            }
        }
    }();

    var Builder = function() {
            var selectOptions = function(option_elements) {
                var options = [];
                option_elements.forEach(function(o) {
                    if (o.text != '--Select--') {
                        options.push('\'' + o.text + '\'');
                    }
                });
                return options;
            }
            var buildSections= function(currentPanel, sections) {
                sections.forEach(function(e, idx) {
                    if (idx > 0) {
                        var sectionName = e.textContent;
                        var currentSection = {
                            section_name: sectionName,
                            panel_name: currentPanel.panel_name,
                            fields: [],
                            section_type: 'section'
                        };
                        currentPanel.sections.push(currentSection);
                        buildfields(null, currentSection);
                    }
                });
            }
            var buildfields = function(xpath, parent) {
                var field_label_xpath = xpath ? xpath : "//table[@class='clssectionheader']/tbody/tr/td[2]/b[contains(text(),'" + parent.panel_name + "')]/ancestor::table[@class='clsContentSection'][1]/tbody/tr[2]/descendant::b[text()='" + parent.section_name + "']/ancestor::table[2]/descendant::label[not(@keyinfo)]";
                var labels = $x(field_label_xpath);
                var fieldIndex = 1;
                var sandwich_menu = $x("//table[@class='clssectionheader']/tbody/tr/td[2]/b[contains(text(),'" + parent.panel_name + "')]/ancestor::table[@class='clsContentSection'][1]/tbody/tr[2]/descendant::b[text()='" + parent.section_name + "']/ancestor::table[2]/descendant::img[@id='menusectionCheckBox']");
                if (sandwich_menu) fieldIndex++;
                labels.forEach(function(e) {
                    if ($(e).is(':visible')) {
                        Log.debug('-------processing-------');
                        Log.debug(e);
                        var fieldName = e.textContent.replace(' ', ' ');
                        var input = e.nextSibling ? e.nextSibling.nextSibling : null;
                        var position = 'above';
                        if (input === null) {
                            input = e.parentNode.nextSibling ? e.parentNode.nextSibling.children : [];
                            input = input.length > 0 ? input[0] : null;
                            position = 'default';
                        }
                        Log.debug(position);
                        Log.debug(labels.indexOf(input));
                        if (input === null || labels.indexOf(input) >= 0) {
                            try{ input = Table.input(e); } catch (err){
                              Log.warn('Not possible to now the type of the field: ', fieldName);
                            }
                            parent.section_type = 'table_section';
                        }
                        var type = Field.type(input);
                        var options;
                        if (type == 'select') {
                            try {
                                options = selectOptions([].slice.call(e.parentNode.parentNode.nextSibling.children[fieldIndex].children[0].children));
                            } catch (err) {
                                try {
                                    options = selectOptions([].slice.call(e.nextSibling.nextSibling.children));
                                } catch (err) {
                                    Log.warn('Failed to get the options for the select field: ' + fieldName);
                                }
                            }
                        }
                        parent.fields.push({
                            field_name: (fieldName != ' ' ? fieldName.replace('*', '') : null),
                            field_type: type,
                            position: position,
                            readonly: input?input.attributes.readonly:false,
                            freeze: input?Table.freezedColumn(input):false,
                            options: options
                        });
                        fieldIndex++;
                        Log.debug('-------finished -----');
                    }
                });
            }
        return {
            title: function() {
                title = Xpath.first("//label[@class='clstitleText'][1]").textContent.trim()
            }

            ,
            tabs: function() {
                Xpath.forEach("//div[@id='Tabs']/table/descendant::label", function(tab) {
                    var currentTab = {
                        tab_name: tab.textContent.trim()
                    };
                    currentTab.tab_name != '>>' && tabs.push(currentTab);
                });
            }

            ,
            actions: function() {
                Xpath.forEach("//table[@class='tableContentViewerMain']/tbody/tr[2]/td/table/tbody/tr/td[3]/table/tbody/tr/td[descendant::label]", function(btn) {
                    var hasMore = !btn.hasAttribute('name');
                    if (hasMore) {
                        var isParent = true;
                        var parentButton = {};
                        $(btn).find('label').each(function(idx, lbl) {
                            if (lbl.textContent && lbl.textContent.trim().length > 0) {
                                if (isParent) {
                                    parentButton = {
                                        label: lbl.textContent.trim(),
                                        buttons: []
                                    };
                                    actionBar.push(parentButton);
                                    isParent = false;
                                } else {
                                    parentButton.buttons.push({
                                        label: lbl.textContent.trim()
                                    });
                                }
                            }
                        });
                    } else {
                        actionBar.push({
                            label: btn.textContent.trim()
                        });
                    }
                });

            }

            ,
            panels: function() {
                Xpath.forEach("//table[@class='clssectionheader']/tbody/tr/td[2]/b", function(e) {
                    var panelName = e.textContent;
                    var currentPanel = {
                        panel_name: panelName,
                        type: null, // TODO: get the right type to handle the others types
                        collapsed: e.parentNode.parentNode.childNodes[0].childNodes[0].src.indexOf('Rarrow')>0,
                        sections: [],
                        fields: []
                    };
                    panels.push(currentPanel);
                    var sections = $x("//table[@class='clssectionheader']/tbody/tr/td[2]/b[contains(text(),'" + panelName + "')]/ancestor::table[@class='clsContentSection'][1]/tbody/tr[2]/ancestor::table[@class='clsContentSection'][1]/descendant::b/text()");
                    var detailsPanelFieldsXpath = "//table[@class='clssectionheader']/tbody/tr/td[2]/b[contains(text(),'" + panelName + "')]/ancestor::table[@class='clsContentSection'][1]/tbody/tr[2]/descendant::tr[@class='row0']/td/descendant::label[contains(normalize-space(), '')]/../../../tr[1]/td/label";
                    Log.debug('.................................');
                    Log.debug('Sections: ');
                    Log.debug(sections);
                    Log.debug('.................................');
                    currentPanel.collapsed && e.parentNode.click();
                    if (sections.length > 1) {
                        buildSections(currentPanel, sections);
                    } else {
                        buildfields(detailsPanelFieldsXpath, currentPanel);
                    }
                });
            }

        }
    }();


    var Print = function() {
        var prefixLevel = 1;
        var groupTag = null;
        var print = function() {
            Print.output += Array.prototype.slice.call(arguments).join('');
        }
        var println = function() {
            Print.output += Array.prototype.slice.call(arguments).join('') + '\n';
        }
        var brealLine = function() {
            Print.output += '\n';
        }
        var printGroup = function(tag, name, args, block) {
            printComp(tag, name, args, 'do');
            groupTag = tag;
            prefixLevel++;
            block();
            prefixLevel--;
            printComp('end');
        }
        var printComp = function(tag, name, args, isGroup) {
            print(Array(prefixLevel).join('  '), tag);
            name && print(' \'', name, '\'');
            args && print(', ', args.join(', '));
            isGroup && print(' do');
            brealLine();
        }
        var printField = function(f, output) {
            var params = [':' + f.field_type];
            f.position != 'default' && params.push(' :' + f.position);
            f.readonly && params.push(' :readonly');
            f.smart_tag && params.push(' :smart_tag');
            f.calendar && params.push(' :calendar');
            f.lookup && params.push(' :lookup');
            f.freeze && params.push(' :freeze');
            if(f.options && f.options.length > 0){
              if(f.options.length>10){
                Log.warn('To much options for the field: ', f.field_name, f.options);
              }else{
                params.push(' :options => [' + f.options + ']');
              }
            }
            printComp(groupTag.indexOf('table') != -1 ? 'column' : 'field', f.field_name, params);
        }
        var flush = function() {
            Log.info(Print.output);
            Print.output = '';
            prefixLevel = 1;
            Log.warn('Finish with ', Log.WARNS, ' warns! Please take a look!')
            Log.WARNS = 0;
        }
        return {
            output: '',
            dsl: function() {
                printComp('title', title);
                brealLine();

                tabs.forEach(function(tab) {
                    printComp('tab', tab.tab_name);
                });
                brealLine();

                actionBar.forEach(function(btn) {
                    if (btn.buttons && btn.buttons.length > 0) {
                        printGroup('button', btn.label, null, function() {
                            btn.buttons.forEach(function(b) {
                                printComp('button', b.label);
                            });
                        });
                    } else {
                        printComp('button', btn.label);
                    }
                });

                panels.forEach(function(p) {
                    brealLine();
                    printGroup('panel', p.panel_name, p.collapsed ? [':collapsed'] : null, function() {
                        p.sections.forEach(function(s) {
                            printGroup(s.section_type, s.section_name, null, function() {
                                s.fields.forEach(function(f) {
                                    printField(f);
                                });
                            });
                        });
                        p.fields.forEach(function(f) {
                            printField(f);
                        });
                    });
                });

                flush();
            }
        }
    }();

    var title = null;
    var tabs = [];
    var panels = [];
    var actionBar = [];

    clear();
    Builder.title();
    Builder.tabs();
    Builder.actions();
    Builder.panels();

    Print.dsl();
}

// decorate(confirm('Debug mode?'));
decorate();
