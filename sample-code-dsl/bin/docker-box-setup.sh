#!/bin/bash
# Docker script

# Pre build
ruby -v
echo "== Looding the bundle of gems =="
bundle install -j $(nproc) --path vendor

echo "== Removing old logs and tempfiles =="
rm -rf logs/*
rm -rf target

# Colors
blue=`tput setaf 4`
cyan=`tput setaf 6`
reset=`tput sgr0`


echo "
== Done ==

  # Cucumber box
  #####
  # This box will hold the cucumber code, to run the test just:
  #    ${blue}docker-compose exec box ${cyan}cucumber -p layout_check${reset}
  #    ${blue}docker-compose exec box ${cyan}cucumber -p regression${reset}
  #    ${blue}docker-compose exec box ${cyan}bundle exec parallel_cucumber features/tradestone_features/ -o '-p layout_check'${reset}
  # To debug attach to the docker:
  #    ${blue}docker attach \$(docker ps|grep _box_1|awk \"{print \\\$NF}\")${reset}
  #    ${cyan}cucumber -t @my-tag${reset}

  to close this output, use ctrl+c
"


# Keeping the bash open to avoid exit
/bin/bash
