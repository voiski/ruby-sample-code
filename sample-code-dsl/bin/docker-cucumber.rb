#!/usr/bin/env ruby
# Docker script

puts "Ruby version: #{RUBY_VERSION}"

# Colors for log output
GREEN = `tput setaf 2`
BLUE  = `tput setaf 4`
CYAN  = `tput setaf 6`
RESET = `tput sgr0`

# if no args, exit showing the options
@args = ARGV.clone
if @args.empty?
  puts "
  It will run the cucumber tests, please read the README file for more information
  usage: #{BLUE}docker-compose run --rm cucumber#{CYAN} [options] #{GREEN}[tags|profiles|file]#{RESET}

  Options:
   #{CYAN }-b,--bundle-install     #{RESET}Force to run bundle install, it will run at the first time anyway
   #{CYAN }-n,--node               #{RESET}Works with --parallel, set the number of parallel threads.
   #{CYAN }--parallel              #{RESET}Enable parrallel execution
   #{CYAN }--path                  #{RESET}Works with --parallel, default to tradestone_features path
   #{CYAN }--skip-cleanup          #{RESET}Skip the cleanup that removes log files and the target folder
   #{CYAN }<cucumber options>      #{RESET}You can pass any other cucumber options, some options will not work with --parallel

  Tags|profiles|file:
   #{GREEN}-t,--tags <arg>         #{RESET}Cucumber tags, works in the same way
   #{GREEN}-p,--profile <arg>      #{RESET}Cucumber profile, works in the same way
   #{GREEN}/path/to/feature[:line] #{RESET}Like cucumber, it will run a specific file

  Examples:
   # runs the tag @layout_check
   #{BLUE}docker-compose run --rm cucumber #{GREEN}-t @layout_check#{RESET}

   # runs the profile layout_check that runs the @layout_check tag generating the cucumber report
   #{BLUE}docker-compose run --rm cucumber #{GREEN}-p layout_check#{RESET}

   # runs the specific scenario from the feature file at the line 11
   #{BLUE}docker-compose run --rm cucumber #{GREEN}features/tradestone_features/smoke_tests/layout_check_test/admin_layout_check.feature:11#{RESET}

   # runs the tag @layout_check in parallel with 5 threads
   #{BLUE}docker-compose run --rm cucumber #{CYAN}--parallel -n 5 #{GREEN}-t @layout_check#{RESET}
  "
  exit 0
end

# Returns true if the given keys were in arguments, it will also remove from the args
def enabled?(*keys)
  exist = @args&keys
  unless exist.empty?
    @args -= exist
    true
  end
end

# Returns the value from the given keys, it will also remove the key and the value from the args
def custom_value(*keys)
  key = (@args&keys).first
  if key
    value = @args[@args.index(key)+1]
    @args -= [key,value]
    value
  end
end

# bundle, set -b to force it, it also will run at the first time
system 'bundle config --local path vendor'
if enabled?('--bundle-install', '-b') || Dir['../../../vendor/ruby/*'].empty?
  puts "#{GREEN}== Looding the bundle of gems ==#{RESET}"
  system 'bundle install'
end

# clean logs and target by default, to skip it pass --skip-cleanup
unless enabled? '--skip-cleanup'
  puts "#{GREEN}== Removing old logs and tempfiles ==#{RESET}"
  system 'rm -rf logs/*'
  system 'rm -rf target'
end

# This will work with cucumber or with parallel
# Ex:
#     -t @my-tag
#     --parallel -n 5 --path /some/path -t @my-tag
#     --parallel -p layout_check
puts "#{GREEN}== I will execute your command now =="
command = if enabled? '--parallel'
            path = custom_value '--path'
            path ||= 'features/tradestone_features/'

            nodes = custom_value '-n', '--node'
            nodes = "-n #{nodes}" if nodes

            {
              base: "bundle exec parallel_cucumber #{path} #{nodes} -o",
              args: "\'#{@args.join(' ')}\'"
            }
          else
            {
              base: 'bundle exec cucumber',
              args: @args.join(' ')
            }
          end

puts "#{BLUE}#{command[:base]} #{CYAN}#{command[:args]}#{RESET}"
exec "#{command[:base]} #{command[:args]}"
